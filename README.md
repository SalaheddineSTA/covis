CoViS
=====

CoViS is the code base for the computer vision algorithms done within the
SDU Robotics group at the University of Southern Denmark
(<a href="http://robotics.sdu.dk" target="_blank">http://robotics.sdu.dk</a>).


Quick installation
==================

The following guide has been tested on Ubuntu Linux.
All commands below should be entered in a terminal.

Install system tools and dependencies from the Ubuntu repositories:
```sh
sudo apt install cmake doxygen g++ git libboost-all-dev libeigen3-dev libflann-dev libopencv-dev libvtk6-dev python-numpy
```

If you have **Ubuntu version 17.04 and up**, you can additionally install PCL from the repositories:
```sh
sudo apt install libpcl-dev
```

Otherwise, for **Ubuntu versions 16 and earlier**, you need to compile PCL 1.8 from source:
```sh
git clone --branch pcl-1.8.0 https://github.com/PointCloudLibrary/pcl.git
mkdir -p pcl/build
cd pcl/build
cmake .. -DCMAKE_BUILD_TYPE=Release
make
```

If you encounter an error related to **libproj** during the compilation process, it's due to a [bug](https://bugs.debian.org/cgi-bin/bugreport.cgi?bug=819741) in the Debian package for VTK. This can be resolved by creating a symlink with the correct filename, after which you can resume compilation with **make** (adjust paths if needed):
```sh
sudo ln -s /usr/lib/x86_64-linux-gnu/libproj.so.9 /usr/lib/x86_64-linux-gnu/libproj.so
```

Now get and compile CoViS, relative to some root folder of your choice:
```sh
git clone https://gitlab.com/caro-sdu/covis.git covis
mkdir -p covis/build
cd covis/build
cmake -DPCL_DIR=/path/to/pcl/build ..
make
```
where you insert the appropriate absolute path to PCL in order to instruct CMake to look for the correct version of PCL.
If you have cloned PCL and CoViS under the same root folder, you don't need to pass PCL_DIR to CMake.

Also build the documentation (inside **covis/build**):
```sh
make doc
```
You will now find API doc and additional information,
e.g. on how to use CoViS in your own CMake project, in **covis/build/doc/html/index.html**.

Interesting examples
====================

Here are some examples for pose estimation and recognition in 3D data:
 - Correspondence voting for 3D correspondence ranking and pose estimation: [example/correspondence_voting](example/correspondence_voting/README.md)
 - RANSAC and prerejective RANSAC pose estimation: [example/ransac](example/ransac/README.md)
 - Pose voting: [example/pose_voting](example/pose_voting/README.md)
 - General multi-object and multi-instance recognition pipeline using either RANSAC or pose voting: [example/recognition](example/recognition/README.md)

