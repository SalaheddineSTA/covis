# Setup main CoViS library
#
# These variables are set in the cache
#   COVIS_MODULE_DIRS
#   COVIS_HEADERS
#   COVIS_SOURCES
#


##
# Find module paths, write to COVIS_MODULE_DIRS
##
set(COVIS_MODULE_DIRS calib core detect feature filter util visu CACHE STRING "CoViS modules to build" FORCE)
mark_as_advanced(COVIS_MODULE_DIRS)

##
# Create CoViS configuration header
##
configure_file(${CMAKE_CURRENT_SOURCE_DIR}/covis_config.h.in
               ${CMAKE_CURRENT_SOURCE_DIR}/covis_config.h)

##
# Find headers, save in cache
##
file(GLOB_RECURSE COVIS_HEADERS RELATIVE ${CMAKE_SOURCE_DIR}/ "*.h" "*.hpp")
set(COVIS_HEADERS ${COVIS_HEADERS} CACHE INTERNAL "CoViS header files")

##
# Visit modules and find sources, save in cache
##
set(COVIS_SOURCES_TMP)
foreach(MODULE_DIR ${COVIS_MODULE_DIRS})
  # Clean up
  unset(SOURCES)
  
  # Get module source files in SOURCES
  add_subdirectory(${MODULE_DIR})
  
  # Check SOURCES variable
  if(SOURCES)
    # Add
    foreach(SOURCE ${SOURCES})
      list(APPEND COVIS_SOURCES_TMP ${CMAKE_CURRENT_SOURCE_DIR}/${MODULE_DIR}/${SOURCE})
    endforeach(SOURCE)
  endif(SOURCES)
endforeach(MODULE_DIR)
set(COVIS_SOURCES ${COVIS_SOURCES_TMP} CACHE INTERNAL "CoViS source files")
unset(COVIS_SOURCES_TMP)
