// Copyright (c) 2013, University of Southern Denmark
// All rights reserved.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice,
//    this list of conditions and the following disclaimer.
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation
//    and/or other materials provided with the distribution.
// 3. Neither the name of the University of Southern Denmark nor the names of
//    its contributors may be used to endorse or promote products derived from
//    this software without specific prior written permission.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL THE UNIVERSITY OF SOUTHERN DENMARK BE
// LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
// CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
// SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
// INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.

// Own
#include "feature_binarization.h"

namespace covis {
    namespace feature {
        BinaryMatrixT FeatureBinarization::compute(const MatrixT& features) {
            COVIS_ASSERT_MSG(features.rows() > 0, "Empty features!");
            COVIS_ASSERT_MSG(features.rows() % 8 == 0, "Invalid feature dimension (" << features.rows() << ") - " <<
                                                       "divisible by 8!");

            BinaryMatrixT result(features.rows() / 8, features.cols());
            for(int i = 0; i < features.cols(); ++i) {
                boost::dynamic_bitset<BinaryBlockT> bitset = binarize(features.col(i));
                boost::to_block_range(bitset, result.col(i).data());
            }

            return result;
        };

        boost::dynamic_bitset<BinaryBlockT> FeatureBinarization::binarize(
                const Eigen::Matrix<MatrixT::Scalar, Eigen::Dynamic, 1>& feature) {
            const int dim = feature.size();
            boost::dynamic_bitset<BinaryBlockT> bitset(dim, 0);
            for(int j = 0; j < dim; j += 4) {
                const float S0 = feature(j);
                const float S1 = feature(j+1);
                const float S2 = feature(j+2);
                const float S3 = feature(j+3);
                const float sum = S0 + S1 + S2 + S3;
                const float sum90 = 0.9 * sum;

                // Case A
                if(sum < 1e-5f) // Already zero, just continue
                    continue;

                // Case B
                if(S0 > sum90 || S1 > sum90 || S2 > sum90 || S3 > sum90) { // Case B
                    bitset[j] = (S0 > sum90);
                    bitset[j+1] = (S1 > sum90);
                    bitset[j+2] = (S2 > sum90);
                    bitset[j+3] = (S3 > sum90);
                    continue;
                }

                // Case C
                const float sum01 = S0 + S1;
                const float sum02 = S0 + S2;
                const float sum03 = S0 + S3;
                const float sum12 = S1 + S2;
                const float sum13 = S1 + S3;
                const float sum23 = S2 + S3;
                if(sum01 > sum90) {
                    bitset[j] = bitset[j+1] = 1;
                    continue;
                } else if(sum02 > sum90) {
                    bitset[j] = bitset[j+2] = 1;
                    continue;
                } else if(sum03 > sum90) {
                    bitset[j] = bitset[j+3] = 1;
                    continue;
                } else if(sum12 > sum90) {
                    bitset[j+1] = bitset[j+2] = 1;
                    continue;
                } else if(sum13 > sum90) {
                    bitset[j+1] = bitset[j+3] = 1;
                    continue;
                } else if(sum23 > sum90) {
                    bitset[j+2] = bitset[j+3] = 1;
                    continue;
                }

                // Case D
                const float sum012 = S0 + S1 + S2;
                const float sum013 = S0 + S1 + S3;
                const float sum023 = S0 + S2 + S3;
                const float sum123 = S1 + S2 + S3;
                if(sum012 > sum90) {
                    bitset[j] = bitset[j+1] = bitset[j+2] = 1;
                    continue;
                } else if(sum013 > sum90) {
                    bitset[j] = bitset[j+1] = bitset[j+3] = 1;
                    continue;
                } else if(sum023 > sum90) {
                    bitset[j] = bitset[j+2] = bitset[j+3] = 1;
                    continue;
                } else if(sum123 > sum90) {
                    bitset[j+1] = bitset[j+2] = bitset[j+3] = 1;
                    continue;
                }

                // Case E
                bitset[j] = bitset[j+1] = bitset[j+2] = bitset[j+3] = 1;
            }

            return bitset;
        }

        BinaryMatrixT binarizeFeatures(const MatrixT& features) {
            FeatureBinarization fb;

            return fb.compute(features);
        }
    }
}
