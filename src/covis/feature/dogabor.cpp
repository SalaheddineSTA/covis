// Copyright (c) 2013, University of Southern Denmark
// All rights reserved.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice,
//    this list of conditions and the following disclaimer.
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation
//    and/or other materials provided with the distribution.
// 3. Neither the name of the University of Southern Denmark nor the names of
//    its contributors may be used to endorse or promote products derived from
//    this software without specific prior written permission.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL THE UNIVERSITY OF SOUTHERN DENMARK BE
// LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
// CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
// SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
// INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.

// Own
#include "dogabor.h"
#include "../covis_config.h"

// OpenCV
#include <opencv2/core/version.hpp>
#include <opencv2/opencv_modules.hpp>
#if defined(CV_VERSION_EPOCH) && CV_VERSION_EPOCH == 2
#define OPENCV_2 1
#endif
#if OPENCV_2 // OpenCV 2
#ifdef HAVE_OPENCV_NONFREE
#include <opencv2/nonfree/nonfree.hpp>
#endif
#else // OpenCV 3
#ifdef HAVE_OPENCV_XFEATURES2D
#include <opencv2/xfeatures2d/nonfree.hpp>
#endif
#endif
#include <opencv2/opencv.hpp>

// PCL
#include <pcl/point_types.h>
#include <pcl/common/transforms.h>
#include <pcl/features/normal_3d.h>
#include <pcl/filters/extract_indices.h>

// Covis
#include "../core/macros.h"

namespace covis {
    namespace feature {
        DOGabor::DOGabor(Detector detector, float f0, float sigma, int scales, int rotations, float k, int kernel_size,
                int patch_size, float r_search) {
            _detector = detector;
            _matcher = filter::GaborJet(kernel_size, f0, sigma, rotations, 1);
            _scales = scales;
            _rotations = rotations;
            _patchSize = patch_size;
            _rSearch = r_search;
            _k = k;
        
}
        void DOGabor::compute(const cv::Mat &image, const cv::Mat &depth, const cv::Mat &mask, cv::Matx33f K,
                float & distance, std::vector<cv::KeyPoint> & kp, cv::Mat& desc, bool original) {
            COVIS_ASSERT(depth.type() == CV_16U);

            //make the image gray scale
            cv::Mat imageGrey;
            if (image.channels() == 1)
                imageGrey = image;
            else
#ifdef OPENCV_3
                cvtColor(image, imageGrey, cv::COLOR_BGR2GRAY);
#else
				cvtColor(image, imageGrey, CV_BGR2GRAY);
#endif

            //calculate the keypoints
#if (OPENCV_2 && defined(HAVE_OPENCV_NONFREE)) || (CV_VERSION_MAJOR == 3 && defined(HAVE_OPENCV_XFEATURES2D))
            if (_detector == SIFT) {
#if OPENCV_2
                cv::SiftFeatureDetector siftDetector(400);
                siftDetector.detect(imageGrey, kp, mask);
#else // OpenCV 3
                cv::Ptr<cv::xfeatures2d::SiftFeatureDetector> siftDetector =
                        cv::xfeatures2d::SiftFeatureDetector::create(400);
                siftDetector->detect(imageGrey, kp, mask);
#endif
            }
            if (_detector == SURF) {
#if OPENCV_2
                cv::SurfFeatureDetector surfDetector(1000);
                surfDetector.detect(imageGrey, kp, mask);
#else // OpenCV 3
                cv::Ptr<cv::xfeatures2d::SurfFeatureDetector> surfDetector =
                        cv::xfeatures2d::SurfFeatureDetector::create(1000);
                surfDetector->detect(imageGrey, kp, mask);
#endif
            }
#else // Non-free functionality unavailable, enforce the FAST detector
            if (_detector != FAST) {
                COVIS_MSG_WARN("Non-free OpenCV functionality not available! Forcing the use of the FAST detector...");
                _detector = FAST;
            }
#endif
            
            if (_detector == FAST) {
#if OPENCV_2
                cv::FastFeatureDetector fastDetector(100);
                fastDetector.detect(imageGrey, kp, mask);
#else // OpenCV 3
                cv::Ptr<cv::FastFeatureDetector> fastDetector = cv::FastFeatureDetector::create();
                fastDetector->detect(imageGrey, kp, mask);
#endif
            }
	    if (_detector == EXTERNAL) {
	    	// Use the keypoints given in kp
            }
#undef OPENCV_2 // Clean up

	    //blur the depth image
	    cv::Mat imageDepth;
	    medianBlur(depth, imageDepth, 5);

	    // calculate mean distance
	    if (distance < 0) {
	      double summedDistance = 0;
	      for (size_t index = 0; index < kp.size(); index++)
		summedDistance += imageDepth.at<uint16_t>(kp[index].pt);
	      distance = (0.001 * summedDistance / kp.size()) * 0.68; /** HARDCODED **/
	    }

	    //create a point cloud
	    pcl::PointCloud<pcl::PointXYZRGB>::Ptr cloud(
							 new pcl::PointCloud<pcl::PointXYZRGB>(imageGrey.cols, imageGrey.rows));

	    //get camera parameters
	    float focalLenghtX = K(0, 0);
	    float focalLenghtY = K(1, 1);

            //convert image to point cloud
	    for (int h = 0; h < imageGrey.cols; h++) {
	      for (int w = 0; w < imageGrey.rows; w++) {
                cloud->at(h, w).z = -float(imageDepth.at<uint16_t>(w, h)) / 1000;

                float z_focal_x = -1000 * cloud->at(h, w).z / focalLenghtX;
                float z_focal_y = -1000 * cloud->at(h, w).z / focalLenghtY;

                cloud->at(h, w).x = float((h - imageGrey.cols / 2) / 1000.0) * z_focal_x;
                cloud->at(h, w).y = float((-w + imageGrey.rows / 2) / 1000.0) * z_focal_y;
	      }
	    }

	    //estimage normals in point cloud
	    pcl::NormalEstimation<pcl::PointXYZRGB, pcl::Normal> ne;
	    ne.setInputCloud(cloud);
	    pcl::PointCloud<pcl::Normal>::Ptr cloud_normals(new pcl::PointCloud<pcl::Normal>);
	    ne.setRadiusSearch(_rSearch);
	    boost::shared_ptr<std::vector<int> > indices(new std::vector<int>());

            //use only the normals for points of interest
	    for (int i = 0; i < int(kp.size()); i++) {

	      int x = kp[i].pt.x;
	      int y = kp[i].pt.y;

	      indices->push_back((y * imageGrey.cols + x));
	    }

	    ne.setIndices(indices);

	    //compute the normals
	    ne.compute(*cloud_normals);

	    //compute the patches

	    std::vector<cv::Mat> matVector; //(cloud_normals->size());
	    cv::Mat gaborJetMat;

	    //only use keypoints where the descriptor actually works
	    std::vector<cv::KeyPoint> acceptedKeypoint; //(cloud_normals->size());

	    int good = 0;
	    int bad = 0;
	    int kernels = 2;

		
#pragma omp parallel for
	    for( int i_pragma = 0; i_pragma < kernels; i_pragma++)
	      //for (int i = 0; i < int(cloud_normals->size()); i++)
	      for (int i = (int(cloud_normals->size()) * (i_pragma) / kernels); i < (int(cloud_normals->size()) * (i_pragma + 1) / kernels); i++) { 
	        //get point 2D
                int x = kp[i].pt.x;
                int y = kp[i].pt.y;

                //get piunt 3D
                pcl::PointXYZRGB _point = cloud->at((y * imageGrey.cols + x));

                //get normal vector n
                Eigen::Vector3f n(cloud_normals->at(i).normal_x, cloud_normals->at(i).normal_y,
				  cloud_normals->at(i).normal_z);
                // flip n towards viewpoint
                pcl::flipNormalTowardsViewpoint(_point, cloud->sensor_origin_[0], cloud->sensor_origin_[1],
						cloud->sensor_origin_[2], n[0], n[1], n[2]); // cloud->at( y * image_color.cols + x )

                // define x_n normal
                Eigen::Vector3f x_n_standard(1, 0, 0);
                // project onto the plane
                Eigen::Vector3f x_n = x_n_standard - x_n_standard.dot(n) * n;
                x_n = x_n.normalized();

                //define y_n as the cross between x_n and n
                Eigen::Vector3f y_n = (n.normalized().cross(x_n.normalized())).normalized(); // define the the y as cross between

                n = n.normalized();

                // define the rotation matrix
                Eigen::Affine3f transformation;
                transformation(0, 0) = x_n[0];
                transformation(0, 1) = y_n[0];
                transformation(0, 2) = n[0];
                transformation(0, 3) = 0.0f;
                transformation(1, 0) = x_n[1];
                transformation(1, 1) = y_n[1];
                transformation(1, 2) = n[1];
                transformation(1, 3) = 0.0f;
                transformation(2, 0) = x_n[2];
                transformation(2, 1) = y_n[2];
                transformation(2, 2) = n[2];
                transformation(2, 3) = 0.0f;
                transformation(3, 0) = 0.0f;
                transformation(3, 1) = 0.0f;
                transformation(3, 2) = 0.0f;
                transformation(3, 3) = 1.0f;

                //Define the 4 planar point
                pcl::PointCloud<pcl::PointXYZ> P0;
                P0.push_back(pcl::PointXYZ(0.1, 0.1, 0));
                P0.push_back(pcl::PointXYZ(0.1, -0.1, 0));
                P0.push_back(pcl::PointXYZ(-0.1, -0.1, 0));
                P0.push_back(pcl::PointXYZ(-0.1, 0.1, 0));

                // transform the planar points
                pcl::PointCloud<pcl::PointXYZ> PT;
                pcl::transformPointCloud(P0, PT, transformation);

                // project the planar point to 2D
                std::vector<cv::Point2f> vectorPlanar;
                for (int pointIndex = 0; pointIndex < 4; pointIndex++) {
                    float zfocalx = distance / focalLenghtX;
                    float zfocaly = distance / focalLenghtY;
                    float xProj = P0[pointIndex].x / (zfocalx) + _patchSize / 2;
                    float yProj = -P0[pointIndex].y / (zfocaly) + _patchSize / 2;
                    vectorPlanar.push_back(cv::Point2f(xProj, yProj));
                }

                //project the scene points into 2D
                std::vector<cv::Point2f> vectorScene;
                for (int pointIndex = 0; pointIndex < 4; pointIndex++) {
                    float zfocalx = (-PT[pointIndex].z - _point.z) / focalLenghtX;
                    float zfocaly = (-PT[pointIndex].z - _point.z) / focalLenghtY;
                    float xProj = (_point.x + PT[pointIndex].x) / (zfocalx) + imageGrey.cols / 2;
                    float yProj = -(_point.y + PT[pointIndex].y) / (zfocaly) + imageGrey.rows / 2;
                    vectorScene.push_back(cv::Point2f(xProj, yProj));
                }

                //estimate homography
                cv::Mat transform2D = getPerspectiveTransform(vectorScene, vectorPlanar);

                // warp image patch
                cv::Mat outputImage;
                cv::warpPerspective(imageGrey, outputImage, transform2D, cv::Size(_patchSize, _patchSize),
                        cv::INTER_NEAREST);

                //calculate the descriptor
                std::vector<cv::Mat> gaborResponseVectorMeanAndSTD;

                for (int scaling = 0; scaling < _scales; scaling++) {

					//blur before scaling
					cv::GaussianBlur(outputImage, outputImage, cv::Size(3, 3), 0, 0);

					//rescale the image
					cv::resize(outputImage, outputImage, cv::Size(outputImage.cols / _k, outputImage.rows / _k), cv::INTER_NEAREST);

					int radius = int(21 * exp(-scaling)) + 3;

					cv::Mat output_imageROI(outputImage,
								cv::Rect(cv::Point(outputImage.cols / 2 - radius / 2, outputImage.rows / 2 - radius / 2),
								cv::Size(radius, radius)));
					
					
					// make a black mask, same size:
					cv::Mat mask(outputImage.size(), CV_8UC1, cv::Scalar::all(0));
					// with a white, filled circle in it:
					cv::circle(mask, cv::Point(outputImage.cols / 2, outputImage.rows / 2), radius / 2,
					cv::Scalar::all(255), -1);

					// perform the filtering
					cv::Mat gaborResponse = _matcher.filter(outputImage);

					std::vector<cv::Mat> gaborResponseVector;
					split(gaborResponse, gaborResponseVector);

					std::vector<cv::Mat> gaborResponseVectorSingle;
					std::vector<cv::Mat> gaborResponseVectorSingleSTD;
					cv::Mat gaborResponseCombined;
					cv::Mat gaborResponseCombinedSTD;

					for (int vectorIndex = 0; vectorIndex < int(gaborResponseVector.size()); vectorIndex++) {

						cv::Mat mean;
						cv::Mat mstd;
						meanStdDev(gaborResponseVector[vectorIndex], mean, mstd, mask);

						cv::Mat meanResult(1, 1, CV_32FC1, cv::mean(mean));
						cv::Mat stdResult(1, 1, CV_32FC1, cv::mean(mstd));


						gaborResponseVectorSingle.push_back(meanResult);
						gaborResponseVectorSingleSTD.push_back(stdResult);
										
					}

					hconcat(gaborResponseVectorSingle, gaborResponseCombined);
					hconcat(gaborResponseVectorSingleSTD, gaborResponseCombinedSTD);
					gaborResponseVectorMeanAndSTD.push_back(gaborResponseCombined);
					gaborResponseVectorMeanAndSTD.push_back(gaborResponseCombinedSTD);

				}

				//concatenate all horizontal gabor jets
				cv::Mat gaborResponse;
				hconcat(gaborResponseVectorMeanAndSTD, gaborResponse);

				float meanOfMat = cv::mean( gaborResponse )[0];
				if(  ( meanOfMat*meanOfMat ) > 0.0001 ) {

					

						if (original) {
							cv::Mat gaborDescribtor(gaborResponse.rows, gaborResponse.cols, CV_32F);

							for (int scale = 0; scale < _scales*2; scale++) {
								for (int rotation = 0; rotation < _rotations; rotation++) {
									gaborDescribtor.at<float>( rotation + scale*_rotations ) = gaborResponse.at<float>(
										rotation + scale*_rotations );
								}
							}
							matVector.push_back(gaborDescribtor);
							acceptedKeypoint.push_back(kp[i]);
							
						} else // if it is, an additional number of descriptors ( the number of rotations ) each with a pushed rotation response
						{
							for (int shift = 0; shift < _rotations; shift++) {
								cv::Mat gaborDescribtor(gaborResponse.rows, gaborResponse.cols, CV_32F);
								for (int scale = 0; scale < _scales*2; scale++) {
									for (int rotation = 0; rotation < _rotations; rotation++) {
									    gaborDescribtor.at<float>( rotation + scale*_rotations ) =
									      gaborResponse.at<float>( ((rotation + shift ) % _rotations)  + scale*_rotations );
									}
								}
							matVector.push_back(gaborDescribtor);
							acceptedKeypoint.push_back(kp[i]);
							}
						}
					
						good++;
					}
					else
					{
						cv::KeyPoint tempKeyPoint;
						matVector.push_back(cv::Mat());						
						tempKeyPoint.pt = cv::Point(-1,-1);
						acceptedKeypoint.push_back(tempKeyPoint);
						bad++;
						
					}

	    }
	
		std::vector<cv::Mat> matVectorGood;
		std::vector<cv::KeyPoint> acceptedKeypointGood;

		for( int i = 0; i < int(acceptedKeypoint.size()); i ++) {

			if( acceptedKeypoint[i].pt.x + acceptedKeypoint[i].pt.y > 0 ) {
				acceptedKeypointGood.push_back(acceptedKeypoint[i]);
 				matVectorGood.push_back(matVector[i]);
			}
		}
		
		kp = acceptedKeypointGood;

		vconcat(matVectorGood, gaborJetMat);

		desc = gaborJetMat;

		return;
        }
    }
}

