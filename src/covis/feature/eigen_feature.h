// Copyright (c) 2013, University of Southern Denmark
// All rights reserved.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice,
//    this list of conditions and the following disclaimer.
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation
//    and/or other materials provided with the distribution.
// 3. Neither the name of the University of Southern Denmark nor the names of
//    its contributors may be used to endorse or promote products derived from
//    this software without specific prior written permission.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL THE UNIVERSITY OF SOUTHERN DENMARK BE
// LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
// CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
// SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
// INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.

#ifndef COVIS_FEATURE_EIGEN_FEATURE_H
#define COVIS_FEATURE_EIGEN_FEATURE_H

// Own
#include "distance_color_normal_histogram.h"
#include "distance_normal_histogram.h"
#include "ecsad.h"
#include "pcl_feature.h"
#include "ppf_histogram.h"
#include "ppf_histogram_full.h"
#include "../core/converters.h"
#include "../core/macros.h"

// Boost
#include <boost/algorithm/string.hpp>
namespace al=boost::algorithm;

namespace covis {
    namespace feature {
        /// Comma separated list of possible features for e.g. @ref computeFeature()
//        const std::string FeatureNames = "ecsad,ndhist,ndchist,ppfhist,ppfhistfull,fpfh,shot,cshot,si,usc";
        /// Comma separated list of possible features for e.g. @ref computeFeature()
        const std::string FeatureNames = "ecsad,fpfh,ndhist,ppfhist,ppfhistfull,shot,si,usc";

        /**
         * @ingroup feaure
         * Get the list of possible features for e.g. @ref computeFeature() as a vector of strings
         * @return vector of possible features
         */
        inline std::vector<std::string> getFeatureNames() {
            std::vector<std::string> result;
            al::split(result, FeatureNames, al::is_any_of(","));

            return result;
        }

        /// Feature matrix type
        typedef Eigen::MatrixXf MatrixT;

        /// Feature matrix vector type
        typedef std::vector<MatrixT,Eigen::aligned_allocator<MatrixT> > MatrixVecT;

        /**
         * @ingroup feature
         * Compute a named feature for a point cloud
         * @param name name of the feature - see @ref getFeatureNames() for a list of possible features
         * @param cloud point cloud for which features should be computed
         * @param surface underlying surface used for computing features
         * @param radius feature radius
         * @return features placed as observations in a matrix
         */
        template<typename PointT>
        inline MatrixT computeFeature(const std::string& name,
                                      typename pcl::PointCloud<PointT>::ConstPtr cloud,
                                      typename pcl::PointCloud<PointT>::ConstPtr surface,
                                      float radius) {
            if(al::iequals(name, "ecsad")) {

                typename pcl::PointCloud<typename ECSAD<PointT>::Histogram>::Ptr feat;
                if(cloud == surface)
                    feat = computeECSAD<PointT>(cloud, radius);
                else
                    feat = computeECSAD<PointT>(cloud, surface, radius);
                return core::mapfeat<typename ECSAD<PointT>::Histogram>(*feat);

            } else if(al::iequals(name, "ndhist") ||
                      al::iequals(name, "dnhist") ||
                      al::iequals(name, "DistanceNormalHistogram") ||
                      al::iequals(name, "NormalDistanceHistogram")) {

                typename pcl::PointCloud<typename DistanceNormalHistogram<PointT>::Histogram>::Ptr feat;
                if(cloud == surface)
                    feat = computeDistanceNormalHistogram<PointT>(cloud, radius);
                else
                    feat = computeDistanceNormalHistogram<PointT>(cloud, surface, radius);
                return core::mapfeat<typename DistanceNormalHistogram<PointT>::Histogram>(*feat);

//            } else if(al::iequals(name, "ncdhist") ||
//                      al::iequals(name, "ndchist") ||
//                      al::iequals(name, "dcnhist") ||
//                      al::iequals(name, "DistanceColorNormalHistogram") ||
//                      al::iequals(name, "NormalColorDistanceHistogram")) {
//
//                typename pcl::PointCloud<typename DistanceColorNormalHistogram<PointT>::Histogram>::Ptr feat;
//                if(cloud == surface)
//                    feat = computeDistanceColorNormalHistogram<PointT>(cloud, radius);
//                else
//                    feat = computeDistanceColorNormalHistogram<PointT>(cloud, surface, radius);
//                return core::mapfeat<typename DistanceColorNormalHistogram<PointT>::Histogram>(*feat);

            } else if(al::iequals(name, "ppfhist") ||
                      al::iequals(name, "ppfh") ||
                      al::iequals(name, "hppf") ||
                      al::iequals(name, "PPFHistogram") ||
                      al::iequals(name, "PointPairFeatureHistogram")) {

                typename pcl::PointCloud<typename PPFHistogram<PointT>::Histogram>::Ptr feat;
                if(cloud == surface)
                    feat = computePPFHistogram<PointT>(cloud, radius);
                else
                    feat = computePPFHistogram<PointT>(cloud, surface, radius);
                return core::mapfeat<typename PPFHistogram<PointT>::Histogram>(*feat);

            } else if(al::iequals(name, "ppfhistfull") ||
                      al::iequals(name, "ppfhfull") ||
                      al::iequals(name, "hppffull") ||
                      al::iequals(name, "PPFHistogramFull") ||
                      al::iequals(name, "PointPairFeatureHistogramFull")) {

                typename pcl::PointCloud<typename PPFHistogramFull<PointT>::Histogram>::Ptr feat;
                if(cloud == surface)
                    feat = computePPFHistogramFull<PointT>(cloud, radius);
                else
                    feat = computePPFHistogramFull<PointT>(cloud, surface, radius);
                return core::mapfeat<typename PPFHistogramFull<PointT>::Histogram>(*feat);

            } else if(al::iequals(name, "fpfh")) {

                pcl::PointCloud<FPFHT>::Ptr feat;
                if(cloud == surface)
                    feat = computeFPFH<PointT>(cloud, radius);
                else
                    feat = computeFPFH<PointT>(cloud, surface, radius);
                return core::mapfeat<FPFHT>(*feat);

            } else if(al::iequals(name, "shot")) {

                pcl::PointCloud<SHOTT>::Ptr feat;
                if(cloud == surface)
                    feat = computeSHOT<PointT>(cloud, radius);
                else
                    feat = computeSHOT<PointT>(cloud, surface, radius);
                return core::mapfeat<SHOTT>(*feat);

//            } else if(al::iequals(name, "cshot") ||
//                      al::iequals(name, "shotc") ||
//                      al::iequals(name, "SHOTColor") ||
//                      al::iequals(name, "ColorSHOT")) {
//
//                pcl::PointCloud<SHOTColorT>::Ptr feat;
//                if(cloud == surface)
//                    feat = computeSHOTColor<PointT>(cloud, radius);
//                else
//                    feat = computeSHOTColor<PointT>(cloud, surface, radius);
//                return core::mapfeat<SHOTColorT>(*feat);

            } else if(al::iequals(name, "si") ||
                      al::iequals(name, "SpinImage") ||
                      al::iequals(name, "SpinImages")) {

                pcl::PointCloud<SpinImageT>::Ptr feat;
                if(cloud == surface)
                    feat = computeSI<PointT>(cloud, radius);
                else
                    feat = computeSI<PointT>(cloud, surface, radius);
                return core::mapfeat<SpinImageT>(*feat);

            } else if(al::iequals(name, "usc") ||
                      al::iequals(name, "UniqueShapeContext")) {

                pcl::PointCloud<USCT>::Ptr feat;
                if(cloud == surface)
                    feat = computeUSC<PointT>(cloud, radius);
                else
                    feat = computeUSC<PointT>(cloud, surface, radius);
                return core::mapfeat<USCT>(*feat);

            } else {
                COVIS_THROW("Unknown feature: " << name << "!");
            }
        }

        /**
         * @ingroup feature
         * Compute a named feature for a point cloud
         * @param name name of the feature - see @ref getFeatureNames() for a list of possible features
         * @param cloud point cloud for which features should be computed
         * @param radius feature radius
         * @return features placed as observations in a matrix
         */
        template<typename PointT>
        inline MatrixT computeFeature(const std::string& name,
                                      typename pcl::PointCloud<PointT>::ConstPtr cloud,
                                      float radius) {
            return computeFeature<PointT>(name, cloud, cloud, radius);
        }

        /**
         * @ingroup feature
         * Compute multiple named features for a point cloud
         * @param names names of the features - see @ref getFeatureNames() for a list of possible features
         * @param cloud point cloud for which features should be computed
         * @param surface underlying surface used for computing features
         * @param radii feature radii
         * @return features placed as observations in a vector of matrices
         */
        template<typename PointT>
        inline MatrixVecT computeFeatures(const std::vector<std::string>& names,
                                          typename pcl::PointCloud<PointT>::ConstPtr cloud,
                                          typename pcl::PointCloud<PointT>::ConstPtr surface,
                                          const std::vector<float>& radii) {
            MatrixVecT result(names.size());
            if(names.size() == 1) { // Avoid nested OpenMP loops because many estimation methods use OpenMP inside
                for(size_t i = 0; i < names.size(); ++i)
                    result[i] = computeFeature<PointT>(names[i], cloud, surface, radii[i]);
            } else {
#ifdef _OPENMP
#pragma omp parallel for
#endif
                for(size_t i = 0; i < names.size(); ++i)
                    result[i] = computeFeature<PointT>(names[i], cloud, surface, radii[i]);
            }

            return result;
        }

        /**
         * @ingroup feature
         * Compute multiple named features for a point cloud
         * @param names names of the features - see @ref getFeatureNames() for a list of possible features
         * @param cloud point cloud for which features should be computed
         * @param radii feature radii
         * @return features placed as observations in a vector of matrices
         */
        template<typename PointT>
        inline MatrixVecT computeFeatures(const std::vector<std::string>& names,
                                          typename pcl::PointCloud<PointT>::ConstPtr cloud,
                                          const std::vector<float>& radii) {
            return computeFeatures<PointT>(names, cloud, cloud, radii);
        }

        /**
         * @ingroup feature
         * Compute a named feature for multiple point clouds, place them in columns after each other
         * @param name name of the feature - see @ref getFeatureNames() for a list of possible features
         * @param clouds point clouds for which features should be computed
         * @param surfaces underlying surfaces used for computing features
         * @param radius feature radii
         * @return features placed as observations in a matrix
         */
        template<typename PointT>
        inline MatrixT computeFeature(const std::string& name,
                                      const std::vector<typename pcl::PointCloud<PointT>::Ptr>& clouds,
                                      const std::vector<typename pcl::PointCloud<PointT>::Ptr>& surfaces,
                                      float radius) {
            COVIS_ASSERT(clouds.size() == surfaces.size());

            MatrixVecT tmp(clouds.size());
            if(clouds.size() == 1) { // Avoid nested OpenMP loops because many estimation methods use OpenMP inside
                for(size_t i = 0; i < clouds.size(); ++i)
                    tmp[i] = computeFeature<PointT>(name, clouds[i], surfaces[i], radius);
            } else {
#ifdef _OPENMP
#pragma omp parallel for
#endif
                for(size_t i = 0; i < clouds.size(); ++i)
                    tmp[i] = computeFeature<PointT>(name, clouds[i], surfaces[i], radius);
            }

            MatrixT result;
            for(size_t i = 0; i < tmp.size(); ++i) {
                size_t cols = result.cols();
                if(i == 0)
                    result.conservativeResize(tmp[i].rows(), result.cols() + tmp[i].cols());
                else
                    result.conservativeResize(Eigen::NoChange, result.cols() + tmp[i].cols());
                result.block(0, cols, tmp[i].rows(), tmp[i].cols()) = tmp[i];
            }


            return result;
        }

        /**
         * @ingroup feature
         * Compute a named feature for multiple point clouds, place them in columns after each other
         * @param name name of the feature - see @ref getFeatureNames() for a list of possible features
         * @param clouds point clouds for which features should be computed
         * @param radius feature radius
         * @return features placed as observations in a matrix
         */
        template<typename PointT>
        inline MatrixVecT computeFeature(const std::string& name,
                                         const std::vector<typename pcl::PointCloud<PointT>::Ptr>& clouds,
                                         float radius) {
            return computeFeature<PointT>(name, clouds, clouds, radius);
        }
    }
}

#endif
