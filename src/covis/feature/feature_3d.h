// Copyright (c) 2013, University of Southern Denmark
// All rights reserved.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice,
//    this list of conditions and the following disclaimer.
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation
//    and/or other materials provided with the distribution.
// 3. Neither the name of the University of Southern Denmark nor the names of
//    its contributors may be used to endorse or promote products derived from
//    this software without specific prior written permission.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL THE UNIVERSITY OF SOUTHERN DENMARK BE
// LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
// CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
// SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
// INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.

#ifndef COVIS_FEATURE_FEATURE_3D_H
#define COVIS_FEATURE_FEATURE_3D_H

// Own
#include "feature_base.h"
#include "../core/macros.h"

// PCL
#include <pcl/point_cloud.h>

namespace covis {
    namespace feature {
        /**
         * @class Feature3D
         * @ingroup feature
         * @brief Feature extraction base class for point cloud or 3D data
         *
         * Generally, shape features are computed using a spherical neighborhood, this is specified by @ref setRadius().
         * The main function @ref compute() estimates a feature for all points in the input point cloud based on a
         * radius search on the point cloud itself. However, it is also possible to specify an external surface, e.g.
         * with more detail, using @ref setSurface(). 
         *
         * @author Anders Glent Buch
         */
        template<typename PointT, typename FeatureT>
        class Feature3D : public FeatureBase {
            public:
                /// Empty constructor
                Feature3D() : _radius(-1.0f) {}

                /// Empty destructor
                virtual ~Feature3D() {}
                
                /**
                 * Main function: compute features at all points in the input cloud
                 * @note If a surface has been provided using @ref setSurface(), features are still computed at the
                 * input points, but now using the external surface
                 * @param cloud input point cloud
                 * @return features, one per input point
                 */
                virtual typename pcl::PointCloud<FeatureT>::Ptr compute(
                        typename pcl::PointCloud<PointT>::ConstPtr cloud) = 0;
                
                /**
                 * Compute features at indexed points on the external surface
                 * @param indices indexes into surface
                 * @return features, one per indexed point
                 */
                inline typename pcl::PointCloud<FeatureT>::Ptr compute(const std::vector<int>& indices) {
                    COVIS_ASSERT(_surface);
                    typename pcl::PointCloud<PointT>::Ptr cloud(
                            new typename pcl::PointCloud<PointT>(indices.size(), 1));
                    for(size_t i = 0; i < indices.size(); ++i)
                        cloud->points[i] = _surface->points[indices[i]];
                    
                    return compute(cloud);
                }
                
                /**
                 * Set an external surface for computing features
                 * @param surface external surface
                 */
                inline void setSurface(typename pcl::PointCloud<PointT>::ConstPtr surface) {
                    _surface = surface;
                }

                /**
                 * Set search radius
                 * @param radius search radius
                 */
                inline void setRadius(float radius) {
                    _radius = radius;
                }
                
            protected:
                /// External surface
                typename pcl::PointCloud<PointT>::ConstPtr _surface;
                
                /// Feature radius
                float _radius;
        };
    }
}

#endif
