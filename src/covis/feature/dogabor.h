// Copyright (c) 2013, University of Southern Denmark
// All rights reserved.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice,
//    this list of conditions and the following disclaimer.
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation
//    and/or other materials provided with the distribution.
// 3. Neither the name of the University of Southern Denmark nor the names of
//    its contributors may be used to endorse or promote products derived from
//    this software without specific prior written permission.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL THE UNIVERSITY OF SOUTHERN DENMARK BE
// LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
// CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
// SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
// INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.

#ifndef COVIS_FEATURE_DOGABOR_H
#define COVIS_FEATURE_DOGABOR_H

// OpenCV
#include <opencv2/opencv.hpp>

// Covis
#include "../filter/gabor_jet.h"

namespace covis {
    namespace feature {
        /**
         * @class DOGabor
         * @ingroup feature
         * @brief Depth oriented Gabor descriptor, using known detectors keypoints are found and using the depth map.
         * The normal vector for these points are found using the shape information. For each keypoint a transform is
         * found and the image is transformed into a viewpoint parallel with the image plane. Using the image patch the
         * Gabor response is calculated and returned as the descriptor.
         *
         * @todo Document this class, write the params properly
         *
         * @sa filter::GaborJet
         * @author Frederik Hagelskjær
         */
        class DOGabor {
            public:
                /// Type of keypoint detector to use
                enum Detector {
                    SIFT, SURF, FAST, EXTERNAL
                };

                /**
                 * Initialize depth oriented Gabor feature
                 * @param detector detector type
                 * @param f0 the frequency of the smallest gabor wavelet
                 * @param sigma the size of the ellipsoid defining the gabor wavelet
                 * @param scales number of scales used in the gabor bank
                 * @param rotations number of rotations used to cover 180 degrees in the gabor bank
                 * @param k scaling between each frequency in the gabor bank
                 * @param kernel_size the kernel size of the smallest gabor wavelet
                 * @param patch_size the size of the patch parallel to the image plane
                 * @param r_search size in centimeters of the radius defining points used for computing the normal vector
                 */
                DOGabor(Detector detector = FAST, float f0 = 0.2, float sigma = 0.795, int scales = 4, int rotations =
                        24, float k = 2, int kernel_size = 20, int patch_size = 320, float r_search = 0.05);

                /// Destructor
                ~DOGabor() {}

                /**
                 * Apply the feature detector and descriptor on an image and depth map, with known camera parameters
                 *
                 * @note If @b image has 3 channels, it is automatically converted to grayscale
                 * @note If the image is not set to original then each descriptor is added the number of rotations to
                 * acommodate for in plane rotation
                 * @note The internally used grayscale representation of the input image is normalized to the range
                 * [0,1] before filtering
                 *
                 * @param image input image
                 * @param depth depth information of the scene, must be an unsigned 16-bit integer image of depths in mm
                 * @param mask for limiting the area where descriptors are calculated
                 * @param K the camera matrix
                 * @param distance the set distance for the patch parallel to the image plane, if negative it is set to
                 * the average of all keypoints distance
                 * @param kp keypoints found by the detector, is overwritten
                 * @param desc the descriptor for all keypoints, is overwritten
                 * @param original if set to true only the descriptor at the keypoints is returned, if false then by
                 * setting different rotations as start point the number of descriptors is the number of rotations
                 **/
                void compute(const cv::Mat &image, const cv::Mat &depth, const cv::Mat &mask, cv::Matx33f K,
                        float & distance, std::vector<cv::KeyPoint> & kp, cv::Mat& desc, bool original);


                /**
                 * Apply the feature detector and descriptor on an image and depth map, with known camera parameters
                 *
                 * @note If @b image has 3 channels, it is automatically converted to grayscale
                 * @note If the image is not set to original then each descriptor is added the number of rotations to
                 * acommodate for in plane rotation
                 * @note The internally used grayscale representation of the input image is normalized to the range
                 * [0,1] before filtering
                 *
                 * @param image input image
                 * @param depth depth information of the scene, must be an unsigned 16-bit integer image of depths in mm
                 * @param K the camera matrix
                 * @param distance the set distance for the patch parallel to the image plane, if negative it is set to
                 * the average of all keypoints distance
                 * @param kp keypoints found by the detector, is overwritten
                 * @param desc the descriptor for all keypoints, is overwritten
                 * @param original if set to true only the descriptor at the keypoints is returned, if false then by
                 * setting different rotations as start point the number of descriptors is the number of rotations
                 **/
                inline void compute(const cv::Mat &image, const cv::Mat &depth, cv::Matx33f K,
                        float & distance, std::vector<cv::KeyPoint> & kp, cv::Mat& desc, bool original) {
                    compute(image, depth, cv::Mat(), K, distance, kp, desc, original);
                }

            private:
                /// the detector type used
                Detector _detector;

                /// the GaborJet matcher which is used
                filter::GaborJet _matcher;

                /// the number of scales used for the gabor bank
                int _scales;

                /// the number of rotations used for the gabor bank
                int _rotations;

                /// the size of the patch parallel to the image plane
                int _patchSize;

                /// size in centimeters of the radius defining points used for computing the normal vector
                float _rSearch;

                /// scaling between each frequency in the gabor bank
                float _k;

        };
    }
}

#endif
