// Copyright (c) 2013, University of Southern Denmark
// All rights reserved.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice,
//    this list of conditions and the following disclaimer.
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation
//    and/or other materials provided with the distribution.
// 3. Neither the name of the University of Southern Denmark nor the names of
//    its contributors may be used to endorse or promote products derived from
//    this software without specific prior written permission.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL THE UNIVERSITY OF SOUTHERN DENMARK BE
// LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
// CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
// SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
// INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.

#ifndef COVIS_FEATURE_NORMAL_CORRECTION_MANIFOLD_IMPL_HPP
#define COVIS_FEATURE_NORMAL_CORRECTION_MANIFOLD_IMPL_HPP

// Own
#include "normal_correction_manifold.h"
#include "../core/dist.h"
#include "../core/graph.h"

// STL
//#include <stack>
#include <queue>

// PCL
#include <pcl/common/centroid.h>
#include <pcl/search/kdtree.h>
#include <pcl/point_types.h>

namespace covis {
    namespace feature {
        template<typename PointNT>
        size_t NormalCorrectionManifold<PointNT>::compute(pcl::PointCloud<PointNT>& cloud) {
            COVIS_ASSERT(!cloud.empty());

            if(_convex) {
                Eigen::Vector4f c;
                pcl::compute3DCentroid(cloud, c);

                const Eigen::Vector3f c3 = c.head<3>();

                size_t flips = 0; // Number of normals flipped
                for(size_t i = 0; i < cloud.size(); ++i) {
                    const Eigen::Vector3f dir = cloud[i].getVector3fMap() - c3;
                    const Eigen::Vector3f n = cloud[i].getNormalVector3fMap();
                    if (dir.dot(n) < 0) {
                        flip(cloud[i]);
                        ++flips;
                    }
                }

                return flips;
            } else {
                // Find seed point as the point with largest positive z-component
                size_t seed = 0;
                float zmax = cloud[0].z;
                for (size_t i = 1; i < cloud.size(); ++i) {
                    const float nzi = cloud[i].z;
                    if (nzi > zmax) {
                        seed = i;
                        zmax = nzi;
                    }
                }

                // Bookkeeping variables
                size_t flips = 0; // Number of normals flipped

                // Visit the seed
                if (cloud[seed].normal_z < 0.0f) {
                    flip(cloud[seed]);
                    ++flips;
                }

                struct Visitor {
                    pcl::PointCloud<PointNT>& _cloud;
                    size_t& _flips;

                    Visitor(pcl::PointCloud<PointNT>& cloud, size_t& flips) : _cloud(cloud), _flips(flips) {}

                    inline void operator()(size_t ip, size_t ic) const {
                        const PointNT& parent = _cloud[ip];
                        PointNT& child = _cloud[ic];
                        if (core::dot<PointNT>(parent, child) < 0.0f) {
                            child.normal_x = -child.normal_x;
                            child.normal_y = -child.normal_y;
                            child.normal_z = -child.normal_z;
                            ++_flips;
                        }
                    }
                };

                Visitor visitor(cloud, flips);

                const std::vector<size_t> path = core::bfs<PointNT,Visitor>(cloud, _k, visitor, seed);

                // Issue a warning if not all points could be visited
                if(path.size() < cloud.size()) {
                    COVIS_MSG_WARN("Not all points could be reached (got " << path.size() << "/" << cloud.size() << ") - "
                                   "try using a larger k-value or radius!");
                }

                return flips;
            }
        }
    }
}

#endif
