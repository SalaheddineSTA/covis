// Copyright (c) 2013, University of Southern Denmark
// All rights reserved.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice,
//    this list of conditions and the following disclaimer.
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation
//    and/or other materials provided with the distribution.
// 3. Neither the name of the University of Southern Denmark nor the names of
//    its contributors may be used to endorse or promote products derived from
//    this software without specific prior written permission.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL THE UNIVERSITY OF SOUTHERN DENMARK BE
// LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
// CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
// SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
// INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.

#ifndef COVIS_DETECT_FEATURE_SEARCH_H
#define COVIS_DETECT_FEATURE_SEARCH_H

// Own
#include "search_base.h"
#include "../covis_config.h"
#include "../core/correspondence.h"

// PCL
#include <pcl/point_types.h>

// FLANN
#include <flann/flann.hpp>

// Eigen
#include <Eigen/Core>

// OpenMP
#ifdef _OPENMP
#include <omp.h>
#endif

namespace covis {
    namespace detect {
        /**
         * @ingroup detect
         * @class FeatureSearch
         * @brief FLANN-based feature matching
         * 
         * This class uses FLANN's randomized kd-trees for approximate nearest neighbor search in high-dimensional
         * (> 10) data. The approximation level is implicitly set by the number of checks the algorithm performs in the
         * trees (@ref setChecks()). For more information, see @cite muja2009fast.   
         * 
         * @note This class returns squared L2 distances
         * 
         * @tparam FeatureT feature type, e.g. <b>pcl::SHOT352</b>
         * @tparam DistT distance type, defaults to <b>flann::L2<float></b>
         * @author Anders Glent Buch
         */
        template<typename FeatureT, typename DistT=flann::L2<float> >
        class FeatureSearch : public SearchBase<FeatureT> {
            using SearchBase<FeatureT>::_target;
            
            public:
                /**
                 * Constructor: set number of randomized trees and a default number of checks
                 * @param trees number of randomized trees
                 */
                FeatureSearch(size_t trees = 4) :
                        _trees(trees),
                        _sparam(512),
#ifdef FLANN_1_8
                        _tree(flann::KDTreeIndexParams(trees)) {
#else
                        _tree(NULL) {
#endif
                    // Sanity check
                    COVIS_ASSERT(_trees > 0);
                    
                    // Set number of cores to use during search
#ifdef _OPENMP
                    _sparam.cores = omp_get_max_threads();
#endif
                }
                
                /**
                 * Destructor: clean up FLANN tree
                 */
                virtual ~FeatureSearch() {
                    if(_mtarget.ptr() != NULL)
                        delete[] _mtarget.ptr();
#ifndef FLANN_1_8
                    if(_tree != NULL)
                        delete _tree;
#endif
                }
                
                /**
                 * Set number of randomized trees
                 * @param trees number of randomized trees
                 */
                inline void setTrees(size_t trees) {
                    _trees = trees;
#ifdef FLANN_1_8
                    _tree = flann::Index<DistT>(flann::KDTreeIndexParams(_trees));
#endif
                    if(_mtarget.ptr() != NULL) {
                        COVIS_MSG_WARN("FLANN tree has already been built once! " <<
                                "You should call setTrees() before setTarget(). Rebuilding...");
                        delete[] _mtarget.ptr();
#ifndef FLANN_1_8
                        if(_tree != NULL)
                            delete _tree;
#endif
                        index();
                    }
                }
                
                /**
                 * Set number of checks to perform during search - set to <= 0 for infinite
                 * @param checks number of checks
                 */
                inline void setChecks(size_t checks) {
                    if(checks > 0)
                        _sparam.checks = int(checks);
                    else
                        _sparam.checks = -1;
                }

                /**
                 * @brief Perform a 2-NN search in feature space and return correspondences with the distance set to the ratio
                 * of the distance to the first match to the distance to the seconds, all squared for efficiency
                 * @param query query features
                 * @return matches as correspondences
                 */
                inline core::Correspondence::VecPtr ratio(const typename pcl::PointCloud<FeatureT>::ConstPtr& query) {
                    core::Correspondence::VecPtr c = this->knn(query, 2);
                    for(size_t i = 0; i < c->size(); ++i) {
                        // Handle invalids
                        float ratio;
                        if((*c)[i].size() == 2)
                            ratio = (*c)[i].distance[0] / (*c)[i].distance[1];
                        else
                            ratio = 1; // Natural upper bound for ratio matching

                        if(!(*c)[i].empty())
                            (*c)[i] = core::Correspondence((*c)[i].query, (*c)[i].match[0], ratio);
                    }

                    return c;
                }
                
            private:
                /// Element type for distance functor
                typedef typename DistT::ElementType ElementT;

                /// Result type for distance functor
                typedef typename DistT::ResultType ResultT;

                /// Number of randomized trees
                size_t _trees;
                
                /// FLANN search parameters
                flann::SearchParams _sparam;
                
                /// FLANN matrix of target features
                flann::Matrix<ElementT> _mtarget;
                
#ifdef FLANN_1_8
                /// Index type returned by FLANN
                typedef size_t idx_t;
                
                /// FLANN search tree
                flann::Index<DistT> _tree;
#else
                /// Index type returned by FLANN
                typedef int idx_t;
                
                /// FLANN search tree
                flann::Index<DistT>* _tree;
#endif
                
                /**
                 * Convert a single feature to a FLANN matrix, row-major
                 * @param query query feature
                 * @param mquery output FLANN matrix
                 */
                inline void convert(const FeatureT& query, flann::Matrix<ElementT>& mquery, bool& isnan) const {
                    isnan = false;

                    // Convert feature to array
                    ElementT* data = new ElementT[FeatureT::descriptorSize()];
                    const ElementT* fiter = reinterpret_cast<const ElementT*>(&query);
                    std::copy(fiter, fiter+FeatureT::descriptorSize(), data);
                    // Replace all NaNs by zeroes
                    for(int i = 0; i < FeatureT::descriptorSize(); ++i) {
                        if(isnanf(data[i])) {
                            data[i] = 0;
                            isnan = true;
                        }
                    }
                    // Create matrix header
                    mquery = flann::Matrix<ElementT>(data, 1, FeatureT::descriptorSize());
                }
                
                /**
                 * Convert a feature cloud to a FLANN matrix, row-major
                 * @param query query feature cloud
                 * @param mquery output FLANN matrix
                 */
                inline void convert(const typename pcl::PointCloud<FeatureT>& query, flann::Matrix<ElementT>& mquery,
                                    std::vector<bool>& nans) const {
                    // Convert features to array
                    ElementT* data = new ElementT[FeatureT::descriptorSize() * query.size()];
                    nans.resize(query.size(), false);
                    for(size_t i = 0; i < query.size(); ++i) {
                        ElementT* row = &data[i * FeatureT::descriptorSize()];
                        const ElementT* fiter = reinterpret_cast<const ElementT*>(&query[i]);
                        std::copy(fiter, fiter+FeatureT::descriptorSize(), row);
                        // Replace all NaNs by zeroes
                        for(int j = 0; j < FeatureT::descriptorSize(); ++j) {
                            if(isnanf(row[j])) {
                                row[j] = 0;
                                nans[i] = true;
                            }
                        }
                    }
                    // Create matrix header
                    mquery = flann::Matrix<ElementT>(data, query.size(), FeatureT::descriptorSize());
                }
                
                /// @copydoc SearchBase::index()
                void index();
                
                /// @copydoc SearchBase::doKnn()
                void doKnn(const FeatureT& query, size_t k, core::Correspondence& result) const;
                
                /// @copydoc SearchBase::doRadius()
                void doRadius(const FeatureT& query, float r, core::Correspondence& result) const;
        };
        
        /**
         * @ingroup detect
         * @brief Perform a k-NN search in feature space
         * @param query query features
         * @param target target features to search into
         * @param k number of neighbors to search for
         * @param trees number of randomized trees to use
         * @param checks number of checks to perform during search
         * @return matches as correspondences
         */
        template<typename FeatureT>
        inline core::Correspondence::VecPtr computeKnnMatches(typename pcl::PointCloud<FeatureT>::ConstPtr query,
                typename pcl::PointCloud<FeatureT>::ConstPtr target,
                size_t k = 1,
                size_t trees = 4,
                size_t checks = 512) {
            FeatureSearch<FeatureT> fm(trees);
            fm.setTarget(target);
            fm.setChecks(checks);
            
            return fm.knn(query, k);
        }

        /**
         * @ingroup detect
         * @brief Perform a radius search in feature space
         * @param query query features
         * @param target target features to search into
         * @param r search radius
         * @param trees number of randomized trees to use
         * @param checks number of checks to perform during search
         * @return matches as correspondences
         */
        template<typename FeatureT>
        inline core::Correspondence::VecPtr computeRadiusMatches(typename pcl::PointCloud<FeatureT>::ConstPtr query,
                                                                 typename pcl::PointCloud<FeatureT>::ConstPtr target,
                                                                 float r,
                                                                 size_t trees = 4,
                                                                 size_t checks = 512) {
            FeatureSearch<FeatureT> fm(trees);
            fm.setTarget(target);
            fm.setChecks(checks);

            return fm.radius(query, r);
        }
        
        /**
         * @ingroup detect
         * @brief Perform a 2-NN search in feature space and return correspondences with the distance set to the ratio
         * of the distance to the first match to the distance to the seconds, all squared for efficiency
         * @param query query features
         * @param target target features to search into
         * @param trees number of randomized trees to use
         * @param checks number of checks to perform during search
         * @return matches as correspondences
         */
        template<typename FeatureT>
        inline core::Correspondence::VecPtr computeRatioMatches(typename pcl::PointCloud<FeatureT>::ConstPtr query,
                typename pcl::PointCloud<FeatureT>::ConstPtr target,
                size_t trees = 4,
                size_t checks = 512) {
            FeatureSearch<FeatureT> fm(trees);
            fm.setTarget(target);
            fm.setChecks(checks);

            return fm.ratio(query);
        }

        /**
         * @ingroup detect
         * @brief Perform ratio matching between two sets of features and fuse the matches
         * @param query1 first set of query features
         * @param target1 first set of target features to search into
         * @param query2 second set of query features
         * @param target2 second set of target features to search into
         * @param trees number of randomized trees to use
         * @param checks number of checks to perform during search
         * @return matches as correspondences
         */
        template<typename Feature1T, typename Feature2T>
        inline core::Correspondence::VecPtr computeFusionMatches(typename pcl::PointCloud<Feature1T>::ConstPtr query1,
                                                                 typename pcl::PointCloud<Feature1T>::ConstPtr target1,
                                                                 typename pcl::PointCloud<Feature2T>::ConstPtr query2,
                                                                 typename pcl::PointCloud<Feature2T>::ConstPtr target2,
                                                                 size_t trees = 4,
                                                                 size_t checks = 512) {
            COVIS_ASSERT(query1->size() == query2->size());
            COVIS_ASSERT(target1->size() == target2->size());

            core::Correspondence::VecPtr corr1, corr2;

#ifdef _OPENMP
#pragma omp parallel sections
            {
#pragma omp section
                {
                    corr1 = computeRatioMatches<Feature1T>(query1, target1, trees, checks);
                }

#pragma omp section
                {
                    corr2 = computeRatioMatches<Feature2T>(query2, target2, trees, checks);
                }
            }
#else
            corr1 = computeRatioMatches<Feature1T>(query1, target1, trees, checks);
            corr2 = computeRatioMatches<Feature2T>(query2, target2, trees, checks);
#endif

            core::Correspondence::VecPtr corr(new core::Correspondence::Vec(corr1->size()));
            for(size_t i = 0; i < corr->size(); ++i) {
                if((*corr1)[i].empty() || (*corr2)[i].empty())
                    continue;

                if((*corr1)[i].distance[0] < (*corr2)[i].distance[0])
                    (*corr)[i] = (*corr1)[i];
                else
                    (*corr)[i] = (*corr2)[i];
            }

            return corr;
        }

        /**
         * @ingroup detect
         * @brief Perform ratio matching between three sets of features and fuse the matches
         * @param query1 first set of query features
         * @param target1 first set of target features to search into
         * @param query2 second set of query features
         * @param target2 second set of target features to search into
         * @param query3 third set of query features
         * @param target3 third set of target features to search into
         * @param trees number of randomized trees to use
         * @param checks number of checks to perform during search
         * @return matches as correspondences
         */
        template<typename Feature1T, typename Feature2T, typename Feature3T>
        inline core::Correspondence::VecPtr computeFusionMatches(typename pcl::PointCloud<Feature1T>::ConstPtr query1,
                                                                 typename pcl::PointCloud<Feature1T>::ConstPtr target1,
                                                                 typename pcl::PointCloud<Feature2T>::ConstPtr query2,
                                                                 typename pcl::PointCloud<Feature2T>::ConstPtr target2,
                                                                 typename pcl::PointCloud<Feature3T>::ConstPtr query3,
                                                                 typename pcl::PointCloud<Feature3T>::ConstPtr target3,
                                                                 size_t trees = 4,
                                                                 size_t checks = 512) {
            COVIS_ASSERT(query1->size() == query2->size());
            COVIS_ASSERT(query1->size() == query3->size());
            COVIS_ASSERT(target1->size() == target2->size());
            COVIS_ASSERT(target1->size() == target3->size());

            core::Correspondence::VecPtr corr1, corr2, corr3;

#ifdef _OPENMP
#pragma omp parallel sections
            {
#pragma omp section
                {

                    corr1 = computeRatioMatches<Feature1T>(query1, target1, trees, checks);
                }

#pragma omp section
                {
                    corr2 = computeRatioMatches<Feature2T>(query2, target2, trees, checks);
                }

#pragma omp section
                {
                    corr3 = computeRatioMatches<Feature3T>(query3, target3, trees, checks);
                }
            }
#else
            corr1 = computeRatioMatches<Feature1T>(query1, target1, trees, checks);
            corr2 = computeRatioMatches<Feature2T>(query2, target2, trees, checks);
            corr3 = computeRatioMatches<Feature3T>(query3, target3, trees, checks);
#endif

            core::Correspondence::VecPtr corr(new core::Correspondence::Vec(corr1->size()));
            for(size_t i = 0; i < corr->size(); ++i) {
                if((*corr1)[i].empty() || (*corr2)[i].empty() || (*corr3)[i].empty())
                    continue;

                if((*corr1)[i].distance[0] < (*corr2)[i].distance[0]) {
                    if((*corr1)[i].distance[0] < (*corr3)[i].distance[0])
                        (*corr)[i] = (*corr1)[i];
                    else
                        (*corr)[i] = (*corr3)[i];
                } else {
                    if((*corr2)[i].distance[0] < (*corr3)[i].distance[0])
                        (*corr)[i] = (*corr2)[i];
                    else
                        (*corr)[i] = (*corr3)[i];
                }
            }

            return corr;
        }
    }
}

#include "feature_search_impl.hpp"
    
#endif
