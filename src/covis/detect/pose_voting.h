// Copyright (c) 2013, University of Southern Denmark
// All rights reserved.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice,
//    this list of conditions and the following disclaimer.
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation
//    and/or other materials provided with the distribution.
// 3. Neither the name of the University of Southern Denmark nor the names of
//    its contributors may be used to endorse or promote products derived from
//    this software without specific prior written permission.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL THE UNIVERSITY OF SOUTHERN DENMARK BE
// LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
// CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
// SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
// INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.

#ifndef COVIS_DETECT_POSE_VOTING_H
#define COVIS_DETECT_POSE_VOTING_H

// Own
#include "detect_base.h"
#include "../core/correspondence.h"
#include "../core/detection.h"

// PCL
#include <pcl/common/common.h>

namespace covis {
    namespace detect {
        /**
         * @ingroup detect
         * @class PoseVoting
         * @brief Pose estimation based on pose voting
         *
         * This class uses a voting mechanism to perform pose estimation. The class requires as inputs an object model
         * with normals (the source), a scene model with normals (the target) and point to point correspondences. The
         * class then searches for a plausible pose using the normal information and the correspondences.
         *
         * The method is described in the paper @cite buch2017rotational.
         *
         * @tparam PointT point type
         * @author Anders Glent Buch
         * @sa @ref Ransac
         * 
         * @example example/pose_voting/pose_voting.cpp example/recognition/recognition.cpp
         */
        template<typename PointT>
        class PoseVoting : public DetectBase {
            public:
                /**
                 * Constructor: set default parameters:
                 *   - Translation bandwidth (@ref setTranslationBandwidth()): 0 (<= 0 means 10 x the target resolution)
                 *   - Rotation bandwidth (@ref setTranslationBandwidth()): \f$\pi/8\f$
                 *   - Tessellation (@ref setTessellation()): \f$2*\pi/60\f$
                 */
                PoseVoting() : _bwt(0),
                               _bwrot(22.5 * M_PI / 180.0),
                               _tessellation(6 * M_PI / 180.0) {}

                /**
                 * Constructor: set main parameters
                 * @param bwt translation bandwidth (0 means 10 x the target resolution)
                 * @param bwrot rotation bandwidth [rad] between 0 and pi
                 * @param tessellation final rotation angle tessellation [rad] between 0 and 2*pi
                 */
                PoseVoting(double bwt, double bwrot, double tessellation) {
                    _bwt = bwt;
                    _bwrot = bwrot;
                    _tessellation = tessellation;
                }

                /**
                 * Run normal voting detector
                 * @return best detection, if any was found - this can be verified directly in a boolean expression:
                 * @code
                 * covis::core::Detection d = estimate();
                 * if(d)
                 *   // Do something...
                 * @endcode
                 * @sa @ref getAllDetections()
                 */
                core::Detection estimate();
                
                /**
                 * Set the source point cloud
                 * @param source source point cloud
                 */
                inline void setSource(typename pcl::PointCloud<PointT>::ConstPtr source) {
                    _source = source;
                    PointT pmin, pmax;
                    pcl::getMinMax3D(*source, pmin, pmax);
                    _diag = (pmax.getVector3fMap() - pmin.getVector3fMap()).norm();
                }
                
                /**
                 * Set the target point cloud
                 * @param target target point cloud
                 */
                inline void setTarget(typename pcl::PointCloud<PointT>::ConstPtr target) {
                    _target = target;
                }
                
                /**
                 * Set point correspondences between the two point clouds, used for casting votes
                 * @param correspondences correspondences source --> target
                 */
                inline void setCorrespondences(core::Correspondence::VecConstPtr correspondences) {
                    _correspondences = correspondences;
                }

                /**
                 * Set translation bandwidth
                 * @param bwt translation bandwidth
                 */
                inline void setTranslationBandwidth(double bwt) {
                    _bwt = bwt;
                }

                /**
                 * Set rotation bandwidth
                 * @param bwrot rotation bandwidth [rad] between 0 and pi
                 */
                inline void setRotationBandwidth(double bwrot) {
                    _bwrot = bwrot;
                }

                /**
                 * Set tessellation of the final rotation angle when casting votes
                 * @param tessellation final rotation angle tessellation [rad] between 0 and 2*pi
                 */
                inline void setTessellation(double tessellation) {
                    _tessellation = tessellation;
                }
                
                /**
                 * Set verbose flag for printing
                 * @param verbose verbose flag
                 */
                inline void setVerbose(bool verbose) {
                    _verbose = verbose;
                }

                /**
                 * Get a non-mutable pointer to the translation votes generated during @ref estimate()
                 * @sa @ref getDensities()
                 * @return translation votes
                 */
                inline typename pcl::PointCloud<PointT>::ConstPtr getTranslationVotes() const {
                    return _translationVotes;
                }

                /**
                 * Get a non-mutable reference to the densities for the input correspondences
                 * @return kernel densities for the input correspondences
                 */
                inline const std::vector<float>& getDensities() const {
                    return _densities;
                }

                /**
                 * Get a non-mutable reference to all the valid detections, after applying non-maximum suppresion
                 * @return all detections
                 * @sa @ref estimate()
                 */
                inline const core::Detection::Vec& getAllDetections() const {
                    return _allDetections;
                }

            private:
                /// Cloud type
                typedef typename pcl::PointCloud<PointT> CloudT;

                /// Source point cloud to be placed into @ref _target
                typename CloudT::ConstPtr _source;

                /// Diameter of @ref _source
                float _diag;
                
                /// Target point cloud
                typename CloudT::ConstPtr _target;
                
                /// Correspondences source --> target
                core::Correspondence::VecConstPtr _correspondences;

                /// Translation bandwidth
                double _bwt;

                /// Rotation bandwidth
                double _bwrot;

                /// Tessellation of the unknown final rotation angle around the normal
                double _tessellation;
                
                /// Verbose flag
                bool _verbose;

                /// Translation votes generated by @ref estimate()
                typename CloudT::Ptr _translationVotes;

                /// Generated non-negative densities for the correspondences
                std::vector<float> _densities;

                /// All generated detections after non-maximum suppression
                core::Detection::Vec _allDetections;

                /**
                 * Compute votes using input correspondences
                 * @param voteT pose votes
                 * @param voteToCorrIdx indexing vote --> correspondence
                 */
                void computeVotes(core::Detection::MatrixVecT& voteT, std::vector<size_t>& voteToCorrIdx);
        };
    }
}

#ifndef COVIS_PRECOMPILE
#include "pose_voting_impl.hpp"
#endif

#endif
