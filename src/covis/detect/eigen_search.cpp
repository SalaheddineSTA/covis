// Copyright (c) 2013, University of Southern Denmark
// All rights reserved.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice,
//    this list of conditions and the following disclaimer.
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation
//    and/or other materials provided with the distribution.
// 3. Neither the name of the University of Southern Denmark nor the names of
//    its contributors may be used to endorse or promote products derived from
//    this software without specific prior written permission.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL THE UNIVERSITY OF SOUTHERN DENMARK BE
// LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
// CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
// SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
// INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.

#include "eigen_search.h"

covis::core::Correspondence::VecPtr covis::detect::knnSearch(const Eigen::MatrixXf& query,
                                                             const Eigen::MatrixXf& target,
                                                             size_t k,
                                                             size_t trees,
                                                             size_t checks) {
    EigenSearch<Eigen::MatrixXf> pm;
    pm.setTrees(trees);
    pm.setChecks(checks);
    pm.setTarget(target);

    return pm.knn(query, k);
}

covis::core::Correspondence::VecPtr covis::detect::radiusSearch(const Eigen::MatrixXf& query,
                                                                const Eigen::MatrixXf& target,
                                                                float r,
                                                                size_t trees,
                                                                size_t checks) {
    EigenSearch<Eigen::MatrixXf> pm;
    pm.setTrees(trees);
    pm.setChecks(checks);
    pm.setTarget(target);

    return pm.radius(query, r);
}

covis::core::Correspondence::VecPtr covis::detect::computeRatioMatches(const Eigen::MatrixXf& query,
                                                                       const Eigen::MatrixXf& target,
                                                                       size_t trees,
                                                                       size_t checks) {
    EigenSearch<Eigen::MatrixXf> fm;
    fm.setTrees(trees);
    fm.setTarget(target);
    fm.setChecks(checks);

    return fm.ratio(query);
}

covis::core::Correspondence::VecPtr covis::detect::computeFusionMatches(
        const std::vector<Eigen::MatrixXf, Eigen::aligned_allocator<Eigen::MatrixXf> >& query,
        const std::vector<Eigen::MatrixXf, Eigen::aligned_allocator<Eigen::MatrixXf> >& target,
        size_t trees,
        size_t checks) {
    // Sanity checks
    COVIS_ASSERT(query.size() > 0 && query.size() == target.size());

    // Compute all ratio matches
    std::vector<covis::core::Correspondence::VecPtr> corr(query.size());
#ifdef _OPENMP
#pragma omp parallel for
#endif
    for(size_t i = 0; i < query.size(); ++i)
        corr[i] = computeRatioMatches(query[i], target[i], trees, checks);

    return computeFusionMatches(corr);
}
