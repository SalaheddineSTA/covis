#ifndef COVIS_DETECT_TEMPLATE_MATCHING_H
#define COVIS_DETECT_TEMPLATE_MATCHING_H

#include <boost/array.hpp>

#include <opencv2/opencv.hpp>

#include "../core/macros.h"

namespace covis {
    namespace detect {

        /**
         * @ingroup detect
         * @class TemplateMatching
         * 
         * @brief 2D template matching using either intensity or edge templates
         * 
         * Template matching class based on either intensity differences or the edge-based chamfer distance. Start by loading a
         * set of templates using @ref loadTemplates(). Then you load a test image using @ref loadImage(). Finally you can apply
         * the algorithm using either @ref matchIntensity() or @ref matchEdges(). An important detail is that this class
         * requires you to specify the size of the test images a priori (@ref setWidth() and @ref setHeight()) in order to speed
         * up calculations by Fourier transforms.
         * 
         * The intensity matching in @ref matchIntensity() is based on the RMSE between grayscale values.
         * 
         * The edge matching in @ref matchEdges() is performed using the chamfer distance, which outputs the mean Euclidean
         * distance to the nearest edge in [px]. The matching is performed bi-directionally, meaning that the chamfer distance
         * is computed both for the template on the input image, but also for the input image patch on top of the template.
         * This, however, can be disabled using @ref setBidirectional()
         * 
         * @author Anders Glent Buch
         */
        class TemplateMatching {
            public:
                /**
                 * Constructor: set camera and edge extraction parameters
                 * @param width image width
                 * @param height image height
                 * @param lowEdgeThreshold lower edge magnitude threshold, set to 0 to use an automatic value
                 * @param highEdgeThreshold upper edge magnitude threshold, set to 0 to use an automatic value
                 */
                TemplateMatching(int width = 640, int height = 480, float lowEdgeThreshold = 75, float highEdgeThreshold = 150);
                
                /// Empty destructor
                virtual ~TemplateMatching() {}
                
                /**
                 * Set the width of the input images
                 * @warning This parameter must be set before loading templates!
                 * @param width number of columns
                 */
                inline void setWidth(int width) {
                    _width = width;
                    if(_roi.area() > 0) {
                    	COVIS_MSG_WARN("Image width altered - resetting ROI!");
                    	_roi = cv::Rect();
                    }
                }
                
                /**
                 * Set the height of the input images
                 * @warning This parameter must be set before loading templates!
                 * @param height number of rows
                 */
                inline void setHeight(int height) {
                    _height = height;
                    if(_roi.area() > 0) {
                    	COVIS_MSG_WARN("Image height altered - resetting ROI!");
                    	_roi = cv::Rect();
                    }
                }
                
                /**
                 * Set the kernel size of the blur filter used before edge extraction
                 * @param ksize blur kernel size
                 */
                inline void setBlurKernelSize(int ksize) {
                    COVIS_ASSERT_MSG(ksize >= 3 && (ksize % 2), "Blur kernel size must be >= 3 and odd!");
                    _ksize = ksize;
                }
                
                /**
                 * Set the normalization flag for the input image
                 * @param normalize normalization flag
                 */
                inline void setNormalize(bool normalize) {
                    _normalize = normalize;
                }
                
                /**
                 * Set the lower edge magnitude threshold
                 * @param lowEdgeThreshold lower edge magnitude threshold
                 */
                inline void setLowEdgeThreshold(float lowEdgeThreshold) {
                    _lowEdgeThreshold = lowEdgeThreshold;
                }
                
                /**
                 * Set the upper edge magnitude threshold
                 * @param highEdgeThreshold upper edge magnitude threshold
                 */
                inline void setHighEdgeThreshold(float highEdgeThreshold) {
                    _highEdgeThreshold = highEdgeThreshold;
                }
                
                /**
                 * Limit the search in the input images by a specified region of interest
                 * @param roi region of interest
                 */
                inline void setROI(cv::Rect roi) {
                	COVIS_ASSERT_MSG(roi.area() > 0, "Invalid ROI: " << roi);
                	_width = roi.width;
                	_height = roi.height;
                    _roi = roi;
                }

                /**
                 * Set bidirectional matching
                 * @param bidirectional bidirectional matching flag
                 */
                inline void setBidirectional(bool bidirectional) {
                	_bidirectional = bidirectional;
                }

                /**
                 * Load a set of templates for matching
                 * @param templates template images, must be single or multi channel 8-bit unsigned
                 * @param external if set to true, only extract external boundary edges
                 */
                void loadTemplates(const std::vector<cv::Mat>& templates, bool external = false);
                
                /**
                 * Load a set of templates for matching from files
                 * @param files template image files
                 * @param external if set to true, only extract external boundary edges
                 */
                inline void loadTemplates(const std::vector<std::string>& files, bool external = false) {
                    std::vector<cv::Mat> templates(files.size());
                    for(size_t i = 0; i < templates.size(); ++i)
                        templates[i] = cv::imread(files[i]);
                    
                    loadTemplates(templates, external);
                }
                
                /**
                 * Load a set of edge-only templates for matching in @ref matchEdges()
                 * @param templates edge template images, must be single channel 8-bit unsigned
                 */
                void loadEdgeTemplates(const std::vector<cv::Mat>& templates);
                
                /**
                 * Load a set of edge-only templates for matching in @ref matchEdges() from files
                 * @param files edge template image files
                 */
                inline void loadEdgeTemplates(const std::vector<std::string>& files) {
                    std::vector<cv::Mat> templates(files.size());
                    for(size_t i = 0; i < templates.size(); ++i)
                        templates[i] = cv::imread(files[i]);
                    
                    loadEdgeTemplates(templates);
                }
                
                /**
                 * Load a test image
                 * To start the matching, use one of the match* functions below
                 * @param image test image, must be single- or multi-channel 8-bit unsigned
                 * @param external if set to true, only extract external boundary edges
                 */
                void loadImage(const cv::Mat& image, bool external = false);
                
                /**
                 * Load a test image
                 * To start the matching, use one of the match* functions below
                 * @param file test image file
                 * @param external if set to true, only extract external boundary edges
                 */
                inline void loadImage(const std::string file, bool external = false) {
                    loadImage(cv::imread(file), external);
                }
                
                /**
                 * Run intensity based matching
                 * 
                 * This function returns results only for the best matching templates. Have a look at @ref getResponses() to get
                 * the result for the remaining templates.
                 * 
                 * @note Remember to load a set of templates and a test image using @ref loadTemplates() and @ref loadImage()
                 * 
                 * @sa matchEdges()
                 * 
                 * @param id index of the best matching template
                 * @param pmin translation of the best matching template
                 * @param min detection error, given as RMSE of intensity differences [0,1]
                 * @param response full error response for the whole image of the best template
                 */
                void matchIntensity(size_t& id, cv::Point& pmin, float& min, cv::Mat& response);
                
                /**
                 * Run edge based matching
                 * 
                 * This function returns results only for the best matching templates. Have a look at @ref getResponses() to get
                 * the result for the remaining templates.
                 * 
                 * @note Remember to load a set of templates and a test image using @ref loadTemplates() and @ref loadImage()
                 * 
                 * @sa matchIntensity()
                 * 
                 * @param id index of the best matching template
                 * @param pmin translation of the best matching template
                 * @param min detection error, given as the mean Euclidean distance in [px] from a template edge point
                 * to a test image edge point [0,inf]
                 * @param response full error response for the whole image of the best template
                 */
                void matchEdges(size_t& id, cv::Point& pmin, float& min, cv::Mat& response);
                
                /**
                 * Perform non-maximum suppression and retrieve all matches, sorted in ascending order of detection error
                 * @param vid indices of the matching templates
                 * @param vpmin translations of the matching templates
                 * @param vmin detection errors, see @ref matchEdges() or @ref matchIntensity()
                 */
                void getAllMatches(std::vector<size_t>& vid, std::vector<cv::Point>& vpmin, std::vector<float>& vmin) const;
                
                /**
                 * Return the complete set of template response images after running the matcher
                 * @return template maching responses
                 */
                inline const std::vector<cv::Mat>& getResponses() const {
                    return _responses;
                }

                /**
                 * Draw the result of a match
                 * @param image input image
                 * @param id index of the best matching template
                 * @param pmin center point of the detected object
                 * @param showROI if set to true (default), show the ROI
                 * @param showTemplate if set to true, the matching template is overlaid on the image in the bottom right corner - only use this flag if you are not showing multiple detections
                 */
                void drawDetection(cv::Mat& image, size_t id, const cv::Point& pmin, bool showROI = true, bool showTemplate = false) const;

                /**
                 * Draw the edges
                 * @param image input image
                 */
                void drawEdges(cv::Mat& image) const;
                
                /**
                 * Get a const reference to the original loaded color image
                 * @return intensity image
                 */
                inline const cv::Mat& getColor() const {
                    return _color;
                }
                
                /**
                 * Get a const reference to the generated intensity image
                 * @return intensity image
                 */
                inline const cv::Mat& getIntensity() const {
                    return _intensity;
                }
                
                /**
                 * Get a const reference to the generated edge image
                 * @return edge image
                 */
                inline const cv::Mat& getEdges() const {
                    return _edges;
                }
                
                /**
                 * Get a const reference to the generated intensity templates
                 * @return intensity templates
                 */
                inline const std::vector<cv::Mat>& getIntensityTemplates() const {
                    return _ti;
                }
                
                /**
                 * Get a const reference to the generated edge templates
                 * @return edge templates
                 */
                inline const std::vector<cv::Mat>& getEdgeTemplates() const {
                    return _te;
                }
                
                /**
                 * Compute image edge features
                 * @note This function uses the blur kernel and normalization parameters of this class for preprocessing
                 * @param image input image, must be single or multi channel 8-bit unsigned
                 * @param edges output edges, single channel 8-bit unsigned with a 255 at each edge point
                 * @param border if non-zero, add a black border of this many pixels before running the edge detector - this is useful for e.g. cropped templates
                 * @param external if set to true, only extract external boundary edges - also useful for some types of objects
                 */
                void computeEdges(const cv::Mat& image,
                        cv::Mat& edges,
						int border = 0,
						bool external = false) const;

                /**
                 * Compute image derivatives using a Sobel filter
                 * @note This function uses the blur kernel and normalization parameters of this class for preprocessing
                 * @param image input image, must be single or multi channel 8-bit unsigned
                 * @param dx horizontal derivatives
                 * @param dy vertical derivatives
                 * @param border if non-zero, add a black border of this many pixels before running the edge detector - this is useful for e.g. cropped templates 
                 * @param normalizeMagnitude if set to true, normalize all gradients to unit norm
                 */
                void computeDerivs(const cv::Mat& image,
                		cv::Mat& dx,
						cv::Mat& dy,
						int border = 0,
						bool normalizeMagnitude = false) const;
                
            private:
                /**
                 * Throw an exception if the image is empty or in an unsupported format
                 * @param image image to check
                 * @throw if image is invalid
                 */
                void checkImage(const cv::Mat& image);
                
                /**
                 * Compute a distance transform for an edge image
                 * @param edges input edge image, must be single channel 8-bit unsigned with a 255 at each edge point
                 * @param result distance transform, each pixel gives the L2 distance to the nearest edge in [px]
                 */
                void dt(const cv::Mat& edges, cv::Mat& result);
                
                /**
                 * Match the input image with the i'th intensity template
                 * @param i template index
                 * @return response
                 */
                cv::Mat matchi(size_t i);
                
                /**
                 * Match the input image with the i'th edge template
                 * @param i template index
                 * @return response
                 */
                cv::Mat matche(size_t i);
                
                /**
                 * @todo Non-maximum suppression: http://code.opencv.org/attachments/994/
                 * @param src image with scalar-valued scores
                 * @param sz size of suppression window
                 * @return Resulting maxima
                 */
                std::vector<cv::Point> nms(const cv::Mat& src, int sz) const;
                
                /// Input image width
                int _width;
                
                /// Input image height
                int _height;
                
                /// Blur kernel size
                int _ksize;
                
                /// Normalization flag for the input image
                int _normalize;
                
                /// Computed max template width
                int _twidth;
                
                /// Computed max template height
                int _theight;
                
                /// Lower edge magnitude gradient threshold
                float _lowEdgeThreshold;
                
                /// Upper edge magnitude gradient threshold
                float _highEdgeThreshold;
                
                /// Region of interest
                cv::Rect _roi;

                /// Bidirectional flag
                bool _bidirectional;

                std::vector<cv::Mat> _t; /// Original input (color) templates
                std::vector<cv::Mat> _ti; /// Intensity templates, float
                std::vector<cv::Mat> _te; /// Edge templates, float, 0 for black, 1 for white
                std::vector<cv::Mat> _dte; /// L2 distance transform of edge templates, float
                /// DFTs of the two above image vectors
                std::vector<cv::Mat> _tedft, _dtedft;

                cv::Mat _color; /// Original input (color) image
                cv::Mat _intensity; /// Intensity image, float
                cv::Mat _edges; /// Edge image, float, 0 at non-edges, 1 at edge
                cv::Mat _dt; /// L2 distance transform of edges, float
                /// DFTs of the two above images
                cv::Mat _edgesdft, _dtdft;
                
                /// Template matching responses
                std::vector<cv::Mat> _responses;
        };
    }
}
#endif
