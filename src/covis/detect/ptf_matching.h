// Copyright (c) 2016, University of Southern Denmark
// All rights reserved.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice,
//    this list of conditions and the following disclaimer.
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation
//    and/or other materials provided with the distribution.
// 3. Neither the name of the University of Southern Denmark nor the names of
//    its contributors may be used to endorse or promote products derived from
//    this software without specific prior written permission.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL THE UNIVERSITY OF SOUTHERN DENMARK BE
// LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
// CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
// SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
// INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.

#ifndef COVIS_DETECT_PTF_MATCHING_H
#define COVIS_DETECT_PTF_MATCHING_H

// Own
#include "../core/correspondence.h"
#include "../core/macros.h"
#include "../core/progress_display.h"
#include "../core/ptf.h"
#include "../core/range.h"
#include "../core/scoped_timer.h"
#include "feature_search.h"
#include "point_search.h"

// PCL
#include <pcl/io/io.h>
#include <pcl/registration/correspondence_rejection_sample_consensus.h>
#include <pcl/registration/transformation_estimation_svd.h>


namespace covis {
    namespace detect {
        /**
        * @class PTFMatching
        * @ingroup detect
        * @brief Class for find an known object pose in a scene using PTFs
        *
        * This class provides two methods of finding an object pose(-s) in a scene using
        * computed object and scene PTFs as input
        *
        * @todo  Add citation when paper is accepted
        *
        * @tparam PointInT input point type, must contain XYZ data
        *
        * @author Lilita Kiforenko
        */
        template<typename PointInT>
        class PTFMatching {
        public:

            PTFMatching() : _checks(512), _search_radius(0.16), _debug(false), _NN_threshold(0), _max_distance(0.1),
                            _use_ransac(false), _use_pose_clustering(true), _number_of_poses(1) {};

            /// Empty destructor
            virtual ~PTFMatching() {}

            /// Number of checks for kd-tree search
            void setNumberOfChecks(int checks){
                this->_checks = checks;
            }

            /// Radii for kd-tree matching covis::core::PTF search
            void setSearchRadius(float rad){
                this->_search_radius = rad;
            }

            /// Object model maximum distance
            void setMaxDistance(float d){
                this->_max_distance = d;
            }

            /// Set point cloud with query PTFs, computed from PTFExtraction
            void setPTFsQuery(pcl::PointCloud<covis::core::PTF> ptfs, pcl::PointCloud<covis::core::PTF_Index> ptfs_idx){
                this->_ptfs_query = ptfs;
                this->_ptfs_idx_query = ptfs_idx;
            }

            /// Set point cloud with target PTFs, computed from PTFExtraction
            void setPTFsTarget(pcl::PointCloud<covis::core::PTF> ptfs, pcl::PointCloud<covis::core::PTF_Index> ptfs_idx){
                this->_ptfs_target = ptfs;
                this->_ptfs_idx_target = ptfs_idx;
            }
            /// Set query keypoints for voting array size
            void setQueryKeypoints(typename pcl::PointCloud<PointInT>::Ptr cloud){
                this->_query = cloud;
            }

            /// Set target keypoints
            void setTargetKeypoints(typename pcl::PointCloud<PointInT>::Ptr cloud){
                this->_target = cloud;
            }

            /// Enable some printout information
            void setDebug(bool debug){
                this->_debug = debug;
            }

            /// Compute RANSAC pose
            void setUseRANSAC(bool ransac){
                this->_use_ransac = ransac;
            }
            /// Compute pose use Pose Clustering
            void setUsePoseClustering(bool clustering){
                this->_use_pose_clustering = clustering;
            }

            /// Set how many instances of the object to retrieve
            void setNumberOfPoses(int pose){
                this->_number_of_poses = pose;
            }

            /// Get found poses object poses using Pose Clustering
            std::vector<Eigen::Matrix4f> getPosesFromPoseClustering(){
                return this->_poses_from_pose_clustering;
            }
            /// Get found pose using RANSAC approach
            Eigen::Matrix4f getPoseFromRANSAC(){
                return this->_pose_from_ransac;
            }
            /// Get found point matches using RANSAC approach
            covis::core::Correspondence::Vec getRansacCorr(){
                return this->_corr_ransac;
            }
            /// Get found point matches using Pose Clustering approach
            covis::core::Correspondence::Vec getPoseClusteringCorr(){
                return this->_corr_pose_clustering;
            }

            void match();

        private:
            int _checks;
            float _search_radius;
            bool _debug;
            int _NN_threshold;
            float _max_distance;
            bool _use_ransac;
            bool _use_pose_clustering;
            int _number_of_poses;
            pcl::PointCloud<covis::core::PTF> _ptfs_query, _ptfs_target;
            pcl::PointCloud<covis::core::PTF_Index> _ptfs_idx_query, _ptfs_idx_target;
            typename pcl::PointCloud<PointInT>::Ptr _query, _target;
            covis::core::Correspondence::Vec _corr_ransac, _corr_pose_clustering;


            std::vector<Eigen::Matrix4f> _poses_from_pose_clustering;
            Eigen::Matrix4f _pose_from_ransac;

        };
    }
}

#ifndef COVIS_PRECOMPILE
#include "ptf_matching_impl.hpp"
#endif

#endif
