// Copyright (c) 2013, University of Southern Denmark
// All rights reserved.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice,
//    this list of conditions and the following disclaimer.
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation
//    and/or other materials provided with the distribution.
// 3. Neither the name of the University of Southern Denmark nor the names of
//    its contributors may be used to endorse or promote products derived from
//    this software without specific prior written permission.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL THE UNIVERSITY OF SOUTHERN DENMARK BE
// LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
// CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
// SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
// INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.

// Own
#include "eigen_search_binary.h"

void covis::detect::EigenSearchBinary::index(const feature::BinaryMatrixT& target) {
    const size_t samples = (target.IsRowMajor ? target.rows() : target.cols());
    _dim = (target.IsRowMajor ? target.cols() : target.rows());
    flann::Matrix<feature::BinaryMatrixT::Scalar> m(const_cast<feature::BinaryMatrixT::Scalar*>(target.data()), samples, _dim);
    _index.buildIndex(m);
}

covis::core::Correspondence::VecPtr covis::detect::EigenSearchBinary::knn(const feature::BinaryMatrixT& query, size_t k) const {
    const size_t samples = (query.IsRowMajor ? query.rows() : query.cols());
    const size_t dim (query.IsRowMajor ? query.cols() : query.rows());
    COVIS_ASSERT_MSG(dim == _dim, "Query matrix has inompatible dimensions with indexed matrix!");

    core::Correspondence::VecPtr result(new core::Correspondence::Vec(samples));

    flann::SearchParams param;
    param.sorted = _sort;

#ifdef _OPENMP
#pragma omp parallel for
#endif
    for(size_t i = 0; i < samples; ++i) {
        (*result)[i].query = i;

        bool isnan;
        if(query.IsRowMajor)
            isnan = query.row(i).hasNaN();
        else
            isnan = query.col(i).hasNaN();
        if(isnan) {
#ifdef _OPENMP
#pragma omp critical
#endif
            COVIS_MSG_WARN("NaN feature given to binary Eigen k-NN search - returning empty correspondence!");
            continue;
        }

        flann::Matrix<feature::BinaryMatrixT::Scalar> m(const_cast<feature::BinaryMatrixT::Scalar*>(query.data() + i * dim), 1, dim);
        std::vector<std::vector<size_t> > indices;
        std::vector<std::vector<BinaryIndexT::DistanceType> > dists;
        _index.knnSearch(m, indices, dists, k, param);

        (*result)[i].match = indices[0];
        (*result)[i].distance.assign(dists[0].begin(), dists[0].end());
    }

    return result;
}

covis::core::Correspondence::VecPtr covis::detect::EigenSearchBinary::radius(const feature::BinaryMatrixT& query, float r) const {
    const size_t samples = (query.IsRowMajor ? query.rows() : query.cols());
    const size_t dim (query.IsRowMajor ? query.cols() : query.rows());
    COVIS_ASSERT_MSG(dim == _dim, "Query matrix has inompatible dimensions with indexed matrix!");

    core::Correspondence::VecPtr result(new core::Correspondence::Vec(samples));

    flann::SearchParams param;
    param.sorted = _sort;

#ifdef _OPENMP
#pragma omp parallel for
#endif
    for(size_t i = 0; i < samples; ++i) {
        (*result)[i].query = i;

        bool isnan;
        if(query.IsRowMajor)
            isnan = query.row(i).hasNaN();
        else
            isnan = query.col(i).hasNaN();
        if(isnan) {
#ifdef _OPENMP
#pragma omp critical
#endif
            COVIS_MSG_WARN("NaN feature given to binary Eigen radius search - returning empty correspondence!");
            continue;
        }

        flann::Matrix<feature::BinaryMatrixT::Scalar> m(const_cast<feature::BinaryMatrixT::Scalar*>(query.data() + i * dim), 1, dim);
        std::vector<std::vector<size_t> > indices;
        std::vector<std::vector<BinaryIndexT::DistanceType> > dists;
        _index.radiusSearch(m, indices, dists, r, param);

        (*result)[i].match = indices[0];
        (*result)[i].distance.assign(dists[0].begin(), dists[0].end());
    }

    return result;
}

covis::core::Correspondence::VecPtr covis::detect::knnSearch(const feature::BinaryMatrixT& query,
                                                             const feature::BinaryMatrixT& target,
                                                             size_t k) {
    EigenSearchBinary pm;
    pm.setTarget(target);

    return pm.knn(query, k);
}

covis::core::Correspondence::VecPtr covis::detect::radiusSearch(const feature::BinaryMatrixT& query,
                                                                const feature::BinaryMatrixT& target,
                                                                float r) {
    EigenSearchBinary pm;
    pm.setTarget(target);

    return pm.radius(query, r);
}

covis::core::Correspondence::VecPtr covis::detect::computeRatioMatches(const feature::BinaryMatrixT& query,
                                                                       const feature::BinaryMatrixT& target) {
    EigenSearchBinary fm;
    fm.setTarget(target);

    return fm.ratio(query);
}

covis::core::Correspondence::VecPtr covis::detect::computeFusionMatches(
        const std::vector<feature::BinaryMatrixT, Eigen::aligned_allocator<feature::BinaryMatrixT> >& query,
        const std::vector<feature::BinaryMatrixT, Eigen::aligned_allocator<feature::BinaryMatrixT> >& target) {
    // Sanity checks
    COVIS_ASSERT(query.size() > 0 && query.size() == target.size());

    // Compute all ratio matches
    std::vector<covis::core::Correspondence::VecPtr> corr(query.size());
#ifdef _OPENMP
#pragma omp parallel for
#endif
    for(size_t i = 0; i < query.size(); ++i)
        corr[i] = computeRatioMatches(query[i], target[i]);

    return computeFusionMatches(corr);
}
