// Copyright (c) 2013, University of Southern Denmark
// All rights reserved.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice,
//    this list of conditions and the following disclaimer.
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation
//    and/or other materials provided with the distribution.
// 3. Neither the name of the University of Southern Denmark nor the names of
//    its contributors may be used to endorse or promote products derived from
//    this software without specific prior written permission.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL THE UNIVERSITY OF SOUTHERN DENMARK BE
// LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
// CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
// SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
// INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.

#ifndef COVIS_FILTER_INTENSITY_GRADIENT_H
#define COVIS_FILTER_INTENSITY_GRADIENT_H

// Own
#include "image_filter_base.h"

// STL
#include <cmath>

// OpenCV
#include <opencv2/core/core.hpp>

namespace covis {
    namespace filter {
        /**
         * @class IntensityGradient
         * @ingroup filter
         * @brief Intensity gradient for BGR or grayscale images
         *
         * This class applies two operations to an image to obtain an estimate of its directional derivatives. First,
         * a Gaussian presmoothing is applied using OpenCV's <b>GaussianBlur()</b> function. Then a separable Sobel
         * filter (1x3 kernel followed by 3x1 kernel) is applied to the blurred image using the OpenCV function
         * <b>Sobel()</b>, using the special kernel size value 1. The result of this is two images, one for each
         * directional derivative. Finally, these are converted to the polar representation (magnitude/orientation).
         * 
         * By default, this class is setup to use the same parameters as the Canny edge detector, meaning a Gaussian
         * smoothing with a standard deviation of 1.4. For more information, see @cite canny1986computational
         * and the Wikipedia page: http://en.wikipedia.org/wiki/Canny_edge_detector.
         * 
         * Presmoothing can be disabled simply by setting the standard deviation to zero.
         *
         * @author Anders Glent Buch
         * @example example/intensity_gradient/intensity_gradient.cpp
         */
        class IntensityGradient : public ImageFilterBase {
            public:
                /**
                 * Default constructor: set smoothing parameters suitable for the Canny edge detector
                 */ 
                IntensityGradient() : _sigma(1.4), _ksize(0, 0) {}

                /// Destructor
                virtual ~IntensityGradient() {}

                /**
                 * Compute the intensity gradient of a BGR or grayscale image
                 *
                 * @note if @b image has 3 channels, it is automatically converted to grayscale
                 *
                 * @param image input BGR or grayscale image, can have either integral or floating point elements
                 * @return a 2-channel double precision image representing magnitude and orientation:
                 *  - Magnitude range: @f$[0,\infty]@f$
                 *  - Orientation range: @f$[0,2\pi]@f$
                 */
                cv::Mat filter(const cv::Mat& image);
                
                /**
                 * Set standard deviation for smoothing (<= 0 for disabled)
                 * @param sigma standard deviation
                 */
                inline void setSigma(double sigma) {
                    _sigma = sigma;
                }
                
                /**
                 * Get standard deviation for smoothing
                 * @return standard deviation
                 */
                inline double getSigma() const {
                    return _sigma;
                }
                
                /**
                 * Set kernel size for smoothing (0 for automatic)
                 * @param ksize kernel size
                 */
                inline void setKernelSize(const cv::Size& ksize) {
                    _ksize = ksize;
                }
                
                /**
                 * Set kernel size for smoothing in both directions (0 for automatic)
                 * @param ksize kernel size
                 */
                inline void setKernelSize(size_t ksize) {
                    _ksize = cv::Size(ksize, ksize);
                }
                
                /**
                 * Get kernel size for smoothing
                 * @return kernel size
                 */
                inline const cv::Size& getKernelSize() const {
                    return _ksize;
                }

            private:
                
                /// Gaussian smoothing kernel standard deviation (<= 0 for disabled)
                double _sigma;
                
                /// Smoothing kernel size, 0 for automatic, otherwise it must be odd
                cv::Size _ksize;
        };
        
        /**
         * @ingroup filter
         * 
         * Compute the intensity gradient of an image using @ref IntensityGradient
         * 
         * @param image input BGR or grayscale image, can have either integral or floating point elements
         * @param sigma smoothing kernel standard deviation (<= 0 for disabled)
         * @param ksize smoothing kernel size (0 for automatic)
         * @return a 2-channel double precision image representing magnitude and orientation:
         *  - Magnitude range: @f$[0,\infty]@f$
         *  - Orientation range: @f$[0,2\pi]@f$
         */
        inline cv::Mat computeIntensityGradient(const cv::Mat& image,
                double sigma = std::sqrt(2),
                const cv::Size& ksize = cv::Size(0,0)) {
            IntensityGradient ig;
            ig.setSigma(sigma);
            ig.setKernelSize(ksize);
            
            return ig.filter(image);
        }
    }
}

#endif