// Copyright (c) 2013, University of Southern Denmark
// All rights reserved.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice,
//    this list of conditions and the following disclaimer.
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation
//    and/or other materials provided with the distribution.
// 3. Neither the name of the University of Southern Denmark nor the names of
//    its contributors may be used to endorse or promote products derived from
//    this software without specific prior written permission.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL THE UNIVERSITY OF SOUTHERN DENMARK BE
// LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
// CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
// SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
// INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.

#ifndef COVIS_FILTER_NON_MAXIMUM_SUPPRESSION_H
#define COVIS_FILTER_NON_MAXIMUM_SUPPRESSION_H

// Own
#include "point_cloud_filter_base.h"
#include "../core/macros.h"

// Boost
#include <boost/shared_ptr.hpp>

// PCL
#include <pcl/point_cloud.h>
#include <pcl/octree/octree_pointcloud.h>

namespace covis {
    namespace filter {
        /**
         * @class NonMaximumSuppression
         * @ingroup filter
         * @brief Perform non-maximum suppression on a set of 3D particles (points) based on a score value
         *
         * Remember to use @ref setScores(), @ref setSuppressionRadius() and potentially @ref setSuppressionFactor()
         * before calling @ref NonMaximumSuppression::filter() "filter()" on this class
         *
         * @tparam PointT input point type, must contain XYZ data
         * @author Anders Glent Buch
         */
        template<typename PointT>
        class NonMaximumSuppression : public PointCloudFilterBase<PointT> {
            public:
                /// Pointer type
                typedef typename boost::shared_ptr<NonMaximumSuppression<PointT> > Ptr;
                
                /**
                 * Constructor: set main parameters
                 * @param suppressionRadius suppression radius
                 * @param suppressionFactor suppression factor
                 */
                NonMaximumSuppression(float suppressionRadius = 0, float suppressionFactor = 0.1) :
                        _suppressionRadius(suppressionRadius),
                        _suppressionFactor(suppressionFactor) {}
                
                /// Destructor
                virtual ~NonMaximumSuppression() {}
                
                /// @copydoc PointCloudFilterBase::filter()
                typename pcl::PointCloud<PointT>::Ptr filter(typename pcl::PointCloud<PointT>::ConstPtr cloud);

                /**
                 * Set the input scores for each particle to use during suppression
                 * @param scores particle scores
                 */
                inline void setScores(const std::vector<float>& scores) {
                    _scores = scores;
                }

                /**
                 * Set metric suppression radius
                 * @param suppressionRadius suppression radius
                 */
                inline void setSuppressionRadius(float suppressionRadius) {
                    _suppressionRadius = suppressionRadius;
                }

                /**
                 * Set suppression factor
                 * @param suppressionFactor suppression factor
                 */
                inline void setSuppressionFactor(float suppressionFactor) {
                    _suppressionFactor = suppressionFactor;
                }
                
                /**
                 * Get mask - only valid after a call to @ref filter()
                 * @return suppression mask
                 */
                inline const std::vector<bool>& getMask() const {
                    return _mask;
                }
                
            protected:
                /// Input scores for each particle
                std::vector<float> _scores;

                /// Suppression radius: suppress all neighbor particles with a lower score within this Euclidean radius
                float _suppressionRadius;

                /// Suppression factor: suppress all particles with a score less than this factor times the max score
                float _suppressionFactor;

                /// Suppression mask, each non-suppressed particle will have a true
                std::vector<bool> _mask;
        };
    }
}

#ifndef COVIS_PRECOMPILE
#include "non_maximum_suppression_impl.hpp"
#endif

#endif