// Copyright (c) 2013, University of Southern Denmark
// All rights reserved.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice,
//    this list of conditions and the following disclaimer.
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation
//    and/or other materials provided with the distribution.
// 3. Neither the name of the University of Southern Denmark nor the names of
//    its contributors may be used to endorse or promote products derived from
//    this software without specific prior written permission.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL THE UNIVERSITY OF SOUTHERN DENMARK BE
// LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
// CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
// SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
// INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.

// Own
#include "visu_3d.h"

// Boost
#include <boost/make_shared.hpp>

using namespace covis::visu;

void Visu3D::addPointCloud(pcl::PCLPointCloud2::ConstPtr cloud, const std::string& title) {
    if(pcl::getFieldIndex(*cloud, "rgb") >= 0 || pcl::getFieldIndex(*cloud, "rgba") >= 0) {
        if(pcl::getFieldIndex(*cloud, "normal_x") >= 0) {
            pcl::PointCloud<pcl::PointXYZRGBNormal>::Ptr pcloud(new pcl::PointCloud<pcl::PointXYZRGBNormal>);
            pcl::fromPCLPointCloud2(*cloud, *pcloud);
            addPointCloud<pcl::PointXYZRGBNormal>(pcloud, title);
        } else {
            pcl::PointCloud<pcl::PointXYZRGB>::Ptr pcloud(new pcl::PointCloud<pcl::PointXYZRGB>);
            pcl::fromPCLPointCloud2(*cloud, *pcloud);
            addPointCloud<pcl::PointXYZRGB>(pcloud);
        }
    } else if(pcl::getFieldIndex(*cloud, "normal_x") >= 0) {
        pcl::PointCloud<pcl::PointNormal>::Ptr pcloud(new pcl::PointCloud<pcl::PointNormal>);
        pcl::fromPCLPointCloud2(*cloud, *pcloud);
        addPointCloud<pcl::PointNormal>(pcloud);
    } else if(pcl::getFieldIndex(*cloud, "intensity") >= 0) {
        if(pcl::getFieldIndex(*cloud, "normal_x") >= 0) {
            pcl::PointCloud<pcl::PointXYZINormal>::Ptr pcloud(new pcl::PointCloud<pcl::PointXYZINormal>);
            pcl::fromPCLPointCloud2(*cloud, *pcloud);
            addPointCloud<pcl::PointXYZINormal>(pcloud);
        } else {
            pcl::PointCloud<pcl::PointXYZI>::Ptr pcloud(new pcl::PointCloud<pcl::PointXYZI>);
            pcl::fromPCLPointCloud2(*cloud, *pcloud);
            addPointCloud<pcl::PointXYZI>(pcloud);
        }
    } else {
        pcl::PointCloud<pcl::PointXYZ>::Ptr pcloud(new pcl::PointCloud<pcl::PointXYZ>);
        pcl::fromPCLPointCloud2(*cloud, *pcloud);
        addPointCloud<pcl::PointXYZ>(pcloud);
    }
}

void Visu3D::addMesh(pcl::PolygonMesh::ConstPtr mesh, const std::string& label) {
    if(mesh->polygons.empty()) {
        addPointCloud(boost::make_shared<pcl::PCLPointCloud2>(mesh->cloud), label);
    } else {
        visu.addPolygonMesh(*mesh, label);
        getMeshActor(label)->GetProperty()->SetInterpolationToPhong();
    }
}

void Visu3D::addPose(const Eigen::Matrix4f& T, float length, const std::string& label) {
    pcl::PointXYZ p(T(0,3), T(1,3), T(2,3));
    pcl::PointXYZ px = p;
    pcl::PointXYZ py = p;
    pcl::PointXYZ pz = p;
    
    px.getVector3fMap() += T.block<3,1>(0,0) * length;
    py.getVector3fMap() += T.block<3,1>(0,1) * length;
    pz.getVector3fMap() += T.block<3,1>(0,2) * length;
    
    visu.addLine(p, px, 1.0, 0.0, 0.0, label + "_x");
    visu.addLine(p, py, 0.0, 1.0, 0.0, label + "_y");
    visu.addLine(p, pz, 0.0, 0.0, 1.0, label + "_z");
}

void Visu3D::addText(const std::string& text,
                     unsigned char r,
                     unsigned char g,
                     unsigned char b,
                     int offset,
                     int fontSize,
                     const std::string& id) {

    if(offset < 0)
        offset = _textOffset++ * 20 + 5;
    visu.addText(text,
                 5, offset, fontSize,
                 double(r) / 255.0, double(g) / 255.0, double(b) / 255.0,
                 id);
}

void Visu3D::jet(double v, double& r, double& g, double& b) {
    COVIS_ASSERT(v >= 0.0 && v <= 1.0);
    r = g = b = 1.0;
    if (v < 0.25) {
       r = 0;
       g = 4 * v;
    } else if (v < 0.5) {
       r = 0;
       b = 1 + 4 * (0.25 - v);
    } else if (v < 0.75) {
       r = 4 * (v - 0.5);
       b = 0;
    } else {
       g = 1 + 4 * (0.75 - v);
       b = 0;
    }
}

void Visu3D::redgreen(double v, double& r, double& g, double& b) {
    COVIS_ASSERT(v >= 0.0 && v <= 1.0);
    b = 0;
    r = (1 - v);
    g = v;
}

void covis::visu::showPointCloud(const Eigen::MatrixXf& xyz, const std::string& title) {
    COVIS_ASSERT(xyz.cols() == 3);
    Visu3D vb(title);
    pcl::PointCloud<pcl::PointXYZ>::Ptr cloud(new pcl::PointCloud<pcl::PointXYZ>(xyz.rows(), 1));
    for(Eigen::MatrixXf::Index i = 0; i < xyz.rows(); ++i)
        cloud->points[i].getVector3fMap() = xyz.row(i);
    vb.addPointCloud<pcl::PointXYZ>(cloud);
    vb.show();
}

void covis::visu::showPointCloud(const pcl::PCLPointCloud2::Ptr& cloud, const std::string& title) {
    Visu3D vb(title);
    vb.addPointCloud(cloud);
    vb.show();
}

void covis::visu::showMesh(pcl::PolygonMesh::ConstPtr mesh, const std::string& title) {
    Visu3D vb(title);
    vb.addMesh(mesh);
    vb.show();
}

void covis::visu::showMeshNormals(pcl::PolygonMesh::ConstPtr mesh,
                     size_t level,
                     float scale,
                     const std::string& title) {
    Visu3D vb(title);
    vb.addMesh(mesh);
    pcl::PointCloud<pcl::PointNormal>::Ptr cloud(new pcl::PointCloud<pcl::PointNormal>);
    pcl::fromPCLPointCloud2(mesh->cloud, *cloud);
    if(scale > 0)
        vb.addNormals<pcl::PointNormal>(cloud, level, scale);
    else
        vb.addNormals<pcl::PointNormal>(cloud, level, 10 * detect::computeResolution<pcl::PointNormal>(cloud));
    vb.show();
}

void covis::visu::showNormals(const pcl::PCLPointCloud2::Ptr& cloud,
                              size_t level,
                              float scale,
                              const std::string& title) {
    pcl::PointCloud<pcl::PointNormal>::Ptr pcloud(new pcl::PointCloud<pcl::PointNormal>);
    pcl::fromPCLPointCloud2(*cloud, *pcloud);
    showNormals<pcl::PointNormal>(pcloud, level, scale, title);
}

void covis::visu::showPoses(const std::vector<Eigen::Matrix4f>& T, float length, const std::string& title) {
    Visu3D vb(title);
    vb.addPoses(T, length);
    vb.show();
}
