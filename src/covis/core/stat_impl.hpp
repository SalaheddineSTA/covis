// Copyright (c) 2013, University of Southern Denmark
// All rights reserved.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice,
//    this list of conditions and the following disclaimer.
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation
//    and/or other materials provided with the distribution.
// 3. Neither the name of the University of Southern Denmark nor the names of
//    its contributors may be used to endorse or promote products derived from
//    this software without specific prior written permission.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL THE UNIVERSITY OF SOUTHERN DENMARK BE
// LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
// CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
// SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
// INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.

#ifndef COVIS_CORE_STAT_IMPL_HPP
#define COVIS_CORE_STAT_IMPL_HPP

// Own
#include "macros.h"

// STL
#include <numeric>

namespace covis {
    namespace core {
        template<typename RealT>
        RealT mean(const std::vector<RealT>& v) {
            COVIS_ASSERT(!v.empty());

            // TODO: Do this with double precision, then downcast in the end
            double result = 0.0;
            for (typename std::vector<RealT>::const_iterator it = v.begin(); it != v.end(); ++it)
                result += *it;
            result /= double(v.size());
            
            return RealT(result);
        }
        
        template<typename RealT>
        RealT var(const std::vector<RealT>& v, RealT mean, bool unbiased) {
            COVIS_ASSERT(v.size() >= 2);
            
            // TODO: Do this with double precision, then downcast in the end
            double result = 0.0;
            for(typename std::vector<RealT>::const_iterator it = v.begin(); it != v.end(); ++it) {
                const double diff = *it - mean;
                result += diff * diff;
            }
            result /= (unbiased ? double(v.size() - 1) : double(v.size()));
            
            return RealT(result);
        }
        
        template<typename RealT>
        RealT median(const std::vector<RealT>& v) {
            COVIS_ASSERT(!v.empty());
            switch(v.size()) {
                case 1:
                    return v[0];
                case 2:
                    return RealT(0.5*(v[0] + v[1]));
                default: {
                    std::vector<RealT> vv = v; // Modified sequence
                    typename std::vector<RealT>::iterator middle = vv.begin() + ptrdiff_t(v.size() / 2);
                    std::nth_element(vv.begin(), middle, vv.end());
                    if(v.size() % 2) // Odd
                        return *middle;
                    else
                        return RealT( 0.5 * (*middle + *std::max_element(vv.begin(), middle - 1)) );
                }
            }
        }
        
        template<typename RealT>
        void hist(const typename std::vector<RealT>& data,
                size_t bins,
                size_t* result,
                RealT lower,
                RealT upper) {
            // Sanity check
            COVIS_ASSERT(bins > 0);
            
            // Initialize result to zero
            std::memset(result, 0, bins * sizeof(size_t));
            
            // Increment
            const RealT inc = RealT(bins) / (upper - lower);
            
            // Compute absolute histogram
            for(typename std::vector<RealT>::const_iterator it = data.begin(); it != data.end(); ++it) {
                const size_t idx = MAX(0, MIN(bins - 1, size_t((*it - lower) * inc)));
                ++result[idx];
            }
        }
        
        template<typename RealT>
        void rhist(const typename std::vector<RealT>& data,
                size_t bins,
                RealT* result,
                RealT lower,
                RealT upper) {
            // Sanity check
            COVIS_ASSERT(bins > 0);

            // Special case
            if(data.empty()) {
                std::fill(result, result + bins, RealT(0));
                return;
            }
            
            // Compute absolute histogram
            size_t histabs[bins];
            hist(data, bins, histabs, lower, upper);
            
            // Normalize
            for(size_t i = 0; i < bins; ++i)
                result[i] = RealT(histabs[i]) / RealT(data.size());
        }
        
        template<typename RealT>
        void hist2(const typename std::vector<RealT>& data1,
                const typename std::vector<RealT>& data2,
                size_t bins1,
                size_t bins2,
                size_t* result,
                RealT lower1,
                RealT upper1,
                RealT lower2,
                RealT upper2) {
            // Sanity checks
            COVIS_ASSERT(data1.size() == data2.size());
            COVIS_ASSERT(bins1 > 0 && bins2 > 0);

            // Total number of bins
            const size_t bins = bins1 * bins2;
            
            // Initialize result to zero
            std::fill(result, result + bins, size_t(0));
            
            // Increments
            const RealT inc1 = RealT(bins1) / (upper1 - lower1);
            const RealT inc2 = RealT(bins2) / (upper2 - lower2);
            
            // Compute absolute 2D histogram
            for(size_t i = 0; i < data1.size(); ++i) {
                const size_t idx1 = MAX(0, MIN(bins1 - 1, size_t((data1[i] - lower1) * inc1)));
                const size_t idx2 = MAX(0, MIN(bins2 - 1, size_t((data2[i] - lower2) * inc2)));
                
                // Row-major storage: stride equal to second dimension
                ++result[idx1 * bins2 + idx2];
            }
        }

        template<typename RealT>
        void rhist2(const typename std::vector<RealT>& data1,
                    const typename std::vector<RealT>& data2,
                    size_t bins1,
                    size_t bins2,
                    RealT* result,
                    RealT lower1,
                    RealT upper1,
                    RealT lower2,
                    RealT upper2) {
            // Number of bins
            const size_t bins = bins1 * bins2;

            // Special case
            if(data1.empty()) {
                std::fill(result, result + bins, RealT(0));
                return;
            }

            // Compute absolute histogram
            size_t histabs[bins];
            hist2(data1, data2, bins1, bins2, histabs, lower1, upper1, lower2, upper2);

            // Normalize
            for(size_t i = 0; i < bins; ++i)
                result[i] = RealT(histabs[i]) / RealT(data1.size());
        }

        template<typename RealT>
        void histn(const typename std::vector<std::vector<RealT> >& data,
                   const std::vector<size_t>& bins,
                   size_t* result,
                   const typename std::vector<RealT>& lower,
                   const typename std::vector<RealT>& upper) {
            // Sanity check
            COVIS_ASSERT(!data.empty());
            // Number of dimensions (histogram axes)
            const size_t dims = data.size();
            COVIS_ASSERT(bins.size() == dims);
            COVIS_ASSERT(lower.size() == dims);
            COVIS_ASSERT(upper.size() == dims);
            // Number of data points
            const size_t size = data[0].size();
            for(size_t i = 1; i < data.size(); ++i)
                COVIS_ASSERT(data[i].size() == size);
            for(size_t i = 0; i < dims; ++i)
                COVIS_ASSERT(bins[i] > 0);

            // Increments
            typename std::vector<RealT> inc(dims);
            for(size_t i = 0; i < dims; ++i)
                inc[i] = RealT(bins[i]) / (upper[i] - lower[i]);

            // Convert all bin sizes to signed
            std::vector<int> sizes(dims);
            std::copy(bins.begin(), bins.end(), sizes.begin());

            // Initialize OpenCV container
            cv::Mat m(dims, sizes.data(), CV_16U);
            COVIS_ASSERT(m.isContinuous());
            std::fill(m.ptr<ushort>(), m.ptr<ushort>() + m.total(), 0);

            // Compute absolute n-D histogram
            for(size_t i = 0; i < size; ++i) {
                std::vector<int> idx(dims);
                for(size_t j = 0; j < dims; ++j)
                    idx[j] = MAX(0, MIN(bins[j] - 1, size_t((data[j][i] - lower[j]) * inc[j])));

                ++m.at<ushort>(idx.data());
            }

            // Copy from OpenCV container to result
            COVIS_ASSERT(m.isContinuous());
            std::copy(m.ptr<ushort>(), m.ptr<ushort>() + m.total(), result);
        }

        template<typename RealT>
        void rhistn(const typename std::vector<std::vector<RealT> >& data,
                    const std::vector<size_t>& bins,
                    RealT* result,
                    const typename std::vector<RealT>& lower,
                    const typename std::vector<RealT>& upper) {
            // Sanity check
            COVIS_ASSERT(!data.empty());

            // Number of dimensions (histogram axes)
            const size_t dims = data.size();

            // Total number of histogram bins
            size_t binss = 1;
            for(size_t i = 0; i < dims; ++i)
                binss *= bins[i];

            // Compute absolute n-D histogram
            size_t histabs[binss];
            histn(data, bins, histabs, lower, upper);

            // Normalize
            for(size_t i = 0; i < binss; ++i)
                result[i] = RealT(histabs[i]) / RealT(data[0].size());
        }
    }
}
#endif
