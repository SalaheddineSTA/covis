// Copyright (c) 2013, University of Southern Denmark
// All rights reserved.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice,
//    this list of conditions and the following disclaimer.
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation
//    and/or other materials provided with the distribution.
// 3. Neither the name of the University of Southern Denmark nor the names of
//    its contributors may be used to endorse or promote products derived from
//    this software without specific prior written permission.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL THE UNIVERSITY OF SOUTHERN DENMARK BE
// LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
// CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
// SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
// INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.

#ifndef COVIS_CORE_LINE_SEGMENT_3D_H
#define COVIS_CORE_LINE_SEGMENT_3D_H

#include "primitive_3d.h"
#include "line_segment_2d.h"

//OpenCV
#include <opencv2/imgproc/imgproc.hpp>

//PCL
#include <pcl/point_types.h>

namespace covis {
    namespace core {
        class LineSegment3D : public core::Primitive3D {

            public:
                /// Empty constructor
                LineSegment3D() {}
                /// Empty destructor
                virtual ~LineSegment3D() {}
                /**
                 * Parameter constructor
                 * @param ls2d reference to LineSegment2D
                 */
                LineSegment3D(LineSegment2D ls2d) {
                    this->prim2d = &ls2d;
                }

                inline LineSegment2D* getLineSegment2D() {
                    return static_cast<LineSegment2D*> (this->prim2d);
                }
        };
    }
}

POINT_CLOUD_REGISTER_POINT_STRUCT(covis::core::LineSegment3D,
        (float, x, x)
        (float, y, y)
        (float, z, z)
        (uint32_t, rgba, rgba)
        (float, normal[0], normal_x)
        (float, normal[1], normal_y)
        (float, normal[2], normal_z)
)

#endif /* COVIS_CORE_LINE_SEGMENT_3D_H */
