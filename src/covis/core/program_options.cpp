// Own
#include "program_options.h"

#include <iomanip>

using namespace covis::core;

void ProgramOptions::addPositional(const std::string& name, const std::string& desc, int maxnum) {
    COVIS_ASSERT_MSG(_vm.empty(), "Options already parsed, new options cannot be added!");
    COVIS_ASSERT(!name.empty());
    COVIS_ASSERT(maxnum != 0);

    if(maxnum < 0 || maxnum > 1)
        _hidden.add_options()(name.c_str(), po::value<std::vector<std::string> >(), desc.c_str());
    else
        _hidden.add_options()(name.c_str(), po::value<std::string>(), desc.c_str());
    _positionals.add(name.c_str(), maxnum);
    _posvec.push_back(std::make_pair(name, desc));

    _maxlen = std::max<size_t>(_maxlen, name.size() + 2);
}

void ProgramOptions::addFlag(char f, const std::string& name, const std::string& desc) {
    COVIS_ASSERT_MSG(_vm.empty(), "Options already parsed, new options cannot be added!");

    _flags.add_options()((name + "," + f).c_str() , desc.c_str());

    _maxlen = std::max<size_t>(_maxlen, name.size());
}

void ProgramOptions::addFlag(char f, const std::string& desc) {
    COVIS_ASSERT_MSG(_vm.empty(), "Options already parsed, new options cannot be added!");

    addFlag(f, std::string(&f, 1), desc);
}

void ProgramOptions::addFlag(const std::string& name, const std::string& desc) {
    COVIS_ASSERT_MSG(_vm.empty(), "Options already parsed, new options cannot be added!");

    _flags.add_options()(name.c_str() , desc.c_str());

    _maxlen = std::max<size_t>(_maxlen, name.size());
}

void ProgramOptions::addOption(const std::string& name, const std::string& desc) {
    COVIS_ASSERT_MSG(_vm.empty(), "Options already parsed, new options cannot be added!");
    COVIS_ASSERT(!name.empty());

    _options.add_options()(name.c_str(), po::value<std::string>(), desc.c_str());

    _maxlen = std::max<size_t>(_maxlen, name.size());
}

void ProgramOptions::addOption(const std::string& name, const std::string& defaultValue, const std::string& desc) {
    COVIS_ASSERT_MSG(_vm.empty(), "Options already parsed, new options cannot be added!");
    COVIS_ASSERT(!name.empty());

    _options.add_options()(name.c_str(),
                           po::value<std::string>()->default_value(defaultValue),
                           desc.c_str());

    _maxlen = std::max<size_t>(_maxlen, name.size());
}

bool ProgramOptions::parse(int argc, const char** argv) {
    // Store if not already done
    if(_vm.empty()) {
        // Store
        try {
            po::options_description visible;
            visible.add_options()("help,h", "show this message");
            visible.add(_flags).add(_options);
            _all.add(visible).add(_hidden);
            po::store(po::command_line_parser(argc, argv).options(_all).positional(_positionals).run(), _vm);
            po::notify(_vm);
        } catch (const std::exception& e) {
            COVIS_MSG_ERROR("Failed to parse command line inputs: " << e.what() << "!");
            COVIS_MSG("");
            return false;
        }
    }

    bool help = false;

    // If '-h' or '--help' requested
    if (!help)
        help = _vm.count("help");

    // If any of the positionals is missing
    if (!help) {
        for (std::vector<std::pair<std::string, std::string> >::const_iterator it = _posvec.begin(); it != _posvec.end(); ++it) {
            if (!_vm.count(it->first)) {
                COVIS_MSG_WARN("Positional arguments missing!");
                COVIS_MSG("");
                help = true;
                break;
            }
        }
    }

    // If any option is empty
    if (!help) {
        for (size_t i = 0; i < _options.options().size(); ++i) {
            if (!_vm.count(_options.options()[i]->long_name())) {
                COVIS_MSG_WARN("Option --" << _options.options()[i]->long_name() << " missing!");
                COVIS_MSG("");
                help = true;
                break;
            }
        }
    }

    // Help required
    if (help) {
        // General usage and positionals
        std::cout << "Usage:" << std::endl;
        std::cout << "  " << argv[0] << " [flags/options] ";

        for (std::vector<std::pair<std::string, std::string> >::const_iterator it = _posvec.begin(); it != _posvec.end(); ++it)
            std::cout << "<" << it->first << "> ";
        std::cout << std::endl << std::endl;

        // Line length
        size_t llen = 80;

        // If line length can be read, get it
        struct winsize ws;
        if (ioctl(STDIN_FILENO, TIOCGWINSZ, &ws) != -1)
            llen = ws.ws_col;

        // Positionals
        if (!_posvec.empty()) {
            std::cout << "Positionals:" << std::endl;
            // Find longest positional string
            size_t maxlenPos = 0;
            for (std::vector<std::pair<std::string, std::string> >::const_iterator it = _posvec.begin();
                    it != _posvec.end();
                    ++it)
                if(it->first.length() > maxlenPos)
                    maxlenPos = it->first.length();
            
            for (std::vector<std::pair<std::string, std::string> >::const_iterator it = _posvec.begin();
                    it != _posvec.end();
                    ++it) {
                const std::string name = "<" + it->first + ">";
                const std::string pad(maxlenPos + 2 - name.size(), ' '); // + 2 for the angle brackets
                std::cout << "  " << name << pad << "  ";
                std::string desc = it->second;
                const size_t namePadLen = (2 + name.length() + pad.length() + 2);
                if(desc.length() <= llen - namePadLen) { // Description does not exceed border
                    std::cout << desc << std::endl;
                } else { // Description exceeds border, begin indented printing
                    std::stringstream iss(desc);
                    std::string word;
                    size_t written = namePadLen;
                    while(iss >> word) {
                        // If word will exceed border, begin a new line
                        if(word.length() + written > llen) {
                            if(namePadLen + word.length() < llen) { // OK to start printing
                                std::cout << std::endl << std::string(namePadLen, ' ');
                                written = 0;
                            } else { // Word itself is too long
                                size_t numChars = llen - written; // Space available
                                while(word.length() > numChars) {
                                    std::cout << word.substr(0, numChars) << std::endl << std::string(namePadLen, ' ');
                                    word.erase(word.begin(), word.begin() + numChars);
                                    numChars = llen - namePadLen;
                                }
                            }
                        }
                        
                        std::cout << word << " ";
                        written += word.length() + 1;
                    }
                    std::cout << std::endl;
                }
            }
            
            std::cout << std::endl;
        }

        // Flags
        if (!_flags.options().empty())
            std::cout << "Flags:" << std::endl << _flags << std::endl;

        // Values
        if (!_options.options().empty())
            std::cout << "Options:" << std::endl << _options;

        return false;
    }

    return true;
}

void ProgramOptions::print() {
    COVIS_ASSERT_MSG(!_vm.empty(), "Options not parsed, use parse()!");

    // Line length
    size_t llen = 80;

    // If line length can be read, get it
    struct winsize ws;
    if (ioctl(STDIN_FILENO, TIOCGWINSZ, &ws) != -1)
        llen = ws.ws_col;

    // Maximum allowed length of a value print
    const size_t maxcol = llen - (2 + _maxlen + 2); // (2 spaces + name + 2 spaces)

    // Positionals
    if (!_posvec.empty()) {
        std::cout << "Positionals:" << std::endl;
        for (std::vector<std::pair<std::string, std::string> >::const_iterator it = _posvec.begin();
                it != _posvec.end();
                ++it) {
            const std::string name = "<" + it->first + ">";
            const std::string pad(_maxlen - name.size(), ' ');
            std::string value = getValue<std::string>(it->first);
            if (value.size() > maxcol) {
                const size_t end = std::min<size_t>(maxcol, value.find('\n'));
                value = value.substr(0, end - 3) + "...";
            }
            
            std::cout << "  " << name << pad << "  " << value << std::endl;
        }
        
        std::cout << std::endl;
    }

    // Flags
    if (!_flags.options().empty()) {
        std::cout << "Flags:" << std::endl;
        for (size_t i = 0; i < _flags.options().size(); ++i) {
            const std::string& name = _flags.options()[i]->long_name();
            const std::string pad(_maxlen - name.size(), ' ');
            if (getFlag(name))
                std::cout << "  " << name << pad << "  true" << std::endl;
            else
                std::cout << "  " << name << pad << "  false" << std::endl;
        }
        std::cout << std::endl;
    }

    // Values
    if (!_options.options().empty()) {
        std::cout << "Options:" << std::endl;
        for (size_t i = 0; i < _options.options().size(); ++i) {
            const std::string& name = _options.options()[i]->long_name();
            const std::string pad(_maxlen - name.size(), ' ');
            std::string value = getValue(name);
            if (value.size() > maxcol) {
                const size_t end = std::min<size_t>(maxcol, value.find('\n'));
                value = value.substr(0, end - 3) + "...";
            }
            std::cout << "  " << name << pad << "  " << value << std::endl;
        }
        std::cout << std::endl;
    }
}
