# Helper script for finding all CoViS dependencies.
# The script adds information to the following variables:
#
#   COVIS_INCLUDE_DIRS
#   COVIS_LIBRARY_DIRS
#   COVIS_LIBRARIES
#   COVIS_DEFINITIONS
#
#
# These libraries are mandatory, and a fatal error will be issued if one
# of these is missing:
#
#   OpenCV
#   Eigen
#   Boost
#   PCL
#
#
# These are optional:
#
#   Astyle
#   Doxygen
#   VTK
#   CUDA
#   OpenMP
#


# Point CMake module path to here
list(APPEND CMAKE_MODULE_PATH ${CMAKE_SOURCE_DIR}/cmake)

# TODO: I know of no better place to place these IDE-specific CMake variables than here
mark_as_advanced(CMAKE_CODEBLOCKS_EXECUTABLE)
mark_as_advanced(CMAKE_CODEBLOCKS_MAKE_ARGUMENTS)
mark_as_advanced(QT_QMAKE_EXECUTABLE)

##
# Set version requirements
# Remember to update doc/installation.h if you change these
##
set(OPENCV_REQUIRED_VERSION 2.3)
set(BOOST_REQUIRED_VERSION 1.46)
set(PCL_REQUIRED_VERSION 1.7.2)

##
# Find OpenCV (mandatory)
##
set(OpenCV_USE_MANGLED_PATHS TRUE)
find_package(OpenCV REQUIRED)
if(OpenCV_FOUND)
  if(OpenCV_VERSION VERSION_LESS ${OPENCV_REQUIRED_VERSION})
    message(FATAL_ERROR "OpenCV version ${OPENCV_REQUIRED_VERSION} not found - found version ${OpenCV_VERSION}!")
  endif(OpenCV_VERSION VERSION_LESS ${OPENCV_REQUIRED_VERSION})

  message(STATUS ">> OpenCV ${OpenCV_VERSION} found")
  if(OpenCV_VERSION VERSION_LESS 3)
    set(OPENCV_3 OFF)
  else(OpenCV_VERSION VERSION_LESS 3)
    set(OPENCV_3 ON)
  endif(OpenCV_VERSION VERSION_LESS 3)
else(OpenCV_FOUND)
  message(FATAL_ERROR ">> OpenCV ${OPENCV_REQUIRED_VERSION} or higher not found!")
endif(OpenCV_FOUND)
list(APPEND COVIS_INCLUDE_DIRS ${OpenCV_INCLUDE_DIRS})
list(APPEND COVIS_LIBRARIES ${OpenCV_LIBS})
mark_as_advanced(OpenCV_DIR)

##
# Find Eigen 3 (mandatory)
# TODO: Make silent
##
find_package(Eigen3 QUIET)
if(EIGEN_FOUND)
  message(STATUS ">> Eigen ${PC_EIGEN_VERSION} found")
  list(APPEND COVIS_INCLUDE_DIRS ${EIGEN_INCLUDE_DIRS})
  list(APPEND COVIS_DEFINITIONS ${EIGEN_DEFINITIONS})
else(EIGEN_FOUND)
  message(FATAL_ERROR ">> Eigen 3 or higher not found!")
endif(EIGEN_FOUND)

##
# Find Boost (mandatory)
##
find_package(Boost ${BOOST_REQUIRED_VERSION} QUIET COMPONENTS program_options python regex timer)
if(Boost_FOUND)
  set(BOOST_VERSION ${Boost_MAJOR_VERSION}.${Boost_MINOR_VERSION}.${Boost_SUBMINOR_VERSION})
  # TODO: Handle the introduction of numpy since Boost 1.63
  if(BOOST_VERSION VERSION_GREATER 1.62)
    find_package(Boost ${BOOST_REQUIRED_VERSION} QUIET COMPONENTS numpy program_options python regex timer)
  endif(BOOST_VERSION VERSION_GREATER 1.62)
  message(STATUS ">> Boost ${BOOST_VERSION} found")
  list(APPEND COVIS_INCLUDE_DIRS ${Boost_INCLUDE_DIRS})
  list(APPEND COVIS_LIBRARIES ${Boost_LIBRARIES})
  mark_as_advanced(BOOST_THREAD_LIBRARY)
else(Boost_FOUND)
  message(FATAL_ERROR ">> Boost ${BOOST_REQUIRED_VERSION} or higher not found!")
endif(Boost_FOUND)
mark_as_advanced(Boost_DIR)


##
# Find PCL (mandatory)
# TODO: Make OpenNI2 silent
##
SET(PkgConfig_FIND_QUIETLY TRUE)
SET(eigen_FIND_QUIETLY TRUE)
SET(Flann_FIND_QUIETLY TRUE)
SET(libusb-1.0_FIND_QUIETLY TRUE)
SET(openni_FIND_QUIETLY TRUE)
SET(opennidev_FIND_QUIETLY TRUE)
SET(qhull_FIND_QUIETLY TRUE)
SET(PCL_FIND_QUIETLY TRUE)
SET(PCL_2D_FIND_QUIETLY TRUE)
SET(PCL_APPS_FIND_QUIETLY TRUE)
SET(PCL_COMMON_FIND_QUIETLY TRUE)
SET(PCL_FEATURES_FIND_QUIETLY TRUE)
SET(PCL_FILTERS_FIND_QUIETLY TRUE)
SET(PCL_GEOMETRY_FIND_QUIETLY TRUE)
SET(PCL_IN_HAND_SCANNER_FIND_QUIETLY TRUE)
SET(PCL_IO_FIND_QUIETLY TRUE)
SET(PCL_KDTREE_FIND_QUIETLY TRUE)
SET(PCL_KEYPOINTS_FIND_QUIETLY TRUE)
SET(PCL_ML_FIND_QUIETLY TRUE)
SET(PCL_MODELER_FIND_QUIETLY TRUE)
SET(PCL_OCTREE_FIND_QUIETLY TRUE)
SET(PCL_OUTOFCORE_FIND_QUIETLY TRUE)
SET(PCL_PEOPLE_FIND_QUIETLY TRUE)
SET(PCL_POINT_CLOUD_EDITOR_FIND_QUIETLY TRUE)
SET(PCL_RECOGNITION_FIND_QUIETLY TRUE)
SET(PCL_REGISTRATION_FIND_QUIETLY TRUE)
SET(PCL_SAMPLE_CONSENSUS_FIND_QUIETLY TRUE)
SET(PCL_SEARCH_FIND_QUIETLY TRUE)
SET(PCL_SEGMENTATION_FIND_QUIETLY TRUE)
SET(PCL_STEREO_FIND_QUIETLY TRUE)
SET(PCL_SURFACE_FIND_QUIETLY TRUE)
SET(PCL_TRACKING_FIND_QUIETLY TRUE)
SET(PCL_VISUALIZATION_FIND_QUIETLY TRUE)
find_package(PCL ${PCL_REQUIRED_VERSION} QUIET HINTS ../pcl/build)
if(PCL_FOUND)
  message(STATUS ">> PCL ${PCL_VERSION} found")
  list(APPEND COVIS_INCLUDE_DIRS ${PCL_INCLUDE_DIRS})
  list(APPEND COVIS_LIBRARY_DIRS ${PCL_LIBRARY_DIRS})
  # TODO: Stupid PCL adds a -NOTFOUND variable to its lib list
  if(NOT OPENNI2_LIBRARIES)
    list(REMOVE_ITEM PCL_LIBRARIES OPENNI2_LIBRARIES-NOTFOUND)
  endif(NOT OPENNI2_LIBRARIES)
  list(APPEND COVIS_LIBRARIES ${PCL_LIBRARIES})
  list(APPEND COVIS_DEFINITIONS ${PCL_DEFINITIONS})
else(PCL_FOUND)
  message(FATAL_ERROR ">> PCL ${PCL_REQUIRED_VERSION} or higher not found!")
endif(PCL_FOUND)
mark_as_advanced(PCL_2D_INCLUDE_DIR)
mark_as_advanced(PCL_APPS_INCLUDE_DIR)
mark_as_advanced(PCL_APPS_LIBRARY)
mark_as_advanced(PCL_APPS_LIBRARY_DEBUG)
mark_as_advanced(PCL_COMMON_INCLUDE_DIR)
mark_as_advanced(PCL_DIR)
mark_as_advanced(PCL_FEATURES_INCLUDE_DIR)
mark_as_advanced(PCL_FILTERS_INCLUDE_DIR)
mark_as_advanced(PCL_GEOMETRY_INCLUDE_DIR)
mark_as_advanced(PCL_IN_HAND_SCANNER_INCLUDE_DIR)
mark_as_advanced(PCL_IO_INCLUDE_DIR)
mark_as_advanced(PCL_KDTREE_INCLUDE_DIR)
mark_as_advanced(PCL_KEYPOINTS_INCLUDE_DIR)
mark_as_advanced(PCL_ML_INCLUDE_DIR)
mark_as_advanced(PCL_MODELER_INCLUDE_DIR)
mark_as_advanced(PCL_OCTREE_INCLUDE_DIR)
mark_as_advanced(PCL_OUTOFCORE_INCLUDE_DIR)
mark_as_advanced(PCL_PEOPLE_INCLUDE_DIR)
mark_as_advanced(PCL_POINT_CLOUD_EDITOR_INCLUDE_DIR)
mark_as_advanced(PCL_RECOGNITION_INCLUDE_DIR)
mark_as_advanced(PCL_REGISTRATION_INCLUDE_DIR)
mark_as_advanced(PCL_SAMPLE_CONSENSUS_INCLUDE_DIR)
mark_as_advanced(PCL_SEARCH_INCLUDE_DIR)
mark_as_advanced(PCL_SEGMENTATION_INCLUDE_DIR)
mark_as_advanced(PCL_STEREO_INCLUDE_DIR)
mark_as_advanced(PCL_SURFACE_INCLUDE_DIR)
mark_as_advanced(PCL_TRACKING_INCLUDE_DIR)
mark_as_advanced(PCL_VISUALIZATION_INCLUDE_DIR)
mark_as_advanced(DAVIDSDK_INCLUDE_DIR)
mark_as_advanced(DAVIDSDK_LIBRARY)
mark_as_advanced(DSSDK_DIR)
mark_as_advanced(ENSENSO_INCLUDE_DIR)
mark_as_advanced(ENSENSO_LIBRARY)
mark_as_advanced(FLANN_LIBRARY)
mark_as_advanced(FLANN_INCLUDE_DIRS)
mark_as_advanced(FLANN_LIBRARY)
mark_as_advanced(FLANN_LIBRARY_DEBUG)
mark_as_advanced(LIBUSB_1_INCLUDE_DIR)
mark_as_advanced(LIBUSB_1_LIBRARY)
mark_as_advanced(OPENNI_INCLUDE_DIRS)
mark_as_advanced(OPENNI_LIBRARY)
mark_as_advanced(OPENNI2_INCLUDE_DIRS)
mark_as_advanced(OPENNI2_LIBRARY)
mark_as_advanced(QHULL_INCLUDE_DIRS)
mark_as_advanced(QHULL_LIBRARY)
mark_as_advanced(QHULL_LIBRARY_DEBUG)
mark_as_advanced(Qt5Core_DIR)
mark_as_advanced(Qt5Gui_DIR)
mark_as_advanced(Qt5Network_DIR)
mark_as_advanced(Qt5WebKit_DIR)
mark_as_advanced(RSSDK_DIR)

##
# FLANN version check
##
if(PC_FLANN_VERSION VERSION_LESS 1.8)
  set(FLANN_1_8 OFF)
else(PC_FLANN_VERSION VERSION_LESS 1.8)
  set(FLANN_1_8 ON)
endif(PC_FLANN_VERSION VERSION_LESS 1.8)

##
# Find Astyle
##
find_program(ASTYLE_EXECUTABLE astyle)
if(ASTYLE_EXECUTABLE)
  set(ASTYLE_FOUND TRUE CACHE STRING "Astyle find status" FORCE)
else(ASTYLE_EXECUTABLE)
  set(ASTYLE_FOUND FALSE CACHE STRING "Astyle find status" FORCE)
endif(ASTYLE_EXECUTABLE)
mark_as_advanced(ASTYLE_EXECUTABLE ASTYLE_FOUND)

##
# Find Doxygen
##
find_package(Doxygen QUIET)

##
# Find CUDA
##
find_package(CUDA QUIET)
if(CUDA_FOUND)
  message(STATUS ">> CUDA ${CUDA_VERSION} found")
endif(CUDA_FOUND)
mark_as_advanced(CUDA_BUILD_CUBIN)
mark_as_advanced(CUDA_BUILD_CUBIN)
mark_as_advanced(CUDA_BUILD_EMULATION)
mark_as_advanced(CUDA_HOST_COMPILER)
mark_as_advanced(CUDA_SDK_ROOT_DIR)
mark_as_advanced(CUDA_SEPARABLE_COMPILATION)
mark_as_advanced(CUDA_TOOLKIT_ROOT_DIR)
mark_as_advanced(CUDA_VERBOSE_BUILD)

##
# Find OpenMP
##
find_package(OpenMP QUIET)
if(OPENMP_FOUND)
  message(STATUS ">> OpenMP found")
  option(COVIS_USE_OPENMP "Use OpenMP" ON)
endif(OPENMP_FOUND)
