# Find script for CoViS
#
#
# Use this script in the CMake function 'find_package()',
# and link automatically to all libraries necessary for CoViS applications:
#
#  find_package(COVIS ...)
#  ...
#  target_link_libraries(<MY_TARGET> ${COVIS_LIBRARIES})
#
#
# This script will define:
#
#  COVIS_FOUND - system has CoViS
#  COVIS_INCLUDE_DIRS - the CoViS include directories
#  COVIS_LIBRARY_DIRS - the CoViS library directories
#  COVIS_LIBRARIES - absolute paths to the CoViS library and its dependencies
#  COVIS_DEFINITIONS - add these flags to compile against CoViS
#  COVIS_VERSION - CoViS version found
#  COVIS_REVISION - CoViS revision found, -1 if not available
#  COVIS_LIBRARY - relative path to the main CoViS library only
#


# Initialize find status
set(COVIS_FOUND TRUE)

# CoViS version as MAJOR.MINOR.PATCH
set(COVIS_VERSION_NUMERIC @COVIS_VERSION@)

# Version check
if(COVIS_FIND_VERSION)
  if(COVIS_FIND_VERSION_EXACT)
    # Exact
    if(COVIS_VERSION_NUMERIC VERSION_EQUAL COVIS_FIND_VERSION)
      set(COVIS_FOUND TRUE)
    else(COVIS_VERSION_NUMERIC VERSION_EQUAL COVIS_FIND_VERSION)
      set(COVIS_FOUND FALSE)
    endif(COVIS_VERSION_NUMERIC VERSION_EQUAL COVIS_FIND_VERSION)
  else(COVIS_FIND_VERSION_EXACT)
    # Equal to or greater
    if(COVIS_VERSION_NUMERIC VERSION_EQUAL COVIS_FIND_VERSION OR
       COVIS_VERSION_NUMERIC VERSION_GREATER COVIS_FIND_VERSION)
      set(COVIS_FOUND TRUE)
    else(COVIS_VERSION_NUMERIC VERSION_EQUAL COVIS_FIND_VERSION OR
         COVIS_VERSION_NUMERIC VERSION_GREATER COVIS_FIND_VERSION)
      set(COVIS_FOUND FALSE)
    endif(COVIS_VERSION_NUMERIC VERSION_EQUAL COVIS_FIND_VERSION OR
          COVIS_VERSION_NUMERIC VERSION_GREATER COVIS_FIND_VERSION)
  endif(COVIS_FIND_VERSION_EXACT)
endif(COVIS_FIND_VERSION)

# If caller has not yet found VTK, find it
if(NOT VTK_FOUND)
  find_package(VTK QUIET)
  if(VTK_FOUND)
    include(${VTK_USE_FILE})
  else(VTK_FOUND)
    set(COVIS_FOUND FALSE)
  endif(VTK_FOUND)
endif(NOT VTK_FOUND)

# If CoViS is compiled with OpenMP support, find it
if(@COVIS_USE_OPENMP@)
  find_package(OpenMP QUIET)
  if(OPENMP_FOUND)
    # Find compiler type (GCC, CLANG)
    string(TOUPPER ${CMAKE_CXX_COMPILER_ID} CXX_COMPILER)
    if(CXX_COMPILER STREQUAL "GNU") # GNU, set to GCC
      set(CXX_COMPILER "GCC")
    elseif(CXX_COMPILER STREQUAL "CLANG") # Clang
      # Do nothing for now
    endif(CXX_COMPILER STREQUAL "GNU")

    # TODO: Add link to OpenMP lib
    list(APPEND COVIS_DEFINITIONS_FIND ${OpenMP_CXX_FLAGS})
    if(CXX_COMPILER STREQUAL "GCC")
      list(APPEND COVIS_LIBRARIES_FIND gomp)
    elseif(CXX_COMPILER STREQUAL "CLANG")
      list(APPEND COVIS_LIBRARIES_FIND omp)
    endif(CXX_COMPILER STREQUAL "GCC")

    unset(CXX_COMPILER)
  endif(OPENMP_FOUND)
endif(@COVIS_USE_OPENMP@)

# Propagate find status
set(COVIS_FOUND ${COVIS_FOUND} CACHE BOOL "CoViS find status")

# If CoViS was found with right version and components, propagate state information
if(COVIS_FOUND)
  set(COVIS_INCLUDE_DIRS @COVIS_INCLUDE_DIRS_BUILD@
      CACHE STRING "CoViS include directories")

  set(COVIS_LIBRARY_DIRS @COVIS_LIBRARY_DIRS_BUILD@
      CACHE STRING "CoViS library directories")

  set(COVIS_LIBRARIES @COVIS_LIBRARIES_BUILD@;${COVIS_LIBRARIES_FIND}
      CACHE STRING "CoViS libraries")

  set(COVIS_DEFINITIONS @COVIS_DEFINITIONS@;${COVIS_DEFINITIONS_FIND}
      CACHE STRING "CoViS definitions")
      
  set(COVIS_VERSION @COVIS_VERSION@
      CACHE STRING "CoViS version")
      
  set(COVIS_REVISION @COVIS_REVISION@
      CACHE STRING "CoViS revision")

  set(COVIS_LIBRARY @COVIS_LIBRARY@
      CACHE STRING "CoViS main library")

  unset(COVIS_LIBRARIES_FIND)
  unset(COVIS_DEFINITIONS_FIND)
endif(COVIS_FOUND)

# Propagate find status
if(NOT COVIS_FIND_QUIETLY)
  if(COVIS_FOUND)
    message(STATUS "CoViS ${COVIS_VERSION_NUMERIC} found!")
  else(COVIS_FOUND)
    if(COVIS_FIND_VERSION)
      message(STATUS "CoViS ${COVIS_FIND_VERSION} or higher NOT found!")
    else(COVIS_FIND_VERSION)
      message(STATUS "CoViS NOT found!")
    endif(COVIS_FIND_VERSION)
  endif(COVIS_FOUND)
endif(NOT COVIS_FIND_QUIETLY)

# Clean up
unset(COVIS_VERSION_NUMERIC)
