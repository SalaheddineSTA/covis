#!/bin/sh

if [ $# -lt 3 ]
then
    echo -e "Usage:\n\t$0 <input> <output> <resolution> [<scaling>]"
    exit
fi

echo "Resampling mesh $1 by 1M points..."
plytmp=`tempfile -s .ply`
meshlabserver -i $1 -o $plytmp -s sampling.mlx

echo "Converting to output PCD file $2..."
pcl_ply2pcd $plytmp $2
rm $plytmp

if [ $# -le 4 ]
then
    echo "Scaling point cloud by a factor of $4..."
    pcl_transform_point_cloud $2 $2 -scale $4,$4,$4
fi

echo "Downsampling point cloud to target resolution $3..."
pcl_voxel_grid -leaf $3,$3,$3 $2 $2

echo "All done!"