// Copyright (c) 2013, University of Southern Denmark
// All rights reserved.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice,
//    this list of conditions and the following disclaimer.
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation
//    and/or other materials provided with the distribution.
// 3. Neither the name of the University of Southern Denmark nor the names of
//    its contributors may be used to endorse or promote products derived from
//    this software without specific prior written permission.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL THE UNIVERSITY OF SOUTHERN DENMARK BE
// LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
// CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
// SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
// INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.

#include <covis/covis.h>
using namespace covis;

#include <opencv2/highgui/highgui.hpp>

int main(int argc, const char** argv) {
    // Setup program options
    core::ProgramOptions po;
    po.addPositional("imgfile", "image file");
    po.addOption<double>("sigma", 3.0, "Gaussian smoothing kernel standard deviation for complex images");
    
    // Parse
    if(!po.parse(argc, argv))
        return 1;
    
    const double sigma = po.getValue<double>("sigma");
    
    // Load image
    cv::Mat img = cv::imread(po.getValue("imgfile"));
    COVIS_ASSERT(!img.empty());
    
    // Compute gradient
    cv::Mat mo = filter::computeIntensityGradient(img);
    cv::Mat_<double> mod[2];
    cv::split(mo, mod);

    // Run intrinsic dimensionality filtering
    filter::IntrinsicDimension id;
    id.setSigma(sigma);
    cv::Mat idResult = id.filter(mod[0], mod[1]);
    
    // Split channels
    cv::Mat_<double> ids[3];
    cv::split(idResult, ids);
    
    // Create an indicator image of each ID type
    cv::Mat_<double> ind(idResult.rows, idResult.cols);
    for(int r = 0; r < ind.rows; ++r) {
        for(int c = 0; c < ind.cols; ++c) {
            int id = 0;
            if(ids[1][r][c] > ids[0][r][c])
                id = 1;
            if(ids[2][r][c] > ids[id][r][c])
                id = 2;
            ind[r][c] = double(2 - id) / 2.0;
        }
    }
    
    // Create montages
    cv::Mat idTile = visu::tile(ids, 3, 1); // All three iD responses in one montage
    cv::Mat imgInd[] = {img, ind};
    cv::Mat imgIndTile = visu::tile(imgInd, 2, 1); // Input image and indicator image in one montage
    
    // Visualize
    COVIS_MSG("Press 'q' to quit...");
    cv::imshow("Input image and iD of each pixel (i0D: white, i1D: gray, i2D: black)", imgIndTile);
    cv::imshow("Responses (i0D, i1D and i2D)", idTile);
    while(cv::waitKey() != 'q')
        ;
    
    return 0;
}
