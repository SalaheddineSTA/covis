// Copyright (c) 2013, University of Southern Denmark
// All rights reserved.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice,
//    this list of conditions and the following disclaimer.
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation
//    and/or other materials provided with the distribution.
// 3. Neither the name of the University of Southern Denmark nor the names of
//    its contributors may be used to endorse or promote products derived from
//    this software without specific prior written permission.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL THE UNIVERSITY OF SOUTHERN DENMARK BE
// LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
// CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
// SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
// INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.

#include <covis/covis.h>
#include "covis/calib/stereo_match.h"

//OpenCV
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc/imgproc.hpp>

//PCL
#include <pcl/io/io.h>
#include <pcl/io/pcd_io.h>
#include <pcl/console/print.h>

using namespace covis;
using namespace covis::filter;
using namespace covis::calib;


int main(int argc, const char** argv) {
    // Setup program options
    core::ProgramOptions po;
    po.addPositional("leftImg", "left image");
    po.addPositional("rightImg", "right image");
    po.addPositional("intrinsic", "intrinsic");
    po.addPositional("extrinsic", "extrinsic");

    po.addFlag('v', "visualize", "enable visualization");
    po.addFlag('e', "verbose", "enable verbose output");
    po.addFlag('r', "rectified", "set rectified = true");
    po.addFlag('s', "save", "save point cloud");


    po.addOption("alg", 0, "choose algorithm: 0 - bm, 1 - sgbm, 2 - hh, 3 - var");
    po.addOption("maxd", 512, "max disparity");
    po.addOption("mind", 0, "min disparity");
    po.addOption("blocksize", 7, "blocksize");
    po.addOption("name", "stereo_match.pcd", "saved point cloud name");

    // Parse
    if(!po.parse(argc, argv))
        return 1;

    const int alg = po.getValue<int>("alg");
    const int maxdisp = po.getValue<int>("maxd");
    const int mindisp = po.getValue<int>("mind");
    const int blocksize = po.getValue<int>("blocksize");
    const bool visualize = po.getFlag("visualize");
    const bool verbose = po.getFlag("verbose");
    const bool save = po.getFlag("save");
    const bool rectified = po.getFlag("rectified");

    pcl::PointCloud<pcl::PointXYZRGBA>::Ptr pointCloud (new pcl::PointCloud<pcl::PointXYZRGBA>());

    /*
     * Compute point cloud
     */
    {
        core::ScopedTimer t("Stereo Matching");
        StereoMatch sm;
        sm.setAlgorithm(alg);
        sm.setMaxDisparity(maxdisp);
        sm.setMinDisparity(mindisp);
        sm.setBlockSize(blocksize);
        sm.setVerbose(verbose);
        sm.setRectified(rectified);
        pointCloud = sm.compute(po.getValue("leftImg"), po.getValue("rightImg"),
                po.getValue("intrinsic"),po.getValue("extrinsic"));
    }

    if (visualize) {
        pcl::visualization::PCLVisualizer viewer ("Point CLoud");
        viewer.addPointCloud<pcl::PointXYZRGBA>(pointCloud);
        while (!viewer.wasStopped ())
        {
            viewer.spinOnce ();
            boost::this_thread::sleep (boost::posix_time::microseconds (100));
        }
    }

    if (save) {
        pcl::io::savePCDFile(po.getValue("name"), *pointCloud);
        pcl::console::print_info("PointCloud was saved\n");
    }

    return 0;
}
