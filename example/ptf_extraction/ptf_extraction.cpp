// Copyright (c) 2016, University of Southern Denmark
// All rights reserved.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice,
//    this list of conditions and the following disclaimer.
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation
//    and/or other materials provided with the distribution.
// 3. Neither the name of the University of Southern Denmark nor the names of
//    its contributors may be used to endorse or promote products derived from
//    this software without specific prior written permission.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL THE UNIVERSITY OF SOUTHERN DENMARK BE
// LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
// CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
// SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
// INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.

//Own
#include "covis/core/ptf.h"
#include "covis/util/eigen_io.h"
#include "covis/feature/ptf_extraction.h"
#include "covis/detect/ptf_matching.h"
#include "covis/core/program_options.h"
#include "covis/core/feedforward_net.h"


//PCL
#include <pcl/io/pcd_io.h>
#include <pcl/filters/voxel_grid.h>
#include <pcl/visualization/pcl_visualizer.h>
#include <pcl/registration/icp.h>

//OpenCV
#include <opencv2/core/eigen.hpp>

using namespace covis;
using namespace covis::core;
using namespace covis::feature;
using namespace covis::detect;
using namespace cv;

typedef pcl::PointXYZRGBNormal PointNT;


///Find maximum distance between two points
template<typename T>
float getMaxDistance(typename pcl::PointCloud<T>::Ptr cloud) {
    T minPt, maxPt;
    pcl::getMinMax3D<T>(*cloud, minPt, maxPt);
    float distance = sqrt((minPt.x - maxPt.x) * (minPt.x - maxPt.x) + (minPt.y - maxPt.y) * (minPt.y - maxPt.y) +
                          (minPt.z - maxPt.z) * (minPt.z - maxPt.z));
    return distance;
}

template<typename T>
void
downsamplePointCloud(typename pcl::PointCloud<T>::Ptr cloud, typename pcl::PointCloud<T>::Ptr &out, float leaf_size) {

    typename pcl::PointCloud<T>::Ptr cloud_down(new pcl::PointCloud<T>());

    pcl::VoxelGrid<T> sor;
    sor.setInputCloud(cloud);
    sor.setLeafSize(leaf_size, leaf_size, leaf_size);
    sor.filter(*cloud_down);
    pcl::copyPointCloud(*cloud_down, *out);
}

std::string finShortNameForFile(std::string path) {
    std::string object = path;
    const size_t ii = object.find_last_of("/");
    if (ii != std::string::npos) {
        object = (object.substr(ii + 1).c_str());
    }

    object = object.substr(0, object.size() - 4).c_str();
    return object;
}

template<typename T>
Eigen::Matrix4f doICP(Eigen::Matrix4f transformation, typename pcl::PointCloud<T>::Ptr downsampled_obj,
                      typename pcl::PointCloud<T>::Ptr downsampled_scn) {
    Eigen::Matrix4f m;
    typename pcl::PointCloud<T>::Ptr final(new pcl::PointCloud<T>());
    typename pcl::PointCloud<T>::Ptr aligned(new pcl::PointCloud<T>());


    pcl::transformPointCloud(*downsampled_obj, *final, transformation);


    typename pcl::IterativeClosestPoint<T, T> icp;
    icp.setInputSource(final);
    icp.setInputTarget(downsampled_scn);
    icp.setMaxCorrespondenceDistance(0.01);
    icp.setMaximumIterations(100);
    icp.align(*aligned);
    m = icp.getFinalTransformation() * (transformation);
    return m;
}

void saveFile(pcl::PointCloud<PTF> ptfs, pcl::PointCloud<PTF_Index> ptfs_idx, std::string short_name) {

    std::stringstream file_name, file_name_idx;
    file_name << short_name << ".pcd";
    file_name_idx << short_name << "_index.pcd";
    pcl::io::savePCDFileBinary(file_name.str(), ptfs);
    pcl::io::savePCDFileBinary(file_name_idx.str(), ptfs_idx);
    COVIS_MSG_INFO("Files " << file_name.str() << " and " << file_name_idx.str() << " are saved");
}


//***DISPLAY
template<typename PointN>
void displayFoundObjects(typename pcl::PointCloud<PointN>::Ptr obj, typename pcl::PointCloud<PointN>::Ptr scn,
                         std::vector<Eigen::Matrix4f> poses) {

    pcl::visualization::PCLVisualizer viewer("Found poses");
    viewer.addPointCloud<PointN>(scn, "scene");
    viewer.setPointCloudRenderingProperties(pcl::visualization::PCL_VISUALIZER_COLOR, 0, 0, 0, "scene");
    viewer.setBackgroundColor(1, 1, 1);


    for (size_t i = 0; i < poses.size(); i++) {
        Eigen::Matrix4f pose = poses[i];
        std::cout << pose << std::endl;
        std::stringstream name;
        name << i;
        typename pcl::PointCloud<PointN>::Ptr obj_transformed(new pcl::PointCloud<PointN>());
        pcl::transformPointCloud<PointN>(*obj, *obj_transformed, pose);
        viewer.addPointCloud<PointN>(obj_transformed, name.str());


        float r = static_cast <float> (rand()) / static_cast <float> (RAND_MAX);
        float g = static_cast <float> (rand()) / static_cast <float> (RAND_MAX);
        float b = static_cast <float> (rand()) / static_cast <float> (RAND_MAX);
        std::cout << r << ", " << g << ", " << b << std::endl;
        viewer.setPointCloudRenderingProperties(pcl::visualization::PCL_VISUALIZER_COLOR, r, g, b, name.str());
    }

    while (!viewer.wasStopped()) {
        viewer.spinOnce();
        boost::this_thread::sleep(boost::posix_time::microseconds(100));
    }
}

void createDir(std::string path) {
    boost::filesystem::path dir(path);
    if (boost::filesystem::create_directory(dir)) {
        std::cerr << "Directory Created: " << path << std::endl;
    }
}

//****Process NN file
covis::core::FeedForwardNet loadNN(std::string nnXML, bool debug) {
    covis::core::FeedForwardNet out;
    cv::Mat _x1_offset, _b1, _iw1, _x1_gain, _iw2, _y1_gain, _x1_ymin, _b2, _b3, _iw3, _b4, _iw4;

    std::stringstream sname_weights;
    cv::FileStorage fs(nnXML, cv::FileStorage::READ);
    fs["x1_xoffset"] >> _x1_offset;
    fs["x1_gain"] >> _x1_gain;
    fs["x1_ymin"] >> _x1_ymin;

    fs["b1"] >> _b1;
    fs["b2"] >> _b2;
    fs["b3"] >> _b3;
    fs["b4"] >> _b4;

    fs["iw1"] >> _iw1;
    fs["iw2"] >> _iw2;
    fs["iw3"] >> _iw3;
    fs["iw4"] >> _iw4;

    Eigen::Matrix<float, Eigen::Dynamic, Eigen::Dynamic> b1_Eigen, b2_Eigen, iw1_Eigen, x1_offset_Eigen;
    Eigen::Matrix<float, Eigen::Dynamic, Eigen::Dynamic> x1_gain_Eigen, x1_ymin_Eigen, iw2_Eigen, iw3_Eigen;
    Eigen::Matrix<float, Eigen::Dynamic, Eigen::Dynamic> iw4_Eigen, b3_Eigen;

    cv2eigen(_x1_offset, x1_offset_Eigen);
    cv2eigen(_x1_gain, x1_gain_Eigen);

    cv2eigen(_b1, b1_Eigen);
    cv2eigen(_b2, b2_Eigen);
    cv2eigen(_b3, b3_Eigen);

    cv2eigen(_iw1, iw1_Eigen);
    cv2eigen(_iw2, iw2_Eigen);
    cv2eigen(_iw3, iw3_Eigen);
    cv2eigen(_iw4, iw4_Eigen);


    fs.release();

    out.setX1Offset(x1_offset_Eigen);
    out.setX1Gain(x1_gain_Eigen);
    out.setX1Ymin(_x1_ymin.at<int>(0, 0));

    out.setB1(b1_Eigen);
    out.setB2(b2_Eigen);
    out.setB3(b3_Eigen);
    out.setB4(_b4.at<float>(0, 0));

    out.setIW1(iw1_Eigen);
    out.setIW2(iw2_Eigen);
    out.setIW3(iw3_Eigen);
    out.setIW4(iw4_Eigen);

    //:::Testing NN prediction:::
    if (debug) {
        Eigen::MatrixXf t_test(15, 1);
        t_test
                << 0.3253, 0.3222, 0.1024, 0.9378, 0.6313, 0.6181, 0.1163, 0.0305, 0.1735, 0.1798, 0.4400, 0.7207, 0.8877, 0.9864, 0.8080;
        float res = out.computeThreeNeuronsTanSig(t_test)(0);
        COVIS_MSG_WARN(
                "Test NN, feature\n t = [0.3253, 0.3222, 0.1024, 0.9378, 0.6313, 0.6181, 0.1163, 0.0305, 0.1735, 0.1798, 0.4400, 0.7207, 0.8877, 0.9864, 0.8080]\n Prediction: "
                        << res);
    }
    return out;
}


void thresholdScenePTFs(pcl::PointCloud<PTF> ptfs_scene_in, pcl::PointCloud<PTF_Index> ptfs_scene_idx_in,
                        pcl::PointCloud<PTF> &ptfs_scene_out, pcl::PointCloud<PTF_Index> &ptfs_scene_idx_out,
                        covis::core::FeedForwardNet ffNet, int minNN, int maxNN, bool debug) {

    int ptfSize = ptfs_scene_in.size();

  //  Eigen::MatrixXf pred = ffNet.getPredictions(ptfs_scene_in);
   // std::cout << "predictions \n" << predictions << std::endl;

   /* std::cout << "predictions(0) " << pred.row(0) << std::endl;
    std::cout << "predictions(1) " << pred.row(1) << std::endl;
    std::cout << "predictions(1) " << pred.col(1) << std::endl; */
    for (int i = 0; i < ptfSize; i++) {
        PTF ptf = (ptfs_scene_in)[i];
        float res = ffNet.getPrediction(ptf);
       // if (i <2) std::cout << "res " << res << std::endl;
        if (res <= minNN || res >= maxNN) continue;
        ptfs_scene_out.push_back(ptf);
        ptfs_scene_idx_out.push_back((ptfs_scene_idx_in)[i]);
    }

    if (debug)
        COVIS_MSG_INFO("In: " << ptfSize << ", after thresholding [" << minNN << ", " << maxNN << "] left: "
                              << ptfs_scene_idx_out.size() << " scene PTFs.");
}

//******FOR PRECISION RECALL CURVES!!!!!!!!!!!*************************************************************************
void printToFile(float result, int doNotHaveGT, std::ofstream &res_file, int corrSize,
                   covis::core::Correspondence::Vec corr, std::vector<int> correct) {

    float procCorrect = result / (float) (corrSize - doNotHaveGT) * 100;
    COVIS_MSG_WARN("Correct matched " << result << " out of " << (corrSize - doNotHaveGT) << " and in proc " <<procCorrect << "%");

        for (size_t i = 0; i < corr.size(); i++) {
            res_file << corr[i].distance[0] << "\t";
            if (correct[i] == -1) res_file << "0" << "\t" << "0";
            else res_file << correct[i] << "\t" << "1";
            res_file << "\n";
        }
}

bool containsId(std::vector<int> random_points, int idx1) {
    for (size_t i = 0; i < random_points.size(); i++) {
        if (random_points[i] == idx1) return true;
    }
    return false;
}

inline float checkMatches(std::vector<std::vector<int> > scene_gt, covis::core::Correspondence::Vec corr,
                                                         int &doNotHaveGT, std::vector<int> &correct) {
    float res = 0.0f;
    std::cout << "corr.size " << corr.size() << "\n";

    for (size_t i = 0; i < corr.size(); i++) {
        int target_index = corr[i].query;
        int source_index = corr[i].match[0];

        std::vector<int> target_gt_indexes = scene_gt[target_index];

        if (target_gt_indexes.size() == 0) {
            correct.push_back(-1);
            doNotHaveGT++;
            continue;
        }

        bool found_id1 = containsId(target_gt_indexes, source_index);
        if (!found_id1) {
            correct.push_back(0);
            continue;
        }
        correct.push_back(1);

        res = res + 1.0;
    }
    std::cout << "Without GT : " << doNotHaveGT << "\n";
    std::cout << "correct.size " << correct.size() << "\n";
    return res;
}
template<typename T>
int getCorrectCorrespondences(typename pcl::PointCloud<T>::Ptr target, typename pcl::PointCloud<T>::Ptr source,
                              std::vector<std::vector<int> > &indexes, Eigen::Matrix4f pose, float _gt_radius) {

    indexes.resize(target->size());
    int amount = 0;
    //transform Target into source and get corresponding indexes
    typename pcl::PointCloud<T>::Ptr target_transformed(new pcl::PointCloud<T>());
    pcl::transformPointCloud<T>(*target, *target_transformed, pose.inverse());

    typename pcl::search::Search<T>::Ptr s;

    s.reset(new pcl::search::KdTree<T>);
    s->setInputCloud(source);

    for (size_t i = 0; i < target_transformed->size(); i++) {

        T p1 = (*target_transformed)[i];
        std::vector<int> pointsIdxP1;
        std::vector<float> pointsSquaredDistP1;
        s->radiusSearch(p1, _gt_radius, pointsIdxP1, pointsSquaredDistP1);

        indexes[i] = pointsIdxP1;

        if (pointsIdxP1.size() > 0) {
            amount++;
        }
    }
    return amount;
}
//*************************
void createFile(std::string path, std::string short_name_object, std::string short_name_scene, std::ofstream& file,
std::string info){
    std::stringstream result_poses_filename;
    createDir(path);
    result_poses_filename << path << "/" << short_name_object << "-" << short_name_scene << ".xf";
    file.open(result_poses_filename.str().c_str(), std::ofstream::out);
    COVIS_MSG_INFO("Saving " <<info <<" into: " << result_poses_filename.str());
}

//*********************************************************************************************************************
void savePoses(std::string path, std::string short_name_object, std::string short_name_scene,
    std::vector<Eigen::Matrix4f> poses, pcl::PointCloud<PointNT>::Ptr object_normals,
               pcl::PointCloud<PointNT>::Ptr scene_normals){
    std::ofstream pose_file;
    createFile(path, short_name_object, short_name_scene, pose_file, "found poses");

    typename pcl::PointCloud<PointNT>::Ptr downsampled_obj(new pcl::PointCloud<PointNT>());
    typename pcl::PointCloud<PointNT>::Ptr downsampled_scn(new pcl::PointCloud<PointNT>());
    downsamplePointCloud<PointNT>(object_normals, downsampled_obj, 0.003);
    downsamplePointCloud<PointNT>(scene_normals, downsampled_scn, 0.003);

    for (size_t i = 0; i < poses.size(); i++) {
        Eigen::Matrix4f pose = doICP<PointNT>(poses[i], downsampled_obj, downsampled_scn);
        pose_file << pose << "\n";
    }
    pose_file.close();
}

//*************************************
int main(int argc, const char **argv) {
    // Setup program options
    core::ProgramOptions po;
    po.addPositional("object", "query point cloud");
    po.addPositional("scene", "target point cloud");

    po.addOption("ringsObject", 10, "number of rings per object");
    po.addOption("ringsScene", 10, "number of rings per scene");
    po.addOption("keepObject", 100, "PTFs to keep on object, used mostly for loading specific file");
    po.addOption("objectInstances", 1, "define the number of object instances to find pose for");

    po.addOption("save", "none", "set which PTFs to save - object, scene, both, none");
    po.addOption("savePosesClustering", "", "set path to save found poses from Pose Clustering");
    po.addOption("savePosesRansac", "", "set path to save found poses from RANSAC");

    po.addOption("nn", "", "load trained neural network (opencv xml) for scene frequency ptf prediction");
    po.addOption("nnMin", 0, "set min (integer) for thresholding NN predictions");
    po.addOption("nnMax", 5, "set max (integer) for thresholding NN predictions");

    po.addOption("gtPose", "", "set the path to ground truth pose if data for PRC need to be saved, rad for gt is 1cm");
    po.addOption("saveForPRC", "", "set path for data for PRC");

    po.addFlag('d', "debug", "enable debug");
    po.addFlag('l', "loadPTFsObject", "load precomputed PTFs from store file - {object_keepObject}.pcd");
    po.addFlag('v', "visualizePoses", "enable pose visualization");


    // Parse
    if (!po.parse(argc, argv))
        return 1;

    int ringsObject = po.getValue<int>("ringsObject");
    int ringsScene = po.getValue<int>("ringsScene");
    int keepObject = po.getValue<int>("keepObject");
    int objectInstances = po.getValue<int>("objectInstances");
    std::string save = po.getValue("save");
    std::string savePosesClustering = po.getValue("savePosesClustering");
    std::string savePosesRansac = po.getValue("savePosesRansac");
    std::string saveForPRC = po.getValue("saveForPRC");
    std::string gtPose = po.getValue("gtPose");

    std::string nnXML = po.getValue("nn");
    int nnMin = po.getValue<int>("nnMin");
    int nnMax = po.getValue<int>("nnMax");


    bool debug = po.getFlag("debug");
    bool loadPTFsObject = po.getFlag("loadPTFsObject");
    bool visualizePoses = po.getFlag("visualizePoses");

    pcl::PointCloud<PointNT>::Ptr object_normals(new pcl::PointCloud<PointNT>());
    pcl::PointCloud<PointNT>::Ptr scene_normals(new pcl::PointCloud<PointNT>());


    pcl::io::loadPCDFile<PointNT>(po.getValue("object"), *object_normals);
    pcl::io::loadPCDFile<PointNT>(po.getValue("scene"), *scene_normals);
    COVIS_MSG_ERROR("Object: " << po.getValue("object"));
    COVIS_MSG_ERROR("Scene: " << po.getValue("scene"));

    float distance = getMaxDistance<PointNT>(object_normals) / 2;

    pcl::PointCloud<PTF> ptfs_object, ptfs_scene;
    pcl::PointCloud<PTF_Index> ptfs_idx_object, ptfs_idx_scene;

    PTFExtraction<PointNT> ptfe;
    ptfe.setMaximumDistance(distance);
    ptfe.setDebug(debug);

    pcl::PointCloud<PointNT>::Ptr object_keypoints(new pcl::PointCloud<PointNT>());
    pcl::PointCloud<PointNT>::Ptr scene_keypoints(new pcl::PointCloud<PointNT>());
    {
        core::ScopedTimer("Downsample point clouds");
        downsamplePointCloud<PointNT>(object_normals, object_keypoints, 0.01);
        downsamplePointCloud<PointNT>(scene_normals, scene_keypoints, 0.01);
    }

    std::string short_name_object = finShortNameForFile(po.getValue("object"));
    std::string short_name_scene = finShortNameForFile(po.getValue("scene"));

    //***Object****
    if (loadPTFsObject) {
        std::stringstream object_file_name, object_file_name_idx;
        object_file_name << short_name_object << "_" << keepObject << ".pcd";
        object_file_name_idx << short_name_object << "_" << keepObject << "_index.pcd";
        COVIS_MSG_INFO("Loading : " << object_file_name.str() << " and " << object_file_name_idx.str());
        pcl::io::loadPCDFile(object_file_name.str(), ptfs_object);
        pcl::io::loadPCDFile(object_file_name_idx.str(), ptfs_idx_object);
    } else {
        {
            core::ScopedTimer("Compute PTFs for Object");
            ptfe.compute(object_keypoints, ptfs_object, ptfs_idx_object, ringsObject);
        }
        if (save == "object" || save == "both") saveFile(ptfs_object, ptfs_idx_object, short_name_object);
    }
    //***Scene****
    {
        core::ScopedTimer("Compute PTFs for Scene");
        ptfe.compute(scene_keypoints, ptfs_scene, ptfs_idx_scene, ringsScene);
    }
    if (save == "scene" || save == "both") saveFile(ptfs_scene, ptfs_idx_scene, short_name_scene);

    //***Threshold scene PTFs using NN predictions***
    if (nnXML != "") {
        if (debug) COVIS_MSG_INFO("Loading : " << nnXML << " file.");
        covis::core::FeedForwardNet ffNet = loadNN(nnXML, debug);
        pcl::PointCloud<PTF> ptfs_scene_out;
        pcl::PointCloud<PTF_Index> ptfs_idx_scene_out;

        thresholdScenePTFs(ptfs_scene, ptfs_idx_scene, ptfs_scene_out, ptfs_idx_scene_out, ffNet, nnMin, nnMax, debug);

        ptfs_scene = ptfs_scene_out;
        ptfs_idx_scene = ptfs_idx_scene_out;
    }

    //***Match PTFs****
    PTFMatching<PointNT> ptfm;
    ptfm.setQueryKeypoints(object_keypoints);
    ptfm.setTargetKeypoints(scene_keypoints);
    ptfm.setPTFsQuery(ptfs_object, ptfs_idx_object);
    ptfm.setPTFsTarget(ptfs_scene, ptfs_idx_scene);
    if (savePosesRansac != "") ptfm.setUseRANSAC(true);
    ptfm.setDebug(debug);
    ptfm.setMaxDistance(distance);
    ptfm.setNumberOfPoses(objectInstances);
    ptfm.match();

    std::vector<Eigen::Matrix4f> poses_pose_clustering = ptfm.getPosesFromPoseClustering();
    COVIS_MSG_WARN("Found " << poses_pose_clustering.size() << " object poses! ");
    Eigen::Matrix4f pose_ransac = ptfm.getPoseFromRANSAC();


    if (visualizePoses) displayFoundObjects<PointNT>(object_normals, scene_normals, poses_pose_clustering);

    //***Saving found poses
    if (savePosesClustering != "") {
        savePoses(savePosesClustering, short_name_object, short_name_scene, poses_pose_clustering, object_normals,
                scene_normals);

    }

    if (savePosesRansac != "") {
        std::vector<Eigen::Matrix4f> poses_from_ransac;
        poses_from_ransac.push_back(pose_ransac);
        savePoses(savePosesRansac, short_name_object, short_name_scene, poses_from_ransac, object_normals,
                  scene_normals);
    }

    //save data for PRC
    if (saveForPRC != "") {
        std::ofstream file;
        createFile(saveForPRC, short_name_object, short_name_scene, file, "PRC");
        Eigen::Matrix4f pose;
        covis::util::loadEigen(gtPose, pose, true);
        //find gt for each point
        std::vector<std::vector<int> > gt_indexes;
        getCorrectCorrespondences<PointNT>(scene_keypoints, object_keypoints, gt_indexes, pose, 0.01);
        covis::core::Correspondence::Vec corrRansac = ptfm.getRansacCorr();
        COVIS_ASSERT_MSG(corrRansac.size() > 0, "Should be more than 0 corr! Right now corr size: " << corrRansac.size());
        int doNotHaveGTVoting = 0;
        std::vector<int> correctOut;
        float resultVoting = checkMatches(gt_indexes, corrRansac, doNotHaveGTVoting, correctOut);
        printToFile(resultVoting, doNotHaveGTVoting, file, corrRansac.size(), corrRansac, correctOut);

    }

    return 0;
}
