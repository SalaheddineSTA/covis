// Copyright (c) 2013, University of Southern Denmark
// All rights reserved.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice,
//    this list of conditions and the following disclaimer.
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation
//    and/or other materials provided with the distribution.
// 3. Neither the name of the University of Southern Denmark nor the names of
//    its contributors may be used to endorse or promote products derived from
//    this software without specific prior written permission.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL THE UNIVERSITY OF SOUTHERN DENMARK BE
// LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
// CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
// SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
// INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.

#include <covis/covis.h>
using namespace covis;

// Point type
typedef pcl::PointXYZ PointT;

/*
 * Main entry point
 */
int main(int argc, const char** argv) {
    // Setup program options
    core::ProgramOptions po;
    po.addPositional("scene [object]",
            "input point cloud file(s) - if no object is specified, self-occlusion is tested",
            2);
    po.addOption<double>("resolution", -1, "octree resolution (<= 0 for automatic)");
    
    // Parse
    if(!po.parse(argc, argv))
        return 1;
    
    const std::vector<std::string> pcdfiles = po.getVector("scene [object]");
    
    // Load point cloud
    COVIS_MSG("Loading point cloud...");
    pcl::PointCloud<pcl::PointXYZ>::Ptr target(new pcl::PointCloud<pcl::PointXYZ>);
    COVIS_ASSERT(pcl::io::loadPCDFile(pcdfiles[0], *target) == 0);
    std::vector<int> dummy;
    pcl::removeNaNFromPointCloud(*target, *target, dummy);

    pcl::PointCloud<pcl::PointXYZ>::Ptr cloud(new pcl::PointCloud<pcl::PointXYZ>);
    if(pcdfiles.size() == 1) {
        COVIS_MSG_INFO("No object file specified, testing with scene only...");
        cloud = target;
    } else {
        COVIS_ASSERT(pcl::io::loadPCDFile(pcdfiles[1], *cloud) == 0);
        pcl::removeNaNFromPointCloud(*cloud, *cloud, dummy);
    }
    
    COVIS_ASSERT(!cloud->empty() && !target->empty());
    
    // Run
    pcl::PointCloud<pcl::PointXYZ>::Ptr visible;
    pcl::PointCloud<pcl::PointXYZ> occluded;
    {
        core::ScopedTimer t("Occlusion removal");
        filter::OcclusionRemoval<pcl::PointXYZ> occ;
        occ.setTarget(target);
        occ.setResolution(po.getValue<double>("resolution"));
        visible = occ.filter(cloud);
        occluded = core::mask(*cloud, occ.getMask(), true);
    }
    
    // Show
    COVIS_MSG("Visualizing results...");
    visu::Visu3D v;
    v.addPointCloud<pcl::PointXYZ>(target, "target");
    v.addColor<pcl::PointXYZ>(visible, 0, 255, 0, "visible)");
    v.addColor<pcl::PointXYZ>(occluded.makeShared(), 255, 0, 0, "occluded");
    v.show();
    
    return 0;
}
