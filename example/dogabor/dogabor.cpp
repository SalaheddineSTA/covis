// Boost
#include <boost/algorithm/string/split.hpp>
#include <boost/algorithm/string/classification.hpp>
#include <boost/lexical_cast.hpp>

// STL
#include <iostream>
#include <string>
#include <fstream>
#include <cmath>

// Covis
#include <covis/covis.h>
using namespace covis;

int main(int argc, const char** argv) {
    // Setup program options
    core::ProgramOptions po;
    po.addPositional("imgfile", "image file");
    po.addPositional("imgfile2", "image file 2");
    po.addPositional("depthfile", "depth file");
    po.addPositional("depthfile2", "depth file 2");

    po.addOption("maskfile", 'm', "", "mask file");
    po.addOption("frequency", 'f', 0.2, "kernel frequency");
    po.addOption("sigma", 's', 0.795, "kernel standard deviation");
    po.addOption("scale", 'a', 4, "kernel scale");
    po.addOption("theta", 't', 24, "kernel rotation");
    po.addOption("k", 'k', 2, "Gabor scaling parameter");
    po.addOption("kernel-size", 'e', 20, "kernel size");
    po.addOption("patch-size", 'p', 320, "patch size");
    po.addOption("radius-search-size", 'r', 0.05, "radius size");

    // Parse
    if (!po.parse(argc, argv))
        return 1;

    COVIS_MSG("Loading images...");
    const cv::Mat imageOriginal = cv::imread(po.getValue("imgfile"));
    COVIS_ASSERT(!imageOriginal.empty());

    const cv::Mat imageScene = cv::imread(po.getValue("imgfile2"));
    COVIS_ASSERT(!imageScene.empty());

    const cv::Mat depthOriginal = cv::imread(po.getValue("depthfile"), -1);
    COVIS_ASSERT(!depthOriginal.empty());

    const cv::Mat depthScene = cv::imread(po.getValue("depthfile2"), -1);
    COVIS_ASSERT(!depthScene.empty());

    cv::Mat mask;
    if(!po.getValue("maskfile").empty())
        mask = cv::imread(po.getValue("maskfile"), -1);

    COVIS_MSG("Creating Gabor filters...");
    feature::DOGabor::Detector detector = feature::DOGabor::SIFT;

    feature::DOGabor matcher(detector, po.getValue<float>("frequency"), po.getValue<float>("sigma"),
            po.getValue<int>("scale"), po.getValue<int>("theta"), po.getValue<float>("k"),
            po.getValue<float>("kernel-size"), po.getValue<float>("patch-size"),
            po.getValue<float>("radius-search-size"));

    COVIS_MSG("Computing keypoints and descriptors for first image...");
    std::vector<cv::KeyPoint> keypoints1;
    cv::Mat descriptor1;

    // TODO: Don't hard-code calibration
    cv::Matx33f K(1050, 0, 639.5, 0, 1050, 479.5, 0, 0, 1);

    float distance = -1;
    matcher.compute(imageOriginal, depthOriginal, mask, K, distance, keypoints1, descriptor1, true);

    std::vector<cv::KeyPoint> keypoints2;
    cv::Mat descriptor2;

    COVIS_MSG("Computing keypoints and descriptors for second image...");
    matcher.compute(imageScene, depthScene, K, distance, keypoints2, descriptor2, false);

    COVIS_MSG("Matching descriptors...");
    cv::BFMatcher l2matcher;
    std::vector<std::vector<cv::DMatch> > SIFTmatches;
    l2matcher.knnMatch(descriptor1, descriptor2, SIFTmatches, 2);

    std::vector<cv::DMatch> goodMatches;
    for (int i = 0; i < descriptor1.rows; i++)
        if (SIFTmatches[i][0].distance < 0.8 * SIFTmatches[i][1].distance)
            goodMatches.push_back(SIFTmatches[i][0]);

    cv::Mat imgMatches;
    cv::drawMatches(imageOriginal, keypoints1, imageScene, keypoints2, goodMatches, imgMatches, cv::Scalar::all(-1),
            cv::Scalar::all(-1), std::vector<char>(), cv::DrawMatchesFlags::NOT_DRAW_SINGLE_POINTS);

    std::vector<cv::Point2f> obj;
    std::vector<cv::Point2f> scene;
    for (size_t i = 0; i < goodMatches.size(); i++) {
        obj.push_back(keypoints1[goodMatches[i].queryIdx].pt);
        scene.push_back(keypoints2[goodMatches[i].trainIdx].pt);
    }

    COVIS_MSG("Finding homography...");
    cv::Mat H = cv::findHomography(obj, scene, cv::RANSAC);

    //-- Get the corners from the image_1 ( the object to be "detected" )
    std::vector<cv::Point2f> objCorners;
    if(mask.empty()) {
        objCorners.resize(4);
        objCorners[0] = cv::Point2f(0, 0);
        objCorners[1] = cv::Point2f(0, imageOriginal.rows-1);
        objCorners[2] = cv::Point2f(imageOriginal.cols-1, imageOriginal.rows-1);
        objCorners[3] = cv::Point2f(imageOriginal.cols-1, 0);
    } else {
        std::vector<std::vector<cv::Point> > contours;
        std::vector<cv::Vec4i> hierarchy;
        cv::findContours(mask, contours, hierarchy, cv::RETR_TREE, cv::CHAIN_APPROX_SIMPLE, cv::Point(0, 0));
        for (int i = 0; i < int(contours[0].size()); i++)
            objCorners.push_back(contours[0][i]);
    }

    if (H.cols) {
        std::vector<cv::Point2f> sceneCorners;
        cv::perspectiveTransform(objCorners, sceneCorners, H);

        for (size_t i = 0; i + 1 < sceneCorners.size(); i++)
            cv::line(imgMatches, sceneCorners[i] + cv::Point2f(imageOriginal.cols, 0),
                    sceneCorners[i + 1] + cv::Point2f(imageOriginal.cols, 0), cv::Scalar(0, 255, 0), 8);
        cv::line(imgMatches, sceneCorners[sceneCorners.size() - 1] + cv::Point2f(imageOriginal.cols, 0),
                sceneCorners[0] + cv::Point2f(imageOriginal.cols, 0), cv::Scalar(0, 255, 0), 8);

        for (size_t i = 0; i + 1 < objCorners.size(); i++)
            cv::line(imgMatches, objCorners[i], objCorners[i + 1], cv::Scalar(0, 255, 0), 8);
        cv::line(imgMatches, objCorners[objCorners.size() - 1], objCorners[0], cv::Scalar(0, 255, 0), 8);

        cv::imshow("Homography estimation result", imgMatches);
        cv::waitKey();

    } else {
        COVIS_MSG_WARN("No homography found!");
    }

    return 0;
}
