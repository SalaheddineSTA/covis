# Script for automated generation of all examples.
#
# To make a new working example, create a subfolder <my_example> containing a single source file.
# The name of this source file MUST be in this format: <my_example.cpp>, otherwise an error will occur.
#

# Find all subdirs and create targets
# See: http://stackoverflow.com/questions/7787823
file(GLOB proto_children RELATIVE ${CMAKE_CURRENT_SOURCE_DIR} ${CMAKE_CURRENT_SOURCE_DIR}/*/*cpp)
set(children "")
foreach(child ${proto_children})
  string(REGEX REPLACE "/.*cpp" "" child ${child})
  list(APPEND children ${child})
endforeach()
list(REMOVE_DUPLICATES children)
set(exlist "") # Example target names
foreach(child ${children})
  # Go to subdir
  if(IS_DIRECTORY ${CMAKE_CURRENT_SOURCE_DIR}/${child})
    # Relative path to example source file
    set(exfile ${child}/${child}.cpp)

    # Check for valid sorce file
    if(NOT EXISTS ${CMAKE_CURRENT_SOURCE_DIR}/${exfile})
      message(FATAL_ERROR "Please provide the following source file: "
                          "\"${CMAKE_CURRENT_SOURCE_DIR}/${exfile}\" "
                          "for the \"${child}\" example!")
    endif()

    # Store target name in list
    list(APPEND exlist ${child})
  endif()
endforeach()
list(SORT exlist)

set(COVIS_EXAMPLES "${exlist}" CACHE STRING "CoViS examples" FORCE)
