// Copyright (c) 2013, University of Southern Denmark
// All rights reserved.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice,
//    this list of conditions and the following disclaimer.
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation
//    and/or other materials provided with the distribution.
// 3. Neither the name of the University of Southern Denmark nor the names of
//    its contributors may be used to endorse or promote products derived from
//    this software without specific prior written permission.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL THE UNIVERSITY OF SOUTHERN DENMARK BE
// LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
// CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
// SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
// INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.

#include <covis/covis.h>
using namespace covis;

// Loaded point cloud and computed histogram features
typedef pcl::PointXYZRGBNormal PointT;
pcl::PointCloud<PointT>::Ptr fcloud;
Eigen::VectorXf feat;
std::string fname;
float res;
float frad;
double scale;

// Viz
visu::Visu3D viz;
pcl::PolygonMesh::Ptr mesh;

// Point picker callback, shows a histogram of chosen feature
void callback(const pcl::visualization::PointPickingEvent& ppe, void*);

float entropy(const Eigen::VectorXf& feat);

/*
 * Main entry point
 */
int main(int argc, const char** argv) {
    // Setup program options
    core::ProgramOptions po;
    po.addPositional("query", "point cloud file");
    po.addOption("resolution", 'r', 0, "downsample to this resolution before computing features (<= 0 for no downsampling)");
    po.addOption("radius-normal", 'n', 0.01, "normal estimation radius");
    po.addOption("radius-feature", 'f', 0.025, "histogram estimation radius");
    po.addOption("feature", "ppfhist", "name the features to compute - possible names are: " + feature::FeatureNames);
    po.addFlag('o', "orient-normals", "set this flag to point normals outwards");
    po.addFlag("concave", "set this flag to assume concave models during normal orientation");
    po.addOption("scale-image", 's', 10, "set a scaling factor on the images used to show the Spin Image and PPF Histogram features");
    
    // Parse
    if(!po.parse(argc, argv))
        return 1;

    const std::string query = po.getValue("query");
    res = po.getValue<float>("resolution");
    const float nrad = po.getValue<float>("radius-normal");
    frad = po.getValue<float>("radius-feature");
    fname = po.getValue("feature");
    scale = po.getValue<double>("scale-image");
    COVIS_ASSERT(scale > 0);
    
    // Load model
    mesh.reset(new pcl::PolygonMesh);
    COVIS_ASSERT(util::load(query, *mesh));
    fcloud.reset(new pcl::PointCloud<PointT>);
    fcloud = filter::preprocess<PointT>(mesh, 1, true, 0, res, nrad, po.getFlag("orient-normals"), po.getFlag("concave"), true);
    COVIS_ASSERT(!fcloud->empty());
    
    // Setup a point cloud visualizer
    viz.addPointCloud<PointT>(fcloud, "cloud");
    viz.visu.registerPointPickingCallback(callback);
    viz.show();

    if(res <= 0)
        res = MAX(0.001, detect::computeResolution<PointT>(fcloud));
    
    return 0;
}

void callback(const pcl::visualization::PointPickingEvent& ppe, void*) {
    const int idx = ppe.getPointIndex();
    if(idx >= 0 && idx < int(fcloud->size())) {
        COVIS_MSG("Picked point " << idx << ": " << fcloud->points[idx]);

        // Compute feature
        pcl::PointCloud<PointT>::Ptr tmp(new pcl::PointCloud<PointT>(1, 1, fcloud->points[idx]));
        feat = feature::computeFeature<PointT>(fname, tmp, fcloud, frad);
        COVIS_MSG("\tEntropy of descriptor: " << entropy(feat));

        // Show feature point using a sphere
        {
            if(!viz.visu.updateSphere(fcloud->points[idx], 2 * res, 1, 0, 0, "sphere_point"))
                viz.visu.addSphere(fcloud->points[idx], 2 * res, 1, 0, 0, "sphere_point");
            vtkLODActor *actor = dynamic_cast<vtkLODActor *>((*viz.visu.getShapeActorMap())["sphere_point"].Get());
            actor->GetProperty()->SetOpacity(1);

            // Increase the fidelity of the sphere
            vtkSphereSource *source = dynamic_cast<vtkSphereSource *>(actor->GetMapper()->GetInputAlgorithm());
            source->SetPhiResolution(100);
            source->SetThetaResolution(100);
            source->Update();
        }

        // Same thing for feature radius
        {
            if(!viz.visu.updateSphere(fcloud->points[idx], frad, 1, 1, 0, "sphere_radius"))
                viz.visu.addSphere(fcloud->points[idx], frad, 1, 1, 0, "sphere_radius");
            vtkLODActor *actor = dynamic_cast<vtkLODActor *>((*viz.visu.getShapeActorMap())["sphere_radius"].Get());
            actor->GetProperty()->SetOpacity(0.5);

            vtkSphereSource *source = dynamic_cast<vtkSphereSource *>(actor->GetMapper()->GetInputAlgorithm());
            source->SetPhiResolution(100);
            source->SetThetaResolution(100);
            source->Update();
        }

        // If we have a mesh, show that now
        if(mesh->polygons.size() > 0) {
            viz.addMesh(mesh);
            viz.visu.setPointCloudRenderingProperties(pcl::visualization::PCL_VISUALIZER_OPACITY, 0, "cloud");
        }

        // Update the window before showing histogram below
        viz.visu.getRenderWindow()->Render();

        COVIS_MSG("Plotting histogram (press any key to close plot)...");
#ifdef OPENCV_3
        const int minmax = cv::NORM_MINMAX;
#else
        const int minmax = CV_MINMAX;
#endif
        if(fname == "ndhist") { // Special case: ND histogram
            cv::Mat_<float> img(8, 16, feat.data());
            img = img.t();
            cv::normalize(img, img, 1, 0, minmax);
            cv::resize(img, img, cv::Size(), scale, scale, cv::INTER_CUBIC);
            cv::imshow("Visualization of ND histogram", img);
            cv::waitKey();
        } else if(fname == "ppfhist") { // Special case: PPF histogram
            cv::Mat_<float> img(16, 32, feat.data());
            img = img.t();
            cv::normalize(img, img, 1, 0, minmax);
            cv::resize(img, img, cv::Size(), scale, scale, cv::INTER_CUBIC);
            cv::imshow("Visualization of PPF histogram", img);
            cv::waitKey();
        } else if(fname == "ppfhistfull") { // Special case: PPF histogram, full
            cv::Mat_<float> img(3 * 16, 32, feat.data());
            img = img.t();
            cv::normalize(img, img, 1, 0, minmax);
            cv::resize(img, img, cv::Size(), scale, scale, cv::INTER_CUBIC);
            cv::imshow("Visualization of full PPF histogram", img);
            cv::waitKey();
        } else if(fname == "si") { // Special case: Spin Image
            cv::Mat_<float> img(9, 17, feat.data());
            img = img.t();
            cv::normalize(img, img, 1, 0, minmax);
            cv::resize(img, img, cv::Size(), scale, scale, cv::INTER_CUBIC);
            cv::imshow("Visualization of Spin Image", img);
            cv::waitKey();
        } else {
            visu::HistVisu<> hv(feat.size());
            hv.setTitle("Visualization of feature " + fname);
            hv.setHist(feat.data());
            hv.show();
        }

        // Remove mesh again
        if(mesh->polygons.size() > 0) {
            viz.visu.removeShape("mesh");
            viz.visu.setPointCloudRenderingProperties(pcl::visualization::PCL_VISUALIZER_OPACITY, 1, "cloud");
        }

        COVIS_MSG("\tDone!");
    }
}

float entropy(const Eigen::VectorXf& feat) {
    float result = 0;

    for(int i = 0; i < feat.size(); ++i)
        if(feat(i) > 0)
            result += -feat(i) * logf(feat(i));

    return result;
}
