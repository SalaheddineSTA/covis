// Copyright (c) 2013, University of Southern Denmark
// All rights reserved.
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met:
//
// 1. Redistributions of source code must retain the above copyright notice,
//    this list of conditions and the following disclaimer.
// 2. Redistributions in binary form must reproduce the above copyright notice,
//    this list of conditions and the following disclaimer in the documentation
//    and/or other materials provided with the distribution.
// 3. Neither the name of the University of Southern Denmark nor the names of
//    its contributors may be used to endorse or promote products derived from
//    this software without specific prior written permission.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
// AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
// IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
// ARE DISCLAIMED. IN NO EVENT SHALL THE UNIVERSITY OF SOUTHERN DENMARK BE
// LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
// CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
// SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
// INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
// CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
// ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
// POSSIBILITY OF SUCH DAMAGE.

#include <covis/covis.h>
using namespace covis;

#include <pcl/point_traits.h>
#include <pcl/features/normal_3d_omp.h>
#include <pcl/features/impl/shot_lrf_omp.hpp>
#include <pcl/features/impl/shot_omp.hpp>
#include <pcl/filters/filter.h>
#include <pcl/filters/voxel_grid.h>
#include <pcl/io/pcd_io.h>
#include <pcl/visualization/pcl_visualizer.h>
#include <pcl/registration/correspondence_rejection_sample_consensus.h>
#include <pcl/registration/correspondence_rejection_trimmed.h>
#include <pcl/registration/icp.h>

// Point and feature/RF types
typedef pcl::PointXYZRGBNormal PointT;
//typedef pcl::SHOT352 FeatureT;
typedef pcl::ReferenceFrame RFT;

// Loaded point clouds and computed histogram features/RFs
pcl::PointCloud<PointT>::Ptr query, target;
feature::MatrixT fquery, ftarget;
pcl::PointCloud<RFT>::Ptr rfquery, rftarget;

/*
 * Main entry point
 */
int main(int argc, const char** argv) {
    // Setup program options
    core::ProgramOptions po;
    po.addPositional("query", "mesh or point cloud file for query model");
    po.addPositional("target", "mesh or point cloud file for target model");
    po.addOption("resolution", 'r', 0.002, "downsample to this resolution (<= 0 for disabled)");
    po.addOption("radius-normal", 'n', 0.01, "normal estimation radius");
    po.addOption("feature", "si", "choose which feature to use from this list: " + feature::FeatureNames);
    po.addOption("radius-feature", 'f', 0.015, "feature estimation radius");
    po.addFlag('s', "show-pose", "show the relative pose of the single top voted correspondence");
    po.addFlag('c', "show-colors", "show correspondences using colors instead of lines");
    po.addOption("show-top", 't', 100, "show this number of top ranked correspondences");
    
    // Parse
    if(!po.parse(argc, argv))
        return 1;

    po.print();
    
    const float res = po.getValue<float>("resolution");
    const float nrad = po.getValue<float>("radius-normal");
    const float hrad = po.getValue<float>("radius-feature");
    const bool showColors = po.getFlag("show-colors");
    const size_t top = po.getValue<size_t>("show-top");
    
    // Load point clouds
    pcl::PolygonMesh::Ptr mquery(new pcl::PolygonMesh);
    pcl::PolygonMesh::Ptr mtarget(new pcl::PolygonMesh);
    util::load(po.getValue("query"), *mquery);
    util::load(po.getValue("target"), *mtarget);

    // Preprocess
    query = filter::preprocess<PointT>(mquery, 1, true, 0, res, nrad, true, false, true);
    target = filter::preprocess<PointT>(mtarget, 1, true, 0, res, nrad, false, false, true);
    COVIS_ASSERT(!query->empty() && !target->empty());
    
    /*
     * Compute RFs
     */
    rfquery.reset(new pcl::PointCloud<RFT>);
    rftarget.reset(new pcl::PointCloud<RFT>);
    {
        core::ScopedTimer t("Reference frames");
        pcl::SHOTLocalReferenceFrameEstimationOMP<PointT,RFT> rfest;
        rfest.setRadiusSearch(hrad);
        
        rfest.setInputCloud(query);
        rfest.compute(*rfquery);
        
        rfest.setInputCloud(target);
        rfest.compute(*rftarget);
    }

    /*
     * Compute features
     */
    {
        const std::string feat = po.getValue("feature");
        core::ScopedTimer t("Features (" + feat + ")");

        fquery = feature::computeFeature<PointT>(feat, query, hrad);
        ftarget = feature::computeFeature<PointT>(feat, target, hrad);
    }
    
    
    /*
     * Match features
     */
    core::Correspondence::Vec corr;
    {
        core::ScopedTimer t("Feature matching");
        corr = *detect::computeKnnMatches(fquery, ftarget, 2);
    }
    
    /*
     * Show input L2 feature correspondences
     */
    if(showColors)
        visu::showCorrespondences<PointT>(query, target, corr, 1, visu::COLORS,
                "Input L2 correspondences");
    else
        visu::showCorrespondences<PointT>(query, target, corr, 10,  visu::LINES,
                                          "Input L2 correspondences");
    
    /*
     * Show L2 feature correspondences after cutoff
     */
    core::Correspondence::Vec corrSort = corr;
    core::sort(corrSort);
    if(showColors) {
        corrSort.resize(corrSort.size() / 2);
        visu::showCorrespondences<PointT>(query, target, corrSort, 1,  visu::COLORS,
                                          "L2 correspondences, median cutoff (" +
                                          core::stringify(corrSort.size()) + "/" + core::stringify(corr.size()) + ")");
    } else {
        if(corrSort.size() > top)
            corrSort.resize(top);
        visu::showCorrespondences<PointT>(query, target, corrSort, 1,  visu::LINES,
                                          "Top " + core::stringify(corrSort.size()) + " L2 correspondences");
    }
    
    /*
     * Compute and show Lowe's feature ratio correspondences
     */
    core::Correspondence::VecPtr corrRatio;
    {
        core::ScopedTimer t("Lowe's feature ratio");
        corrRatio = detect::filterCorrespondencesRatio(corr);
    }
    
    if(showColors) {
        visu::showCorrespondences<PointT>(query, target, *corrRatio, 1, visu::COLORS,
                                          "Ratio correspondences (" +
                                          core::stringify(corrRatio->size()) + "/" + core::stringify(corr.size()) + ")");
    } else {
        if(corrRatio->size() > top)
            corrRatio->resize(top);
        visu::showCorrespondences<PointT>(query, target, *corrRatio, 1, visu::LINES,
                                          "Top " + core::stringify(corrRatio->size()) + " ratio correspondences");
    }
    
    /*
     * Perform voting-based correspondence rejection
     */
    detect::CorrespondenceVoting<PointT,RFT> voting;
    core::Correspondence::VecPtr corrv; 
    {
        core::ScopedTimer t("Voting-based correspondence rejection");
        voting.setQuery(query);
        voting.setQueryRF(rfquery);
        voting.setTarget(target);
        voting.setTargetRF(rftarget);
        corrv = voting.filter(corr);
    }
    
    /*
     * Show voting result
     */
    if(corrv->size() > 1) {
        if(showColors) {
            visu::showCorrespondences<PointT>(query, target, *corrv, 1, visu::COLORS,
                                              "Voting correspondences (" +
                                              core::stringify(corrv->size()) + "/" + core::stringify(corr.size()) + ")");
        } else {
            if(corrv->size() > top)
                corrv->resize(top);
            visu::showCorrespondences<PointT>(query, target, *corrv, 1,  visu::LINES,
                                              "Top " + core::stringify(corrv->size()) + " voting correspondences");
        }
        
        if(po.getFlag("show-pose")) {
            COVIS_MSG_INFO("Showing relative pose of best correspondence...");
            Eigen::Matrix4f T = voting.getTransformations()[0];
            visu::showDetection<PointT>(query, target, T);
        }
    } else {
        COVIS_MSG_WARN("Voting-based correspondence rejection failed!");
    }
    
    return 0;
}
