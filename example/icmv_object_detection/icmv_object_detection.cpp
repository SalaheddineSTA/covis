// We avoid defining PCL_NO_PRECOMPILE by manually including implementation headers
// See here: http://pointclouds.org/documentation/tutorials/adding_custom_ptype.php#example
#include <pcl/impl/pcl_base.hpp>
#include <pcl/point_types.h>
#include <pcl/point_traits.h>
#include <pcl/features/normal_3d_omp.h>
#include <pcl/io/pcd_io.h>
#include <pcl/visualization/pcl_visualizer.h>
#include <pcl/visualization/impl/point_cloud_geometry_handlers.hpp>
#include <pcl/registration/correspondence_rejection_sample_consensus.h>
#include <pcl/sample_consensus/impl/ransac.hpp>
#include <pcl/search/impl/kdtree.hpp>
#include <pcl/kdtree/impl/kdtree_flann.hpp>

#include <covis/covis.h>

using namespace covis::core;
using namespace covis::feature;
using namespace covis::visu;

// Desired histogram dimension
#define HIST_DIM 25
#define HIST_DIM_TWO HIST_DIM *2

// Point and feature type
typedef pcl::PointXYZRGBA PointT;
typedef pcl::Histogram<HIST_DIM> FeatureT;
typedef pcl::Histogram<HIST_DIM_TWO> FeatureTwo;

pcl::PointCloud<covis::core::CategorisedEdge>::Ptr shortEdgeQ, shortEdgeT;
// Loaded point clouds and computed histogram features
pcl::PointCloud<PointT>::Ptr query, target;
pcl::PointCloud<FeatureTwo>::Ptr fquery, ftarget;
pcl::PointCloud<FeatureTwo>::Ptr ftargetTwo;
pcl::PointCloud<FeatureT>::Ptr ftargetAlternativeDistance, ftargetAlternativeAngle;
// Feature needs instantiation into PCL
POINT_CLOUD_REGISTER_POINT_STRUCT(pcl::Histogram<HIST_DIM>, (float[HIST_DIM], histogram, histogram))
POINT_CLOUD_REGISTER_POINT_STRUCT(pcl::Histogram<HIST_DIM_TWO>, (float[HIST_DIM_TWO], histogram, histogram))

pcl::PointCloud<FeatureTwo>::Ptr getHistograms(pcl::PointCloud<PointT>::Ptr &cloud,
        pcl::PointCloud<covis::core::CategorisedEdge>::Ptr &edgeOut,  bool isTemplate);

using namespace covis;
using namespace pcl;
using namespace pcl::io;
using namespace pcl::console;

enum {EDGELABEL_NAN_BOUNDARY=1, EDGELABEL_OCCLUDING=2, EDGELABEL_OCCLUDED=4, EDGELABEL_HIGH_CURVATURE=8, EDGELABEL_RGB_CANNY=16};

//define canny parameters
float depth = 0.01f;
int max_search = 50; int canny_low = 75; int canny_hight = 150; // 50 100 20,60    50, 30, 70
float radiusDistance, radiusAngle;

int main(int argc, const char** argv) {
    core::ProgramOptions po;
    po.addPositional("pcd-query", "point cloud file for query point cloud");
    po.addPositional("pcd-target", "point cloud file for target point cloud");

    po.addOption("corrSize", 5, "correspondences search radius");
    po.addOption("radiusDistance", 0.03f, "radius for distance histogram");
    po.addOption("radiusAngle", 0.03f, "radius for angle histogram");

    po.addOption("d", "no", "enable directory: all -  for query/target, no - disable directory (default), query - only for query, target - only for target");

    po.addFlag('c', "showCorr", "show correspondences");
    po.addFlag('v', "visRes", "visualize results");
    // Parse
    if(!po.parse(argc, argv))
        return 1;

    int corrSize = po.getValue<int>("corrSize");
    radiusDistance = po.getValue<float>("radiusDistance");
    radiusAngle = po.getValue<float>("radiusAngle");

    bool showCorr = po.getFlag("showCorr");
    bool showResult = po.getFlag("visRes");

    const std::string directory = po.getValue("d");
    std::vector<std::string> objectsPath;
    objectsPath.push_back(po.getValue("pcd-query"));


    pcl::PointCloud<PointT>::Ptr target (new pcl::PointCloud<PointT>());
    COVIS_ASSERT(pcl::io::loadPCDFile<PointT>(po.getValue("pcd-target"), *target) == 0);
    COVIS_ASSERT(!target->empty());
    {
        ScopedTimer t("TARGET");
        ftarget = getHistograms(target, shortEdgeT, false);
        pcl::console::print_value("ftarget: %d \t short edges: %d\n", ftarget->size(), shortEdgeT->size());

    }

    pcl::PointCloud<PointT>::Ptr query (new pcl::PointCloud<PointT>());
    COVIS_ASSERT(pcl::io::loadPCDFile<PointT>(po.getValue("pcd-query"), *query) == 0);
    COVIS_ASSERT(!query->empty());
    {
        ScopedTimer t("QUERY");
        fquery = getHistograms(query, shortEdgeQ, true);
        pcl::console::print_value("fquery: %d \t short edges: %d\n", fquery->size(), shortEdgeQ->size());

    }

    core::Correspondence::Vec corr, best;
    pcl::PointCloud<core::CategorisedEdge>::Ptr final (new pcl::PointCloud<covis::core::CategorisedEdge>());
    pcl::PointCloud<core::CategorisedEdge>::Ptr aligned (new pcl::PointCloud<covis::core::CategorisedEdge>());

    {
        ScopedTimer t("ComputeKnnMatches");
        corr = *detect::computeKnnMatches<FeatureTwo>(fquery, ftarget, corrSize);
        pcl::console::print_value("Corr size: %d\n", corr.size());
    }

    {
        ScopedTimer t("CorrespondenceRejectorSampleConsensus");
        pcl::CorrespondencesPtr pclc = core::convert(corr);
        pcl::CorrespondencesPtr corrOut(new pcl::Correspondences());
        pcl::registration::CorrespondenceRejectorSampleConsensus<covis::core::CategorisedEdge> crsc;
        crsc.setInputTarget(shortEdgeT);
        crsc.setInputSource(shortEdgeQ);
        crsc.setMaximumIterations(50000);
        crsc.setInlierThreshold(0.01);
        crsc.setSaveInliers(true);
        crsc.setRefineModel(true);
        crsc.getRemainingCorrespondences(*pclc, *corrOut);

        Eigen::Matrix4f transformation = crsc.getBestTransformation();
        std::vector<int> ransacIdx;
        crsc.getInliersIndices(ransacIdx);
        pcl::transformPointCloud(*shortEdgeQ, *final, transformation);
    }

    if (showCorr) {
        best = corr;
        core::sort(best);
        //        best.resize(500);
        visu::showCorrespondences<covis::core::CategorisedEdge>(shortEdgeQ, shortEdgeT, best);
    }

    {
        ScopedTimer t("ICP");
        pcl::IterativeClosestPoint<covis::core::CategorisedEdge, covis::core::CategorisedEdge> icp;
        icp.setInputSource(final);
        icp.setInputTarget(shortEdgeT);
        icp.setMaximumIterations(1000);
        icp.align(*aligned);

        Eigen::Matrix4f m = icp.getFinalTransformation();
        std::cout << m(0) << " " << m(4) << " "<< m(8) << " " << m(12) << " "<< m(1) << " " << m(5) << " "<< m(9) << " " << m(13) << " "<< m(2) << " " << m(6) << " "<< m(10) << " " << m(14) << " "<< m(3) << " " << m(7) << " "<< m(11) << " " << m(15) << "\n";

    }


    if (showResult) {
        pcl::visualization::PCLVisualizer viewer ("Alignment");
        viewer.addPointCloud<covis::core::CategorisedEdge> (shortEdgeT, "shortEdgeT");
        viewer.setPointCloudRenderingProperties (pcl::visualization::PCL_VISUALIZER_COLOR, 255, 255, 255, "shortEdgeT");

        viewer.addPointCloud<covis::core::CategorisedEdge> (aligned, "aligned");
        viewer.setPointCloudRenderingProperties (pcl::visualization::PCL_VISUALIZER_COLOR, 255, 0, 0, "aligned");

        viewer.addPointCloud<covis::core::CategorisedEdge> (final, "final");
        viewer.setPointCloudRenderingProperties (pcl::visualization::PCL_VISUALIZER_COLOR, 0, 0, 255, "final");

        while (!viewer.wasStopped ())
        {
            viewer.spinOnce ();
            boost::this_thread::sleep (boost::posix_time::microseconds (100));
        }
    }


    return 0;
}


//----------------------------
pcl::PointCloud<FeatureTwo>::Ptr getHistograms(pcl::PointCloud<PointT>::Ptr &cloud,
        pcl::PointCloud<core::CategorisedEdge>::Ptr &edgeOut, bool isTemplate){
//EDGELABEL_OCCLUDING EDGELABEL_HIGH_CURVATURE
    pcl::PointCloud<FeatureT>::Ptr histograms (new pcl::PointCloud<FeatureT>());
    pcl::PointCloud<core::CategorisedEdge>::Ptr edge;
    feature::PCLEdgeExtraction<PointT> pclee =  feature::PCLEdgeExtraction<PointT>(depth, max_search, canny_low,
            canny_hight, EDGELABEL_RGB_CANNY | EDGELABEL_OCCLUDING | EDGELABEL_OCCLUDED, isTemplate);
    std::vector<pcl::PointIndices> label_indices;
    int edge_size;

    pcl::PointIndices::Ptr edge_indices  (new pcl::PointIndices);

    edge_size = pclee.extractPCLEdges(cloud, label_indices, edge, edgeOut, edge_indices);


    ftargetTwo.reset(new pcl::PointCloud<pcl::Histogram<HIST_DIM_TWO> >);
    ftargetTwo->resize(edge_size);
    {
        ScopedTimer t("computing distance histograms");

        feature::CategorizedEdgeDistanceHistogram<PointT, HIST_DIM> cedh;
        cedh.setEdgeAmount(edge_size);
        cedh.setEdgeCloud(*edge);
        cedh.setRadius(radiusDistance);
        ftargetAlternativeDistance = cedh.compute(cloud);
    }
    {
        ScopedTimer t("computing angle histograms");

        feature::CategorizedEdgeAngleHistogram<PointT, HIST_DIM> ceah;
        ceah.setEdgeAmount(edge_size);
        ceah.setEdgeCloud(*edge);
        ceah.setRadius(radiusAngle);
        ftargetAlternativeAngle = ceah.compute(cloud);
    }

    {
        ScopedTimer t("combining distance and angle histograms");
        for(int i = 0; i < edge_size; i++) {
            std::memcpy(ftargetTwo->points[i].histogram, ftargetAlternativeDistance->points[i].histogram, sizeof(ftargetAlternativeDistance->points[i].histogram));
            std::memcpy(ftargetTwo->points[i].histogram + HIST_DIM, ftargetAlternativeAngle->points[i].histogram, sizeof(ftargetAlternativeAngle->points[i].histogram));
        }

    }

    return ftargetTwo;
}
