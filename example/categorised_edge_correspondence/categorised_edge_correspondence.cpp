// We define PCL_NO_PRECOMPILE to use our own point type, CategorisedEdge
// See here: http://pointclouds.org/documentation/tutorials/adding_custom_ptype.php#example
#define PCL_NO_PRECOMPILE
#include <pcl/point_types.h>
#include <pcl/point_traits.h>
#include <pcl/features/normal_3d_omp.h>
#include <pcl/io/pcd_io.h>
#include <pcl/registration/correspondence_rejection_sample_consensus.h>
#include <pcl/registration/icp.h>
#include <pcl/sample_consensus/ransac.h>
#include <pcl/search/kdtree.h>
#include <pcl/visualization/pcl_visualizer.h>

#include <covis/covis.h>

using namespace covis::core;
using namespace covis::feature;
using namespace covis::visu;

void callback(const pcl::visualization::PointPickingEvent& ppe, void*);
void callbackTarget(const pcl::visualization::PointPickingEvent& ppe, void*);
std::vector<std::string> getObjectsPaths (const char* path);
// Desired histogram dimension
#define HIST_DIM 15 * 10
#define HIST_DIM_SMALL 15
#define HIST_DIM_TWO HIST_DIM *2

// Point and feature type
typedef pcl::PointXYZRGBA PointT;
typedef pcl::Histogram<HIST_DIM> FeatureT;
typedef pcl::Histogram<HIST_DIM_TWO> FeatureTwo;

pcl::PointCloud<covis::core::CategorisedEdge>::Ptr shortEdgeQ, shortEdgeT;
// Loaded point clouds and computed histogram features
pcl::PointCloud<PointT>::Ptr query, target;
pcl::PointCloud<FeatureT>::Ptr fquery, ftarget;

// Feature needs instantiation into PCL
POINT_CLOUD_REGISTER_POINT_STRUCT(pcl::Histogram<HIST_DIM>, (float[HIST_DIM], histogram, histogram))
POINT_CLOUD_REGISTER_POINT_STRUCT(pcl::Histogram<HIST_DIM_SMALL>, (float[HIST_DIM_SMALL], histogram, histogram))

pcl::PointCloud<FeatureT>::Ptr getHistograms(pcl::PointCloud<PointT>::Ptr &cloud,
        pcl::PointCloud<covis::core::CategorisedEdge>::Ptr &edgeOut, bool IsTemplate);



pcl::PointCloud<covis::core::CategorisedEdge>::Ptr emptyPointCloud;
pcl::visualization::PCLVisualizer pclvisu;

using namespace covis;
using namespace pcl;
using namespace pcl::io;
using namespace pcl::console;

enum {EDGELABEL_NAN_BOUNDARY=1, EDGELABEL_OCCLUDING=2, EDGELABEL_OCCLUDED=4, EDGELABEL_HIGH_CURVATURE=8, EDGELABEL_RGB_CANNY=16};

float depth = 0.02f;
int max_search = 50; int canny_low = 50; int canny_hight = 100; // 50 100 20,60    50, 30, 70
float radiusDistance;

int main(int argc, const char** argv) {
    core::ProgramOptions po;
    po.addPositional("pcd-query", "point cloud file for query point cloud");
    po.addPositional("pcd-target", "point cloud file for target point cloud");

    po.addOption("corrSize", 5, "correspondences search radius");
    po.addOption("radius", 0.03f, "radius for distance histogram");

    po.addOption("d", "no", "enable directory: all -  for query/target, no - disable directory (default), query - only for query, target - only for target");

    po.addFlag('c', "showCorr", "show correspondences");
    po.addFlag('v', "visRes", "visualize results");
    po.addFlag('q', "histQ", "show histogram query");
    po.addFlag('t', "histT", "show histogram target");
    // Parse
    if(!po.parse(argc, argv))
        return 1;


    int corrSize = po.getValue<int>("corrSize");
    radiusDistance = po.getValue<float>("radius");

    bool showCorr = po.getFlag("showCorr");
    bool showHistogramQ = po.getFlag("histQ");
    bool showHistogramT = po.getFlag("histT");
//    bool showResult = po.getFlag("visRes");

    const std::string directory = po.getValue("d");
    std::vector<std::string> objectsPath;
    if (directory == "all") {
        const char* pathObject = po.getValue("pcd-query").c_str();
        objectsPath = getObjectsPaths(pathObject);
    } else if (directory == "one"){
        objectsPath.push_back(po.getValue("pcd-query"));
    }

    pcl::PointCloud<PointT>::Ptr target (new pcl::PointCloud<PointT>());
    COVIS_ASSERT(pcl::io::loadPCDFile<PointT>(po.getValue("pcd-target"), *target) == 0);
    COVIS_ASSERT(!target->empty());
    {
        ScopedTimer t("TARGET");
        ftarget = getHistograms(target, shortEdgeT, false);
        pcl::console::print_value("ftarget: %d \t short edges: %d\n", ftarget->size(), shortEdgeT->size());

    }

    // Setup a point cloud visualizer
    if (showHistogramT) {
        pcl::visualization::PCLVisualizer pclvisu;
        pclvisu.addPointCloud<covis::core::CategorisedEdge>(shortEdgeT);
        pclvisu.registerPointPickingCallback(callbackTarget);
        pclvisu.spin();
    }

    pcl::PointCloud<PointT>::Ptr query (new pcl::PointCloud<PointT>());
    COVIS_ASSERT(pcl::io::loadPCDFile<PointT>(po.getValue("pcd-query"), *query) == 0);
    COVIS_ASSERT(!query->empty());
    {
        ScopedTimer t("query");
        fquery = getHistograms(query, shortEdgeQ, true);
        pcl::console::print_value("fquery: %d \t short edges: %d\n", fquery->size(), shortEdgeQ->size());

    }
    if (showHistogramQ) {

        emptyPointCloud.reset(new pcl::PointCloud<covis::core::CategorisedEdge>());
        emptyPointCloud->height = shortEdgeQ->height;
        emptyPointCloud->width = shortEdgeQ->width;
        emptyPointCloud->points.resize(emptyPointCloud->height * emptyPointCloud->width);

        pcl::visualization::PCLVisualizer pclvisu;
        pclvisu.addPointCloud<covis::core::CategorisedEdge>(shortEdgeQ);
        pclvisu.addPointCloud<covis::core::CategorisedEdge>(emptyPointCloud);
        pclvisu.registerPointPickingCallback(callback);
        pclvisu.spin();
    }

    core::Correspondence::Vec corr, best;
    pcl::PointCloud<covis::core::CategorisedEdge>::Ptr final (new pcl::PointCloud<covis::core::CategorisedEdge>());
    pcl::PointCloud<covis::core::CategorisedEdge>::Ptr aligned (new pcl::PointCloud<covis::core::CategorisedEdge>());

    {
        ScopedTimer t("computeKnnMatches");
        corr = *detect::computeKnnMatches<FeatureT>(fquery, ftarget, corrSize);
        std::cout << "corr size: " << corr.size() << std::endl;
    }
    pcl::registration::CorrespondenceRejectorSampleConsensus<covis::core::CategorisedEdge> crsc;
    {
        ScopedTimer t("CorrespondenceRejectorSampleConsensus");
        pcl::CorrespondencesPtr pclc = core::convert(corr);
        pcl::CorrespondencesPtr corrOut(new pcl::Correspondences());
        crsc.setInputTarget(shortEdgeT);
        crsc.setInputSource(shortEdgeQ);
        crsc.setMaximumIterations(50000);
        crsc.setInlierThreshold(0.01);
        crsc.setSaveInliers(true);
        crsc.setRefineModel(true);
        crsc.getRemainingCorrespondences(*pclc, *corrOut);

        Eigen::Matrix4f transformation = crsc.getBestTransformation();
        std::cout << "transformation:: " << transformation << std::endl;
        std::vector<int> ransacIdx;
        crsc.getInliersIndices(ransacIdx);
        pcl::transformPointCloud(*shortEdgeQ, *final, transformation);
    }

    if (showCorr) {
        best = corr;
        core::sort(best);
        best.resize(500);
        visu::showCorrespondences<covis::core::CategorisedEdge>(shortEdgeQ, shortEdgeT, best);
    }

    {
        ScopedTimer t("ICP");
        pcl::IterativeClosestPoint<covis::core::CategorisedEdge, covis::core::CategorisedEdge> icp;
        icp.setInputSource(final);
        icp.setInputTarget(shortEdgeT);
        icp.setMaximumIterations(5000);
        icp.align(*aligned);

        std::cout << "Final transformation: \n" << icp.getFinalTransformation()  * crsc.getBestTransformation() << std::endl;
        Eigen::Matrix4f m = icp.getFinalTransformation()  * crsc.getBestTransformation();
        std::cout << "\n\n";
        std::cout << m(0) << " " << m(4) << " "<< m(8) << " " << m(12) << " "<< m(1) << " " << m(5) << " "<< m(9) << " " << m(13) << " "<< m(2) << " " << m(6) << " "<< m(10) << " " << m(14) << " "<< m(3) << " " << m(7) << " "<< m(11) << " " << m(15) << "\n";
        //pcl::io::savePCDFile("s05_1.pcd", *shortEdgeT);
    }


    //    if (showResult) {
    //        pcl::visualization::PCLVisualizer viewer ("Alignment");
    //        viewer.addPointCloud<covis::core::CategorisedEdge> (shortEdgeT, "shortEdgeT");
    //        viewer.setPointCloudRenderingProperties (pcl::visualization::PCL_VISUALIZER_COLOR, 255, 255, 255, "shortEdgeT");
    //
    //        viewer.addPointCloud<covis::core::CategorisedEdge> (aligned, "aligned");
    //        viewer.setPointCloudRenderingProperties (pcl::visualization::PCL_VISUALIZER_COLOR, 255, 0, 0, "aligned");
    //
    //        viewer.addPointCloud<covis::core::CategorisedEdge> (final, "final");
    //        viewer.setPointCloudRenderingProperties (pcl::visualization::PCL_VISUALIZER_COLOR, 0, 0, 255, "final");
    //
    //        while (!viewer.wasStopped ())
    //        {
    //            viewer.spinOnce ();
    //            boost::this_thread::sleep (boost::posix_time::microseconds (100));
    //        }
    //    }


    return 0;
}


void callback(const pcl::visualization::PointPickingEvent& ppe, void*) {
    std::ofstream out;
    out.open("out.txt");
    const int idx = ppe.getPointIndex();
    if(idx >= 0 && size_t(idx) < fquery->size()) {
        pcl::console::print_value("Picked point %d: in original: %d \n", idx, (*shortEdgeQ)[idx].idx_original);
        std::cout << idx << "\t" <<(*shortEdgeQ)[idx].idx_edge_types[0] << ", " << (*shortEdgeQ)[idx].idx_edge_types[1] << ", " <<(*shortEdgeQ)[idx].idx_edge_types[2] << ", " << (*shortEdgeQ)[idx].idx_edge_types[3] << std::endl;
        const pcl::Histogram<HIST_DIM>& hidx = fquery->at(idx);
        const std::vector<float> hidxv(hidx.histogram, hidx.histogram + HIST_DIM);
        if(!covis::core::zero<float>(hidxv)) {
            pcl::console::print_info("Plotting the following histogram (press any key to close plot):\n");

            for(std::vector<float>::const_iterator it = hidxv.begin(); it != hidxv.end(); ++it)
                out << *it << ",";

            covis::core::print(hidxv);
            covis::visu::HistVisu<> hv(HIST_DIM);
            hv.setHist(hidx.histogram);
            hv.show();
        }

    }
    out.close();
}

void callbackTarget(const pcl::visualization::PointPickingEvent& ppe, void*) {
    const int idx = ppe.getPointIndex();
    if(idx >= 0 && size_t(idx) < ftarget->size()) {
        pcl::console::print_value("Picked point %d in original %d: \n", idx, (*shortEdgeT)[idx].idx_original);
        std::cout << idx << "\t" <<(*shortEdgeT)[idx].idx_edge_types[0] << ", " << (*shortEdgeT)[idx].idx_edge_types[1] << ", " <<(*shortEdgeT)[idx].idx_edge_types[2] << ", " << (*shortEdgeT)[idx].idx_edge_types[3] << std::endl;
        const pcl::Histogram<HIST_DIM>& hidx = ftarget->at(idx);
        const std::vector<float> hidxv(hidx.histogram, hidx.histogram + HIST_DIM);
        if(!covis::core::zero<float>(hidxv)) {
            pcl::console::print_info("Plotting the following histogram (press any key to close plot):\n");
            covis::core::print(hidxv);
            covis::visu::HistVisu<> hv(HIST_DIM);
            hv.setHist(hidx.histogram);
            hv.show();
        }
    }
}


pcl::PointCloud<FeatureT>::Ptr getHistograms(pcl::PointCloud<PointT>::Ptr &cloud,
        pcl::PointCloud<covis::core::CategorisedEdge>::Ptr &only_edge, bool IsTemplate){

    pcl::PointCloud<FeatureT>::Ptr histograms (new pcl::PointCloud<FeatureT>());
    pcl::PointCloud<covis::core::CategorisedEdge>::Ptr edge;
    feature::PCLEdgeExtraction<PointT> pclee =  feature::PCLEdgeExtraction<PointT>(depth, max_search, canny_low,
            canny_hight, EDGELABEL_OCCLUDING | EDGELABEL_OCCLUDED| EDGELABEL_HIGH_CURVATURE | EDGELABEL_RGB_CANNY, IsTemplate);
    std::vector<pcl::PointIndices> label_indices;
    pcl::PointIndices::Ptr edge_indices (new pcl::PointIndices);

    pclee.extractPCLEdges(cloud, label_indices, edge, only_edge, edge_indices);
    {
        ScopedTimer t("computing histograms");
        feature::CategorizedEdgeDistanceAngleHistogram<covis::core::CategorisedEdge, HIST_DIM> cedah;
        cedah.setEdgeCloud(*only_edge);
        cedah.setRadius(radiusDistance);
        histograms = cedah.compute(edge);
    }




    return histograms;
}


std::vector<std::string> getObjectsPaths (const char* path) {

    std::vector<std::string> objectsPath;
    core::ScopedTimer t("Reading object files");
    DIR *dir;
    struct dirent *ent;

    dir = opendir(path);
    std::string full_path;
    while ((ent = readdir(dir)) != NULL) {
        if(ent->d_type == DT_REG) {
            full_path = std::string(path);
            full_path.append(ent->d_name);
            objectsPath.push_back(full_path);
        }
    }
    pcl::console::print_value("amount of templates: %d\n", objectsPath.size());

    return objectsPath;
}

