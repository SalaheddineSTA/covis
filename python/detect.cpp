#include "covis_python_common.h"

#include <covis/detect/correspondence_filter_distance.h>
#include <covis/detect/correspondence_filter_ratio.h>
#include <covis/detect/correspondence_filter_transform.h>
#include <covis/detect/correspondence_voting.h>
#include <covis/detect/eigen_search.h>
#include <covis/detect/point_search.h>
#include <covis/detect/recognition.h>

#include <boost/make_shared.hpp>
using namespace boost::python;

/*
 * point_search.h
 */

float computeResolution(const Mesh& mesh, bool robust, int k) {
    return covis::detect::computeResolution(boost::make_shared<Mesh>(mesh), robust, k);
}

float computeDiagonal1(const Mesh& mesh) {
    return covis::detect::computeDiagonal(boost::make_shared<Mesh>(mesh));
}

float computeDiagonal2(const PointCloud& cloud) {
    PointCloudT::Ptr pcloud(new PointCloudT);
    pcl::fromPCLPointCloud2(cloud, *pcloud);
    return covis::detect::computeDiagonal<PointT>(pcloud);
}

/*
 * eigen_search.h
 */

typedef covis::detect::EigenSearch<Eigen::MatrixXf,flann::L2<float>> EigenSearch;
void setSearchTarget(EigenSearch& search, const object& array) {
    search.setTarget(fromNumpyArray(array));
}

list knn(const EigenSearch& search,
         const object& query,
         size_t k) {
    return toList(*search.knn(fromNumpyArray(query), k));
}

list radius(const EigenSearch& search,
            const object& query,
            float r) {
    return toList(*search.radius(fromNumpyArray(query), r));
}

list ratio(const EigenSearch& search,
           const object& query) {
    return toList(*search.ratio(fromNumpyArray(query)));
}

list computeKnnMatches(const object& query,
                       const object& target,
                       size_t k) {
    return toList(*covis::detect::computeKnnMatches(fromNumpyArray(query), fromNumpyArray(target), k));
}

list computeKnnMatchesExact(const object& query,
                            const object& target,
                            size_t k) {
    return toList(*covis::detect::computeKnnMatches(fromNumpyArray(query), fromNumpyArray(target), k, 0));
}

list computeRatioMatches(const object& query,
                         const object& target) {
    return toList(*covis::detect::computeRatioMatches(fromNumpyArray(query), fromNumpyArray(target)));
}

list computeRatioMatchesExact(const object& query,
                              const object& target) {
    return toList(*covis::detect::computeRatioMatches(fromNumpyArray(query), fromNumpyArray(target), 0));
}

list computeRadiusMatches(const object& query,
                          const object& target,
                          float r) {
    return toList(*covis::detect::computeRadiusMatches(fromNumpyArray(query), fromNumpyArray(target), r));
}

/*
 * search_base.h
 */
list computeFusionMatches(list corr) {
    std::vector<covis::core::Correspondence::VecPtr> correspondences;
    for(ssize_t i = 0; i < len(corr); ++i)
        correspondences.push_back(
                    boost::make_shared<covis::core::Correspondence::Vec>(
                        fromList<covis::core::Correspondence::Vec>(
                            extract<list>(corr[i]))
                        )
                    );
    return toList(*covis::detect::computeFusionMatches(correspondences));
}

/*
 * correspondence_filter_*.h
 */
list filterCorrespondencesDistance(list corr, float threshold) {
    return toList(*covis::detect::filterCorrespondencesDistance(fromList<covis::core::Correspondence::Vec>(corr), threshold));
}

list filterCorrespondencesRatio(list corr, float threshold) {
    return toList(*covis::detect::filterCorrespondencesRatio(fromList<covis::core::Correspondence::Vec>(corr), threshold));
}

list filterCorrespondencesTransform(list corr,
                                    const PointCloud& query,
                                    const PointCloud& target,
                                    const covis::core::Detection::MatrixT& pose,
                                    float threshold) {
    PointCloudT::Ptr pquery(new PointCloudT);
    PointCloudT::Ptr ptarget(new PointCloudT);
    pcl::fromPCLPointCloud2(query, *pquery);
    pcl::fromPCLPointCloud2(target, *ptarget);
    return toList(*covis::detect::filterCorrespondencesTransform<PointT>(
                      fromList<covis::core::Correspondence::Vec>(corr),
                      pquery,
                      ptarget,
                      pose,
                      threshold)
                  );
}

/*
 * correspondence_voting.h
 */

list filterCorrespondencesVoting(list corr,
                                 const PointCloud& query,
                                 const PointCloud& target,
                                 float rfradius,
                                 size_t samples,
                                 float similarity) {
    PointCloudT::Ptr pquery(new PointCloudT);
    PointCloudT::Ptr ptarget(new PointCloudT);
    pcl::fromPCLPointCloud2(query, *pquery);
    pcl::fromPCLPointCloud2(target, *ptarget);
    return toList(*covis::detect::filterCorrespondencesVoting<PointT>(
                      fromList<covis::core::Correspondence::Vec>(corr),
                      pquery,
                      ptarget,
                      rfradius,
                      samples,
                      similarity)
                  );
}

/*
 * recognition.h
 */

void setSources(covis::detect::Recognition<PointT>& rec, const list& clouds) {
    std::vector<PointCloudT::Ptr> pclouds(len(clouds));
    for(ssize_t i = 0; i < len(clouds); ++i) {
        pclouds[i].reset(new PointCloudT);
        pcl::fromPCLPointCloud2(extract<PointCloud>(clouds[i]), *pclouds[i]);
    }
    rec.setSources(pclouds);
}

list getSources(covis::detect::Recognition<PointT>& rec) {
    list result;
    for(size_t i = 0; i < rec.getSources().size(); ++i) {
        PointCloud cloud;
        pcl::toPCLPointCloud2(*rec.getSources()[i], cloud);
        result.append(cloud);
    }
    return result;
}

void setSourceFeatures(covis::detect::Recognition<PointT>& rec, const object& sourceFeatures) {
    rec.setSourceFeatures(fromNumpyArray(sourceFeatures));
}

object getSourceFeatures(const covis::detect::Recognition<PointT>& rec) {
    return toNumpyArray(rec.getSourceFeatures());
}

void setSourceNames(covis::detect::Recognition<PointT>& rec, const list& labels) {
    rec.setSourceNames(fromList<std::vector<std::string>>(labels));
}

list getSourceNames(covis::detect::Recognition<PointT>& rec) {
    return toList(rec.getSourceNames());
}

void setRecognitionTarget(covis::detect::Recognition<PointT>& rec, const PointCloud& cloud) {
    PointCloudT::Ptr pcloud(new PointCloudT);
    pcl::fromPCLPointCloud2(cloud, *pcloud);
    rec.setTarget(pcloud);
}

PointCloud getTarget(covis::detect::Recognition<PointT>& rec) {
    PointCloudT::ConstPtr pcloud = rec.getTarget();
    PointCloud result;
    pcl::toPCLPointCloud2(*pcloud, result);
    return result;
}

void setTargetFeatures(covis::detect::Recognition<PointT>& rec, const object& targetFeatures) {
    rec.setTargetFeatures(fromNumpyArray(targetFeatures));
}

object getTargetFeatures(const covis::detect::Recognition<PointT>& rec) {
    return toNumpyArray(rec.getTargetFeatures());
}

void setFeatureCorrespondences(covis::detect::Recognition<PointT>& rec, const list& featureCorrespondences) {
    rec.setFeatureCorrespondences(fromList<covis::core::Correspondence::Vec>(featureCorrespondences));
}

list getFeatureCorrespondences(covis::detect::Recognition<PointT>& rec) {
    return toList(rec.getFeatureCorrespondences());
}

list recognize(covis::detect::Recognition<PointT>& rec) {
    const covis::core::Detection::Vec detections = rec.recognize();
    return toList(detections);
}

/*
 * Module
 */
BOOST_PYTHON_MODULE(detect) {
    // Suppress numpy warning about unused function
    _import_array();
    docstring_options local_docstring_options(true, true, false);

    /*
     * point_search.h
     */
    def("computeResolution", computeResolution, (arg("mesh"), arg("robust")=false, arg("k")=5),
        "Estimate the resolution of a mesh");
    def("computeDiagonal", computeDiagonal1, arg("mesh"),
        "Compute the length of the diagonal of a mesh");
    def("computeDiagonal", computeDiagonal2, arg("cloud"),
        "Compute the length of the diagonal of a point cloud");

    /*
     * eigen_search.h
     */
    class_<EigenSearch>("EigenSearch")
            .def(init<optional<bool>>())
            .add_property("trees", &EigenSearch::getTrees, &EigenSearch::setTrees)
            .add_property("checks", &EigenSearch::getChecks, &EigenSearch::setChecks)
            .add_property("target", &EigenSearch::getTarget, setSearchTarget)
            .def("knn", knn, (arg("query"), arg("k")=1))
            .def("ratio", ratio)
            .def("radius", radius, (arg("query"), arg("r")))
            ;
    def("computeKnnMatches", computeKnnMatches, (arg("query"), arg("target"), arg("k")=1));
    def("computeKnnMatchesExact", computeKnnMatchesExact, (arg("query"), arg("target"), arg("k")=1));
    def("computeRatioMatches", computeRatioMatches, args("query", "target"));
    def("computeRatioMatchesExact", computeRatioMatchesExact, args("query", "target"));
    def("computeRadiusMatches", computeRadiusMatches, args("query", "target", "r"));
    def("computeFusionMatches", computeFusionMatches, arg("corr"));

    /*
     * correspondence_filter_*.h
     */
    def("filterCorrespondencesDistance", filterCorrespondencesDistance, args("corr", "threshold"));
    def("filterCorrespondencesRatio", filterCorrespondencesRatio, (arg("corr"), arg("threshold")=0.8));
    def("filterCorrespondencesTransform", filterCorrespondencesTransform,
        args("corr", "query", "target", "pose", "threshold"));

    /*
     * correspondence_voting.h
     */
    def("filterCorrespondencesVoting", filterCorrespondencesVoting,
        (arg("corr"), arg("query"), arg("target"), arg("rfradius"), arg("samples")=250, arg("similarity")=0.9));

    /*
     * recognition.h
     */
    {
        scope rec = class_<covis::detect::Recognition<PointT>>("Recognition")
                .add_property("detector",
                              &covis::detect::Recognition<PointT>::getDetector,
                              &covis::detect::Recognition<PointT>::setDetector)
                .add_property("multiInstance",
                              &covis::detect::Recognition<PointT>::getMultiInstance,
                              &covis::detect::Recognition<PointT>::setMultiInstance)
               .add_property("refinementIterations",
                              &covis::detect::Recognition<PointT>::getRefinementIterations,
                              &covis::detect::Recognition<PointT>::setRefinementIterations)
                .add_property("sources",
                              getSources,
                              setSources)
                .add_property("sourceFeatures",
                              getSourceFeatures,
                              setSourceFeatures)
                .add_property("sourceNames",
                              getSourceNames,
                              setSourceNames)
                .add_property("target",
                              getTarget,
                              setRecognitionTarget)
                .add_property("targetFeatures",
                              getTargetFeatures,
                              setTargetFeatures)
                .add_property("featureCorrespondences",
                              getFeatureCorrespondences,
                              setFeatureCorrespondences)
                .add_property("ransacIterations",
                              &covis::detect::Recognition<PointT>::getRansacIterations,
                              &covis::detect::Recognition<PointT>::setRansacIterations)
                .add_property("prereject",
                              &covis::detect::Recognition<PointT>::getPrereject,
                              &covis::detect::Recognition<PointT>::setPrereject)
                .add_property("prerejectionSimilarity",
                              &covis::detect::Recognition<PointT>::getPrerejectionSimilarity,
                              &covis::detect::Recognition<PointT>::setPrerejectionSimilarity)
                .add_property("inlierThreshold",
                              &covis::detect::Recognition<PointT>::getInlierThreshold,
                              &covis::detect::Recognition<PointT>::setInlierThreshold)
                .add_property("inlierFraction",
                              &covis::detect::Recognition<PointT>::getInlierFraction,
                              &covis::detect::Recognition<PointT>::setInlierFraction)
                .add_property("translationBandwidth",
                              &covis::detect::Recognition<PointT>::getTranslationBandwidth,
                              &covis::detect::Recognition<PointT>::setTranslationBandwidth)
                .add_property("rotationBandwidth",
                              &covis::detect::Recognition<PointT>::getRotationBandwidth,
                              &covis::detect::Recognition<PointT>::setRotationBandwidth)
                .add_property("kdeThreshold",
                              &covis::detect::Recognition<PointT>::getKdeThreshold,
                              &covis::detect::Recognition<PointT>::setKdeThreshold)
                .add_property("verbose",
                              &covis::detect::Recognition<PointT>::getVerbose,
                              &covis::detect::Recognition<PointT>::setVerbose)
                .def("recognize", recognize)
                ;

        enum_<covis::detect::Recognition<PointT>::DETECTOR>("DETECTOR")
                .value("RANSAC", covis::detect::Recognition<PointT>::RANSAC)
                .value("VOTING", covis::detect::Recognition<PointT>::VOTING)
                ;
    }
}
