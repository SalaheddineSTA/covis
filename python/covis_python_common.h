#ifndef COVIS_PYTHON_COMMON_H
#define COVIS_PYTHON_COMMON_H

#include <covis/feature/eigen_feature.h>

#define NPY_NO_DEPRECATED_API NPY_1_7_API_VERSION
#include <numpy/arrayobject.h>
#include <boost/python.hpp>

#include <boost/version.hpp>
#if (BOOST_VERSION / 100 % 1000 >= 63)
#include <boost/python/numpy.hpp>
#define BOOST_NUMPY_ENABLED
#else
#undef BOOST_NUMPY_ENABLED
#endif

#include <pcl/PCLPointCloud2.h>
#include <pcl/point_cloud.h>
#include <pcl/point_types.h>
#include <pcl/PolygonMesh.h>

typedef pcl::PointXYZRGBNormal PointT;
const size_t PointTDim = 11;
typedef pcl::PointCloud<PointT> PointCloudT;
typedef pcl::PCLPointCloud2 PointCloud;
typedef pcl::PolygonMesh Mesh;
typedef covis::feature::MatrixT Matrix;

template<typename VectorT>
boost::python::list toList(const VectorT& v) {
    boost::python::list l;
    for(size_t i = 0; i < v.size(); ++i)
        l.append(v[i]);
    return l;
}

template<typename VectorT>
VectorT fromList(const boost::python::list& l) {
    VectorT v;
    for(ssize_t i = 0; i < boost::python::len(l); ++i)
        v.push_back(boost::python::extract<typename VectorT::value_type>(l[i]));
    return v;
}

boost::python::object toNumpyArray(const Matrix& m);

boost::python::object toNumpyArray(npy_intp rows, npy_intp cols, float* data);

Matrix fromNumpyArray(const boost::python::object& array);

#endif
