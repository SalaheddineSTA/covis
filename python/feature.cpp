#include "covis_python_common.h"
using namespace boost::python;

/*
 * eigen_feature.h
 */
boost::python::object computeFeature1(const std::string& name,
                                       const PointCloud& cloud,
                                       const PointCloud& surface,
                                       float radius) {
    PointCloudT::Ptr pcloud(new PointCloudT);
    PointCloudT::Ptr psurface(new PointCloudT);
    pcl::fromPCLPointCloud2(cloud, *pcloud);
    pcl::fromPCLPointCloud2(surface, *psurface);

    return toNumpyArray(covis::feature::computeFeature<PointT>(name, pcloud, psurface, radius));
}

boost::python::object computeFeature2(const std::string& name,
                                        const list& clouds,
                                        const list& surfaces,
                                        float radius) {
    std::vector<PointCloudT::Ptr> pclouds(len(clouds));
    std::vector<PointCloudT::Ptr> psurfaces(len(surfaces));

    for(ssize_t i = 0; i < len(clouds); ++i) {
        pclouds[i].reset(new PointCloudT);
        psurfaces[i].reset(new PointCloudT);
        pcl::fromPCLPointCloud2(extract<PointCloud>(clouds[i]), *pclouds[i]);
        pcl::fromPCLPointCloud2(extract<PointCloud>(surfaces[i]), *psurfaces[i]);
    }

    covis::feature::MatrixT feat = covis::feature::computeFeature<PointT>(name, pclouds, psurfaces, radius);

    return toNumpyArray(feat);
}

/*
 * Module
 */
BOOST_PYTHON_MODULE(feature) {
    // Suppress numpy warning about unused function
    _import_array();
    docstring_options local_docstring_options(true, true, false);

    /*
     * eigen_feature.h
     */

    class_<covis::feature::MatrixT>("Matrix")
            .def_readonly("rows", &covis::feature::MatrixT::rows)
            .def_readonly("cols", &covis::feature::MatrixT::cols)
            .def_readonly("size", &covis::feature::MatrixT::size)
            ;
    def("computeFeature", computeFeature1, args("name", "cloud", "surface", "radius"));
    def("computeFeature", computeFeature2, args("name", "clouds", "surfaces", "radius"));
}
